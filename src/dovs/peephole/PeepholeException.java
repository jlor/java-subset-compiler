
package dovs.peephole;

@SuppressWarnings("serial")
public class PeepholeException extends RuntimeException {
    public PeepholeException(String message) {
	super(message);
    }
}
