
package dovs.peephole;

import dovs.instructions.Instruction;

public class InstructionList {
    Instruction inst;
    InstructionList next, previous;

    public InstructionList(Instruction inst, InstructionList next, InstructionList previous) {
	this.inst = inst;
	this.next = next;
	this.previous = previous;
    }

    public Instruction remove() {
	//if (inst == null) {
	//    throw new RuntimeException("Instruction already removed");
	//}
	previous.next = next;
	if (next != null) next.previous = previous;
	Instruction i = inst;
	//inst = null;
	//next = null;
	//previous = null;
	return i;
    }

    public InstructionList insertAfter(Instruction inst) {
	next = new InstructionList(inst, next, this);
	if (next.next != null) {
	    next.next.previous = next;
	}
	return next;
    }

}
