package dovs;

/** Thrown to indicate that the compiler has reached
 *  an unexpected situation, probably because of a
 *  bug in the compiler.
 */
@SuppressWarnings("serial")
public class InternalCompilerError extends RuntimeException {
	public InternalCompilerError(String message) {
		super(message);
	}

	public InternalCompilerError(String message, Throwable cause) {
		super(message, cause);
	}
}
