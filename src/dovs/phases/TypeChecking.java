package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import static dovs.Util.*;

import java.util.*;

/** Compiler phase to perform type checking of the program and link
 *  the uses of fields and methods to their definitions.
 *  It also performs type coercions and other transformations
 *  based on the types.
 */
public aspect TypeChecking extends DepthFirstAdapter {
   /** The static type of the expression */
   public PType PExp.type;

   /** The static type of the lvalue */
   public PType PLvalue.type;

   /** The declaration of the field referenced in this lvalue */
   public AFieldDecl AStaticFieldLvalue.field_decl;

   /** The declaration of the field referenced in this lvalue */
   public AFieldDecl ANonstaticFieldLvalue.field_decl;

   /** The declaration of the method invoked by this expression */
   public AMethodDecl AStaticInvokeExp.method_decl;

   /** The declaration of the method invoked by this expression */
   public AMethodDecl ANonstaticInvokeExp.method_decl;

   /** The declaration of the constructor invoked by this expression */
   public AConstructorDecl ANewExp.constructor_decl;

   /** The declaration of the constructor invoked by this statement */
   public AConstructorDecl ASuperStm.constructor_decl;

   /** The declaration of the constructor invoked by this statement */
   public AConstructorDecl AThisStm.constructor_decl;

   /** The return type of the current method.
    *  Used for type checking return statements.
    */
   private PType current_return_type;

   private boolean staticfound,nonstaticfound;	
   /** A dummy expression used temporarily in replace
    *  operations where the old expression is a subexpression
    *  of its replacement.
    */
   private static final PExp dummy_exp = new ANullExp();

   /** Insert a conversion to string of the given expression.
    *  @param exp the expression to coerce.
    */
   private static void coerceToString(PExp exp) {
      PExp rep = null;
      if (exp.type.isString()) {
	 // If the expression is not a constant or the result
	 // of a string concatenation or conversion, it may
	 // be null. Since null values must be coerced to the
	 // string "null", we must insert a conversion to string
	 // even though the value is already a string.
	 if (!(exp instanceof AStringConstExp ||
		  exp instanceof ABinopExp ||
		  exp instanceof AUnopExp)) {
	    rep = new AUnopExp(new AObjectToStringUnop(), dummy_exp);
		  }
      } else {
	 switch (exp.type.kindPType()) {
	    case BYTE:
	       rep = new AUnopExp(new AByteToStringUnop(), dummy_exp);
	       break;
	    case SHORT:
	       rep = new AUnopExp(new AShortToStringUnop(), dummy_exp);
	       break;
	    case INT:
	       rep = new AUnopExp(new AIntToStringUnop(), dummy_exp);
	       break;
	    case CHAR:
	       rep = new AUnopExp(new ACharToStringUnop(), dummy_exp);
	       break;
	    case BOOLEAN:
	       rep = new AUnopExp(new ABooleanToStringUnop(), dummy_exp);
	       break;
	    case NAMED:
	    case NULL:
	    case ARRAY:
	       // Null values are not subject to constant folding, so
	       // just replacing by the string "null" could lead to
	       // incorrect results.
	       rep = new AUnopExp(new AObjectToStringUnop(), dummy_exp);
	       break;
	    default:
	       errorBinopType((ABinopExp)exp.parent());
	 }
      }
      if (rep != null) {
	 rep.type = makeType("java.lang.String", true);
	 exp.replaceBy(rep);
	 dummy_exp.replaceBy(exp);
      }
   }

   /** Convert a comparison operator into its address
    *  comparison equivalent.
    *  @param comp the operator
    *  @return the address comparison operator
    */
   private static PBinop addressCompare(PBinop comp) {
      switch (comp.kindPBinop()) {
	 case EQ:
	    return new AAeqBinop(((AEqBinop)comp).getToken());
	 case NE:
	    return new AAneBinop(((ANeBinop)comp).getToken());
	 default:
	    return comp;
      }
   }

   public @Override
      void inAMethodDecl(AMethodDecl d){
	 if(d.getReturnType().kindPType()==EType.NAMED){
	    current_return_type=((ANamedType)d.getReturnType()).decl.type;
	    return;
	 }
	 current_return_type=d.getReturnType();
	 throwableConstructorOrMethod(d);         
      }

   /** Check if a value of one type can be assigned
    *  to a variable of another.
    *  @param to the type that was expected;

    *  @param from the type that was given
    *  @return <code>true</code> if the assignment is legal,
    *          <code>false</code> otherwise.
    */
   public static boolean assignable(PType to, PType from) {
      if(from==null || to==null) return false; //Defensive Programming
      EType ftype=from.kindPType();
      switch(to.kindPType()){
	 case ARRAY:
	    switch(ftype){
	       case NULL:
		  return true;
	       case ARRAY:
		  switch(((AArrayType)from).getType().kindPType()){
		     case ARRAY:
			return ((AArrayType)to).getType().kindPType()==EType.NAMED && ((ANamedType)((AArrayType)to).getType()).decl.canonical_name.equals("java.lang.Object");	   
		     default:
			return ((AArrayType)to).getType().kindPType()==EType.NAMED && 
			   ((AArrayType)from).getType().kindPType()==EType.NAMED &&
			   Hierarchy.isSuperType(((ANamedType)((AArrayType)to).getType()).decl.type, ((ANamedType)((AArrayType)from).getType()).decl.type)
			   || ((AArrayType)to).getType().equals(((AArrayType)from).getType());
		  }

	    }	
	    ;break;
	 case NAMED:
	    switch(ftype){
	       case NULL: 
		  return true;
	       case NAMED:
		  return /*((ANamedType)to).decl.canonical_name.equals(((ANamedType)from).decl.canonical_name) ||*/
		     Hierarchy.isSuperType(((ANamedType)to).decl.type,((ANamedType)from).decl.type);
	       case ARRAY:			  
		  String n=((ANamedType)to).decl.canonical_name;
		  return n.equals("java.io.Serializable") ||
		     n.equals("java.lang.Cloneable") ||
		     n.equals("java.lang.Object");	


	    }
	    ;break;
	 case INT:
	    return ftype==EType.SHORT || ftype==EType.CHAR || ftype==EType.BYTE || ftype==EType.INT;
	 case SHORT:
	    return ftype==EType.SHORT||ftype==EType.BYTE;
	 case BYTE:
	    return ftype==EType.BYTE;
	 case CHAR:	
	    return ftype==EType.CHAR;	 
	 case BOOLEAN: return ftype==EType.BOOLEAN;

      }
      return false;
   }

   /**
    * Check whether type {@code from} is assignable to the type {@code to} and
    * report an error if this is not the case.
    * 
    * @param pos
    *            the position at which the assignment occured
    * @param to
    *            the type being assigned to
    * @param from
    *            the type being assigned
    */
   private void checkAssignable(Token pos, PType to, PType from) {
      if (!assignable(to, from)) {
	 errorAssignType(pos, to, from);
      }
   }

   public @Override
      void outALocalLvalue(ALocalLvalue lvalue) {
	 lvalue.type = lvalue.local_decl.getType();
      }

   public @Override void outAArrayLvalue(AArrayLvalue lvalue) {
      PType btype = lvalue.getBase().type;
      switch (btype.kindPType()) {
	 case ARRAY:
	    lvalue.type = ((AArrayType)btype).getType();
	    break;
	 default:
	    errorNonArrayTypeArrayBase(lvalue, true);
      }
      if (!lvalue.getIndex().type.isNumeric()) {
	 errorNonNumericArrayIndex(lvalue);
      }
   }

   private boolean samePackage(PTypeDecl h1,PTypeDecl h2){
      String str1=h1.canonical_name.substring(0,Math.max(0,h1.canonical_name.lastIndexOf(".")));
      String str2=h2.canonical_name.substring(0,Math.max(0,h2.canonical_name.lastIndexOf(".")));
      return str1.equals(str2);
   }

   private boolean checkProtected(PTypeDecl host, PTypeDecl host2, PTypeDecl fhost,boolean isStatic){
      if(!samePackage(host,fhost)){
	 if(Hierarchy.isSuperType(fhost.type,host.type)){
	    if(isStatic)
	       return true;
	    if(Hierarchy.isSuperType(host.type,host2.type))
	       return true;	
	 }
	 return false;	  
      }	
      return true;
   }

   public @Override 
      void outANonstaticFieldLvalue(ANonstaticFieldLvalue lv){
	 //Check if array.length
	 PExp exp=lv.getExp();
	 PType lvtype=exp.type;
	 if(!lvtype.isJoosType())
	    errorNonJoosFieldType(lv,true);
	 if(lvtype.kindPType()==EType.ARRAY){				
	    if(lv.getName().getText().trim().equals("length")){			
	       AArrayLengthExp al=new AArrayLengthExp(exp);
	       al.type=new AIntType();
	       lv.parent().replaceBy(al);					
	       return;
	    }else{
	       errorFieldOnArray(lv,true);
	    }										
	 }              

	 PTypeDecl host=((ANamedType)lv.getExp().type).decl;
	 TIdentifier ident=lv.getName();
	 AFieldDecl decl=Hierarchy.lookupField(host,ident);
	 if(decl==null){
	    errorFieldNotFound(host,lv.getName(),true);
	    return;
	 }         

	 if(decl.getStatic()!=null)
	    errorStaticFieldLinkedAsNonstatic(lv);		
	 PTypeDecl host2=lv.getAncestor(PTypeDecl.class);
	 PTypeDecl fhost=decl.getAncestor(PTypeDecl.class);
	 if(decl.getAccess().kindPAccess()==EAccess.PROTECTED){
	    if(!checkProtected(host2,host,fhost,false)){
	       errorProtectedMemberAccess(lv.getName());
	    }
	 }
	 lv.field_decl=decl;
	 lv.type=decl.getType();
      }

   public @Override
      void outAStaticFieldLvalue(AStaticFieldLvalue lv){
	 PTypeDecl host=((ANamedType)lv.getType()).decl;
	 AFieldDecl fdecl=Hierarchy.lookupField(host,lv.getName());
	 if(fdecl==null){
	    errorFieldNotFound(host,lv.getName(),true);
	    return;
	 }

	 if(fdecl.getStatic()==null)
	    errorNonstaticFieldLinkedAsStatic(lv);

	 PTypeDecl host2=lv.getAncestor(PTypeDecl.class);
	 PTypeDecl fhost=fdecl.getAncestor(PTypeDecl.class);
	 if(fdecl.getAccess().kindPAccess()==EAccess.PROTECTED){
	    if(!checkProtected(host2,host,fhost,true)){
	       errorProtectedMemberAccess(lv.getName());
	    }
	 }

	 lv.field_decl=fdecl;
	 lv.type=fdecl.getType();
	 if(!lv.type.isJoosType())
	    errorNonJoosFieldType(lv,true);
      }	

   public @Override 
      void outACastExp(ACastExp exp){
	 PType t=exp.getType();
	 PType expt=exp.getExp().type;
	 if(exp.getType().kindPType()==EType.NAMED){
	    PType temp=((ANamedType)t).decl.type;
	    t=temp;
	 }

	 if(t.isNumeric() && expt.isNumeric()
	       || assignable(t,expt)
	       || assignable(expt,t)){
	    exp.type=t;return;
	       }
	 exp.type=t;
	 errorInvalidCast(exp);			
      }	

   private static void SOP(String in){
      //System.out.println(in);
   }

   public @Override
      void outAFieldDecl(AFieldDecl d){
	 if(d.getInit()!=null){
	    checkAssignable(d.getName(),d.getType(),d.getInit().type);
	 }
      }	

   public @Override
      void inAClassTypeDecl(AClassTypeDecl decl){
	 Collection<ASimpleInvokeExp> exps=decl.getDescendants(ASimpleInvokeExp.class);
	 for(ASimpleInvokeExp exp:exps)
	    transSimple(exp);	   
      }	

   private 
      void transSimple(ASimpleInvokeExp exp){
	 if(exp.getAncestor(AMethodDecl.class) != null && exp.getAncestor(AMethodDecl.class).getStatic() != null){
	    checkJoos1ImplicitThisClassStaticMethod(exp);
	 }
	 ANonstaticInvokeExp newNode = new ANonstaticInvokeExp();
	 AThisExp newThis = new AThisExp(new TThis(exp.getToken().getLine(),exp.getToken().getPos()));
	 newNode.setReceiver(newThis);
	 newNode.setName(exp.getName());
	 newNode.setArgs(exp.getArgs());
	 exp.replaceBy(newNode);
      }

   private AMethodDecl findMethodDecl(PTypeDecl host,TIdentifier ident,List<PType> l,boolean isstatic){
      Set<AMethodDecl> ll=Hierarchy.lookupMethod(host,ident);
      if(ll == null)
	 return null;
      for(AMethodDecl d:ll){
	 List<PType> types=getType(d);
	 boolean good=true;
	 if(types.size()!=l.size())
	    continue;
	 if(types.equals(l) && 
	       ((d.getStatic()!=null && isstatic) ||
		(d.getStatic()==null && !isstatic))){
	    return d;
		}
	 else if(types.equals(l)){
	    if(d.getStatic()!=null){
	       staticfound=true;
	    }else{
	       nonstaticfound=true;	
	    }
	 }
      }	
      return null;	      
   }


   private AConstructorDecl findConstructor(ANamedType t,List<PType> ty){
      PTypeDecl host=t.decl;
      Collection<AConstructorDecl> decls=host.getDescendants(AConstructorDecl.class);
      for(AConstructorDecl d:decls){
	 List<PType> types=getType(d);
	 if(types.equals(ty))
	    return d;
      }
      return null;
   }

   public @Override
      void outANonstaticInvokeExp(ANonstaticInvokeExp exp){
	 PType rec=exp.getReceiver().type;

	 if(!(rec instanceof ANamedType) || ((ANamedType)rec).decl==null){
	    if(rec.kindPType()==EType.ARRAY){
	       checkJoos1ArrayMethodCall(exp);
	       exp.type=rec;
	       return;		    
	    }else{ 
	       errorNonReferenceReceiver(exp,true);
	    }

	 }

	 PTypeDecl host=((ANamedType)rec).decl;//Receiver
	 List<PType> types=new LinkedList<PType>();
	 for(PExp e:exp.getArgs()){
	    types.add((PType)e.type.clone());
	 }

	 staticfound=false;
	 AMethodDecl md=findMethodDecl(host,exp.getName(),types,false);
	 if(md==null){
	    /*if(staticfound && exp.getDescendants(AThisExp.class).size()>0){
	       checkJoos1ImplicitThisClassStaticMethod(exp);
	    }else */
	    if(staticfound){
	       errorStaticMethodLinkedAsNonstatic(exp);
	    }
	    errorNoMatchingMethodFound(exp,true);
	 }		

	 PTypeDecl host2=exp.getAncestor(PTypeDecl.class);//Host of expression
	 PTypeDecl mhost=md.getAncestor(PTypeDecl.class);//Host of declaration
	 if(md.getAccess().kindPAccess()==EAccess.PROTECTED){ 
	    if(!checkProtected(host2,host,mhost,false)){
	       errorProtectedMemberAccess(exp.getName());
	    } 
	 }

	 exp.method_decl=md;	
	 AMethodDecl temp=exp.getAncestor(AMethodDecl.class);

	 if(temp!=null){
	    //nested in a method
	    testThrows(temp,host,types,exp.getName());
	 } else {
	    AConstructorDecl tcons=exp.getAncestor(AConstructorDecl.class);
	    if(tcons!=null){
	       //nested in a constructor	       
	       testThrows(tcons,host,types,exp.getName());
	    }else{
	       //in a fielddecl
	       AFieldDecl fdecl = exp.getAncestor(AFieldDecl.class);
	       if(fdecl!=null){
		  //Since we don't have inner classes only methods are accessed here..
		  Set<AMethodDecl> met=Hierarchy.lookupMethod(host,exp.getName());
		  List<Util.MethodOrConstructor> ll=new LinkedList<Util.MethodOrConstructor>();                        

		  for(Util.MethodOrConstructor de:met){
		     if(getType(de).equals(types))
			ll.add(de);
		  }

		  List<ANamedType> res=getIntersection(ll);
		  for(ANamedType nt: res){
		     if(!isUnChecked(nt))
			errorIllegalThrows(exp.method_decl.getName(),nt);
		  }
	       }
	    }
	 }
	 exp.type=exp.method_decl.getReturnType();

	 if(!exp.type.isJoosType())
	    errorNonJoosReturnType(exp,false);
      }

   public @Override
      void outAStaticInvokeExp(AStaticInvokeExp exp){
	 PTypeDecl host=((ANamedType)exp.getType()).decl;

	 List<PType> types=new LinkedList<PType>();	      
	 for(PExp d:exp.getArgs()){
	    types.add(d.type);
	 }

	 nonstaticfound=false;
	 exp.method_decl = findMethodDecl(host,exp.getName(),types,true);
	 /*if(exp.getDescendants(AThisExp.class).size()>0)
	    checkJoos1ImplicitThisClassStaticMethod(exp);*/
	 if(exp.method_decl==null){
	    if(nonstaticfound){
	       errorNonstaticMethodLinkedAsStatic(exp);	
	    }
	    errorNoMatchingMethodFound(exp,true);

	 }

	 PTypeDecl host2=exp.getAncestor(PTypeDecl.class);
	 PTypeDecl mhost=exp.method_decl.getAncestor(PTypeDecl.class);

	 if(exp.method_decl.getAccess().kindPAccess()==EAccess.PROTECTED){ 
	    if(!checkProtected(host2,host,mhost,true)){
	       errorProtectedMemberAccess(exp.getName());
	    } 
	 }

	 AMethodDecl d=exp.getAncestor(AMethodDecl.class);
	 if(d!=null){
	    testThrows(d,host,types,exp.getName().clone());
	 } else {
	    AConstructorDecl dd=exp.getAncestor(AConstructorDecl.class);

	    if(dd!=null){
	       testThrows(dd,host,types,exp.getName().clone());
	    }else{
	       Set<AMethodDecl> met=Hierarchy.lookupMethod(host,exp.getName());
	       List<Util.MethodOrConstructor> ll=new LinkedList<Util.MethodOrConstructor>();

	       for(Util.MethodOrConstructor de:met){
		  if(getType(de).equals(types))
		     ll.add(de);
	       }

	       List<ANamedType> res=getIntersection(ll);
	       for(ANamedType nt: res){
		  if(!isUnChecked(nt))
		     errorIllegalThrows(exp.method_decl.getName(),nt);
	       }
	    }
	 }
	 exp.type=exp.method_decl.getReturnType();

	 if(!exp.type.isJoosType())
	    errorNonJoosReturnType(exp,false);
      }

   private boolean validInstanceofType(PType t){
      EType type=t.kindPType();
      return type==EType.NAMED || type==EType.NULL || type==EType.ARRAY;
   }

   public @Override
      void outAInstanceofExp(AInstanceofExp exp){
	 PExp pexp=exp.getExp();
	 PType pt=pexp.type;
	 PType D=exp.getType();

	 if(!validInstanceofType(pt)||!validInstanceofType(D))
	    errorInvalidInstanceof(exp);

	 if(assignable(pt,D)||assignable(D,pt)){
	    exp.type=new ABooleanType();
	    return;
	 }
	 exp.type=new ABooleanType();

	 errorInvalidInstanceof(exp);
      }

   public @Override 
      void outASuperStm(ASuperStm exp){
	 List<PType> l=new LinkedList<PType>();
	 AClassTypeDecl host=exp.getAncestor(AClassTypeDecl.class);
	 for(PExp e:exp.getArgs())
	    l.add(e.type);	   
	 exp.constructor_decl = findConstructor(host.getSuper(),l);

	 if(exp.constructor_decl==null){
	    errorNoMatchingConstructorFound(exp,true);
	 }

	 AMethodDecl temp=exp.getAncestor(AMethodDecl.class);
	 if(temp!=null){
	    testThrows(temp,host.getSuper().decl,l,exp.constructor_decl.getName().clone());
	 }else{
	    AConstructorDecl tconst=exp.getAncestor(AConstructorDecl.class);
	    if(tconst!=null){
	       testThrows(tconst,host.getSuper().decl,l,exp.constructor_decl.getName().clone());
	    }else{
	       AFieldDecl de=exp.getAncestor(AFieldDecl.class);
	       Collection<AConstructorDecl> d=host.getSuper().decl.getDescendants(AConstructorDecl.class);
	       List<Util.MethodOrConstructor> d2=new LinkedList<Util.MethodOrConstructor>();
	       for(Util.MethodOrConstructor mm:d) 
		  if(getType(mm).equals(l)) 
		     d2.add(mm);
	       List<ANamedType> s=getIntersection(d2);
	       for(ANamedType n:s){
		  if(!isUnChecked(n)){
		     errorIllegalThrows(exp.constructor_decl.getName(),n);
		  }
	       }
	    }
	 }
      }

   public @Override
      void inAThisExp(AThisExp exp){
	 AMethodDecl m = exp.getAncestor(AMethodDecl.class);
	 if(m != null && m.getStatic() != null && exp.getAncestor(AStaticInvokeExp.class)==null){
	    errorThisInStaticContext(exp);
	 }
	 AFieldDecl f = exp.getAncestor(AFieldDecl.class);
	 if(f != null && f.getStatic() != null){
	    errorThisInStaticContext(exp);
	 }
      }

   public @Override 
      void outAThisExp(AThisExp exp){
	 PTypeDecl host=exp.getAncestor(PTypeDecl.class);
	 exp.type=host.type;
      }

   public @Override
      void inAConstructorDecl(AConstructorDecl c){
	 String con_name = c.getName().clone().getText();
	 String class_name = c.getAncestor(PTypeDecl.class).getName().clone().getText();
	 if (!con_name.equals(class_name)){
	    errorConstructorName(c);
	 }
	 current_return_type=null;
	 throwableConstructorOrMethod(c);
      }

   private void throwableConstructorOrMethod(PDecl c){
      if (!(c instanceof AFieldDecl)){
	 List<ANamedType> ll;
	 if (c instanceof AMethodDecl){
	    ll = ((AMethodDecl)c).getThrows();
	 }else {
	    ll = ((AConstructorDecl)c).getThrows();
	 }
	 for(ANamedType t:ll){
	    PTypeDecl throwable = Environments.lookupNamedType("java.lang.Throwable");
	    if (!Hierarchy.isSuperType(throwable.type,t)){
	       errorNonThrowableInThrows(t);
	    }
	 }                
      } 
   }

   private List<ANamedType> getIntersection(List<Util.MethodOrConstructor> ll){
      if(ll.size()<=0) return new LinkedList<ANamedType>();
      List<ANamedType> res=ll.get(0).getThrows();
      for(int i=1; i<ll.size();i++){
	 List<ANamedType> thrs=ll.get(i).getThrows();
	 List<ANamedType> temp=new LinkedList<ANamedType>();
	 for(ANamedType am:res){
	    for(ANamedType am2:thrs){
	       if(assignable(am,am2)){
		  temp.add(am2);}
	    }
	 }
	 res=temp;
      }
      return res;
   }

   private List<PType> getType(Util.MethodOrConstructor m){
      List<PType> l=new LinkedList<PType>();
      for(ALocalDecl ld: m.getFormals()){
	 PType np=ld.getType();
	 if(np.kindPType()==EType.NAMED){
	    np=((ANamedType)np).decl.type;
	 }
	 l.add(np);
      }
      return l;
   }

   private void testThrows(Util.MethodOrConstructor m,PTypeDecl receiver,List<PType> l,TIdentifier ident){
      List<ANamedType> thr=m.getThrows();
      List<ANamedType> interthr;
      List<Util.MethodOrConstructor> d=new ArrayList<Util.MethodOrConstructor>();
      List<Util.MethodOrConstructor> res=new ArrayList<Util.MethodOrConstructor>();
      Set<AMethodDecl> tempd=Hierarchy.lookupMethod(receiver,ident);
      //Tricky.. Hierarchy.lookupMethod returnerer null eller []..
      if(tempd!=null && tempd.size()>0){
	 d.addAll(tempd);
      }else{
	 d.addAll(receiver.getDescendants(AConstructorDecl.class));
      }
      for(Util.MethodOrConstructor amd:d){
	 List<PType> ll=getType(amd);            
	 if(ll.equals(l)){
	    res.add(amd);
	 }
      }
      List<ANamedType> n=getIntersection(res);
      if(n==null)
	 return;
      for(ANamedType tp:n){
	 if(!isUnChecked(tp)){
	    boolean found=false;
	    for(ANamedType tpp: thr){
	       if(!isUnChecked(tpp)){
		  if(assignable(tpp,tp) || assignable(tp,tpp)){
		     found=true;
		     break;
		  }       
	       }             
	    }
	    if(!found){
	       errorIllegalThrows(ident,tp);
	    }
	 }
      }
   }

   private boolean isUnChecked(ANamedType t){
      PTypeDecl pt=Environments.lookupNamedType("java.lang.RuntimeException");
      PTypeDecl er=Environments.lookupNamedType("java.lang.Error");
      return (assignable(pt.type,t) || assignable(er.type,t));
   }  

   public @Override 
      void outANewExp(ANewExp exp){
	 PTypeDecl host = exp.getAncestor(AClassTypeDecl.class);
	 if(exp.getType()!=null &&
	       exp.getType().kindPType()==EType.NAMED){
	    PTypeDecl p = Environments.lookupNamedType(((ANamedType)exp.getType()).decl.canonical_name);
	    List<PType> l=new LinkedList<PType>();
	    for(PExp e:exp.getArgs()){
	       l.add(e.type);	   
	    } 
	    if(exp.getType().kindPType()==EType.NAMED){
	       exp.type=((ANamedType)exp.getType()).decl.type;
	       if(p instanceof AInterfaceTypeDecl){
		  errorInstantiateInterface(p.type);
	       }else if(((AClassTypeDecl)p).getAbstract() != null){
		  errorInstantiateAbstractClass(p.type);
	       }

	       exp.constructor_decl = findConstructor((ANamedType)exp.type,l);
	       if(exp.constructor_decl==null){
		  errorNoMatchingConstructorFound(exp,true);
	       } else {
		  PTypeDecl host2 = exp.constructor_decl.getAncestor(PTypeDecl.class);
		  if(exp.constructor_decl.getAccess().kindPAccess() == EAccess.PROTECTED){
		     if(!samePackage(host,host2)){
			errorProtectedConstructorInvocation(exp.getToken());
		     }
		  }
	       }
	    }

	    AMethodDecl temp=exp.getAncestor(AMethodDecl.class);
	    if(temp!=null){
	       testThrows(temp,((ANamedType)exp.type).decl,l,((ANamedType)exp.type).decl.getName());
	    }else{
	       AConstructorDecl tconst=exp.getAncestor(AConstructorDecl.class);
	       if(tconst!=null){
		  testThrows(tconst,((ANamedType)exp.type).decl,l,((ANamedType)exp.type).decl.getName());
	       }else{
		  AFieldDecl fdecl = exp.getAncestor(AFieldDecl.class);

		  if(fdecl!=null){
		     //Her aendres !!
		     Collection<AConstructorDecl> decls=p.getDescendants(AConstructorDecl.class);
		     List<Util.MethodOrConstructor> d2=new LinkedList<Util.MethodOrConstructor>();
		     for(Util.MethodOrConstructor mm:decls) 
			if(getType(mm).equals(l)) 
			   d2.add(mm);
		     List<ANamedType> s=getIntersection(d2);
		     for(ANamedType n:s){
			if(!isUnChecked(n)){
			   errorIllegalThrows(exp.getToken(),n);
			}
		     }
		  }
	       }
	    }
	       }
	 exp.type = exp.getType();
      }	

   public @Override 
      void outANewArrayExp(ANewArrayExp exp){
	 List<PExp> pt=exp.getSizes();
	 for(PExp e:pt){
	    PType t=e.type;
	    if(t.isNumeric()){
	    }else{
	       errorNonNumericArraySize(exp);				
	    }
	 }	 
	 exp.type=exp.getType();			
      }

   public @Override
      void outALvalueExp(ALvalueExp exp) {
	 PLvalue lv=exp.getLvalue();
	 exp.type = lv.type;
      }

   public @Override
      void outAIntConstExp(AIntConstExp exp) {
	 exp.type = new AIntType();
      }

   public @Override
      void outACharConstExp(ACharConstExp exp) {
	 AIntConstExp rep = new AIntConstExp();
	 rep.value = exp.value;
	 rep.type = new ACharType();
	 exp.replaceBy(rep);
      }

   public @Override
      void outAStringConstExp(AStringConstExp exp) {
	 exp.type = makeType("java.lang.String", true);
      }

   public @Override
      void outABooleanConstExp(ABooleanConstExp exp) {
	 exp.type = new ABooleanType();
      }

   public @Override
      void outANullExp(ANullExp exp) {
	 exp.type = new ANullType();
      }

   public @Override void outABinopExp(ABinopExp exp) {
      PExp left = exp.getLeft(), right = exp.getRight();
      PType lt = left.type, rt = right.type;
      EType ltk = lt.kindPType();
      EType rtk = rt.kindPType();
      switch (exp.getBinop().kindPBinop()) {
	 case PLUS:
	    if (lt.isString() || rt.isString()) {
	       // String concatenation
	       coerceToString(left);
	       coerceToString(right);
	       exp.setBinop(new AConcatBinop(((APlusBinop)exp.getBinop()).getToken()));
	       exp.type = makeType("java.lang.String", true);
	    } else {
	       if (lt.isNumeric() && rt.isNumeric()) {
		  // Integer addition
	       } else {
		  errorBinopType(exp);
	       }
	       exp.type = new AIntType();
	    }
	    break;
	 case MINUS:
	 case TIMES:
	 case DIVIDE:
	 case MODULO:
	    if (lt.isNumeric() && rt.isNumeric()) {
	       // Integer operation
	    } else {
	       errorBinopType(exp);
	    }
	    exp.type = new AIntType();
	    break;
	 case EQ:
	 case NE:
	    if (lt.isNumeric() && rt.isNumeric()) {
	       // Integer comparison
	    } else if (ltk == EType.BOOLEAN && rtk == EType.BOOLEAN) {
	       // Boolean comparison
	    } else if (lt.isReference() && rt.isReference() &&
		  (assignable(lt, rt) || assignable(rt, lt))) {
	       // Reference comparison
	       exp.setBinop(addressCompare(exp.getBinop()));
	    } else {
	       errorBinopType(exp);
	    }
	    exp.type = new ABooleanType();
	    break;
	 case LT:
	 case LE:
	 case GT:
	 case GE:
	    if (lt.isNumeric() && rt.isNumeric()) {
	       // Integer comparison
	    } else {
	       errorBinopType(exp);
	    }
	    exp.type = new ABooleanType();
	    break;
	 case AND:
	 case OR:
	 case XOR:
	    if (ltk == EType.BOOLEAN && rtk == EType.BOOLEAN) {
	       // Boolean combination
	       exp.type = new ABooleanType();
	    } else {
	       checkJoos1BitwiseOperations(exp);
	       exp.type = new AIntType();
	    }
	    break;
	 case LAZY_AND:
	 case LAZY_OR:
	    if (ltk == EType.BOOLEAN && rtk == EType.BOOLEAN) {
	       // Boolean combination
	    } else {
	       errorBinopType(exp);
	    }
	    exp.type = new ABooleanType();
	    break;
      }
   }

   public @Override void outAUnopExp(AUnopExp exp) {
      PType et = exp.getExp().type;
      switch (exp.getUnop().kindPUnop()) {
	 case NEGATE:
	    if (et.isNumeric()) {
	       // Integer negation
	    } else {
	       errorUnopType(exp);
	    }
	    exp.type = new AIntType();
	    break;
	 case COMPLEMENT:
	    if (et.kindPType() == EType.BOOLEAN) {
	       // Boolean complement
	    } else {
	       errorUnopType(exp);
	    }
	    exp.type = new ABooleanType();
	    break;
	 default:
	    throw new InternalCompilerError("Unexpected unop: "+exp.getUnop().kindPUnop());
      }
   }

   public @Override
      void outAAssignmentExp(AAssignmentExp exp) {
	 PLvalue lt = exp.getLvalue();
	 PExp et = exp.getExp();
	 Collection<ANonstaticFieldLvalue> l1=lt.getDescendants(ANonstaticFieldLvalue.class);
	 Collection<AStaticFieldLvalue> l2=lt.getDescendants(AStaticFieldLvalue.class);
	 if(l1.size()>0){
	    ANonstaticFieldLvalue ll=l1.iterator().next();
	    if (ll.field_decl != null){
	       AFieldDecl d1=ll.field_decl;
	       if(d1.isFinal()){
		  errorAssignToFinalField(ll);
	       }
	    } else {
	       TIdentifier t1=ll.getName();
	       if (t1.getText().equals("length")){
		  errorAssignToArrayLength(ll,true);
	       }
	    }
	 }
	 if (l2.size()>0){
	    AStaticFieldLvalue ll=l2.iterator().next();
	    AFieldDecl d2=ll.field_decl;
	    if(d2.isFinal()){
	       errorAssignToFinalField(ll);
	    }
	 }
	 checkAssignable(exp.getToken(), lt.type, et.type);
	 exp.type = lt.type;
      }

   public @Override
      void outAIfThenStm(AIfThenStm stm) {
	 if (stm.getCondition().type.kindPType() != EType.BOOLEAN) {
	    errorNonBooleanCondition(stm);
	 }
      }

   public @Override
      void outAIfThenElseStm(AIfThenElseStm stm) {
	 if (stm.getCondition().type.kindPType() != EType.BOOLEAN) {
	    errorNonBooleanCondition(stm);
	 }
      }

   public @Override 
      void outALocalDeclStm(ALocalDeclStm stm){
	 if(stm.getLocalDecl().getInit()!=null){
	    checkAssignable(stm.getToken(),stm.getLocalDecl().getType(),stm.getLocalDecl().getInit().type);
	 }
      }	

   public @Override
      void outAWhileStm(AWhileStm stm) {
	 if (stm.getCondition().type.kindPType() != EType.BOOLEAN) {
	    errorNonBooleanCondition(stm);
	 }
      }

   public @Override
      void outAVoidReturnStm(AVoidReturnStm stm) {
	 if (current_return_type != null && current_return_type.kindPType() != EType.VOID) {
	    errorVoidReturnInNonVoidMethod(stm);
	 }
      }

   public @Override
      void outAValueReturnStm(AValueReturnStm stm) {
	 PType rt = current_return_type;
	 PType et = stm.getExp().type;
	 checkAssignable(stm.getToken(), rt, et);
      }


   // /////////////////////////////////////////////////////////////////////////
   // ERROR MESSAGES
   // /////////////////////////////////////////////////////////////////////////

   /**
    *	Reports the error that an invocations does not resolve uniquely a
    *	closest matching method or constructor.
    *	@param	invocation	the ambiguous invocation 
    */
   @SuppressWarnings("unused")
      private static void errorAmbiguousOverloading(Invocation invocation) {
	 Errors.error(ErrorType.AMBIGUOUS_OVERLOADING, invocation.getToken(),
	       "Ambiguous overloading", false);
      }

   /**
    * 	Reports the error that array length cannot be assigned to.
    * 	@param	lvalue	the nonstatic field lvalue which is the array length expression
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorAssignToArrayLength(ANonstaticFieldLvalue lvalue,
	    boolean fatal) {
	 Errors.error(ErrorType.ASSIGN_TO_ARRAY_LENGTH, lvalue.getName(),
	       "Array length cannot be assigned to", fatal);
      }

   /**
    * 	Reports the error that a final field cannot be assigned to.
    * 	@param	fieldAccess	the field access of the final field
    */
   @SuppressWarnings("unused")
      private static void errorAssignToFinalField(FieldAccess fieldAccess) {
	 Errors.error(ErrorType.ASSIGN_TO_FINAL_FIELD, fieldAccess.getName(),
	       "Final field " + fieldAccess.getName().getText() + " cannot be assigned to", false);
      }

   /**
    * 	Reports the error that two types are assignment incompatible.
    * 	@param	pos		the position at which the error occured
    * 	@param	to		the type which was assigned to
    * 	@param	from	the type which was assigned from
    */
   @SuppressWarnings("unused")
      private static void errorAssignType(Token pos, PType to, PType from) {
	 Errors.error(ErrorType.ASSIGN_TYPE, pos, "The type " + from.typeName()
	       + " cannot be assigned to the type " + to.typeName(), false);
      }

   /**
    * 	Reports the error that a binary operator cannot be used with the types of
    * 	its subexpressions.
    * 	@param	exp		the binary expression
    */
   @SuppressWarnings("unused")
      private static void errorBinopType(ABinopExp exp) {
	 Errors.error(ErrorType.BINOP_TYPE, exp.getBinop().getToken(),
	       "The operator " + exp.getBinop().opName()
	       + " cannot be used with the types "
	       + exp.getLeft().type.typeName() + " and "
	       + exp.getRight().type.typeName(), false);
      }

   /**
    * 	Reports the error that a constructor invocation is circular.
    * 	@param	decl	one of the constructor declarations
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorCircularConstructorInvocation(
	    AConstructorDecl decl, boolean fatal) {
	 Errors.error(ErrorType.CIRCULAR_CONSTRUCTOR_INVOCATION, decl.getName(),
	       "Circular constructor invocation", fatal);
	    }

   /**
    * 	Reports the error that a constructor does not have the same name as its
    * 	enclosing class.
    * 	@param	constructor	the constructor declaration
    */
   @SuppressWarnings("unused")
      private static void errorConstructorName(AConstructorDecl constructor) {
	 Errors.error(ErrorType.CONSTRUCTOR_NAME, constructor.getName(),
	       "Constructor must have the same name as its enclosing class",
	       false);
      }

   /**
    * 	Reports the error that a field could not be found on a host type.
    * 	@param	host		the host type
    * 	@param	field_id	the identifier of the field
    * 	@param	fatal		if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorFieldNotFound(PTypeDecl host, TIdentifier field_id, boolean fatal) {
	 Errors.error(ErrorType.FIELD_NOT_FOUND, field_id,
	       "Could not find field " + field_id.getText() + " on type " 
	       + host.canonical_name, fatal);
      }

   /**
    * 	Reports the error that the field cannot be accessed on an array.
    * 	@param	lvalue	the nonstatic field lvalue
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorFieldOnArray(ANonstaticFieldLvalue lvalue,
	    boolean fatal) {
	 Errors.error(ErrorType.FIELD_ON_ARRAY, lvalue.getName(),
	       "Cannot access field on array", fatal);
      }

   /**
    * 	Reports the error that a field cannot be accessed on a non-reference
    * 	type.
    * 	@param	lvalue	the nonstatic field lvalue
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorFieldOnNonReference(ANonstaticFieldLvalue lvalue,
	    boolean fatal) {
	 Errors.error(ErrorType.FIELD_ON_NON_REFERENCE, lvalue.getName(),
	       "Cannot access field on " + lvalue.getExp().type.typeName(),
	       fatal);
      }

   /**
    * 	Reports the error that a checked exception cannot be thrown from the
    *	current method context.
    *	@param	pos			the position at which the error occured
    *	@param	throw_type	the named type of the exception
    */
   @SuppressWarnings("unused")
      private static void errorIllegalThrows(Token pos, ANamedType throw_type) {
	 Errors.error(ErrorType.ILLEGAL_THROWS, pos,
	       "The type " + throw_type.typeName()
	       + " cannot be thrown by the current context", false);
      }

   /**
    * 	Reports the error that an abstract class cannot be instantiated.
    * 	@param	class_type	the named type of the class
    */
   @SuppressWarnings("unused")
      private static void errorInstantiateAbstractClass(ANamedType class_type) {
	 Errors.error(ErrorType.INSTANTIATE_ABSTRACT_CLASS, class_type.getName()
	       .getToken(), "Abstract class " + class_type.decl.canonical_name
	       + " cannot be instantiated", false);
      }

   /**
    * 	Reports the error that an interface cannot be instantiated.
    * 	@param	interface_type	the named type of the interface
    */
   @SuppressWarnings("unused")
      private static void errorInstantiateInterface(ANamedType interface_type) {
	 Errors.error(ErrorType.INSTANTIATE_INTERFACE, interface_type.getName()
	       .getToken(), "Interface " + interface_type.decl.canonical_name
	       + " cannot be instantiated", false);
      }

   /**
    * 	Reports the error that a cast expression is invalid.
    * 	@param	exp		the cast expression
    */
   @SuppressWarnings("unused")
      private static void errorInvalidCast(ACastExp exp) {
	 PType lt = exp.getExp().type;
	 PType rt = exp.getType();
	 Errors.error(ErrorType.INVALID_CAST, exp.getToken(), "Cannot cast "
	       + lt.typeName() + " to " + rt.typeName(), false);
      }

   /**
    * 	Reports the error that an instanceof expression is invalid.
    * 	@param	exp		the instanceof expression
    */
   @SuppressWarnings("unused")
      private static void errorInvalidInstanceof(AInstanceofExp exp) {
	 PType rt = exp.getExp().type;
	 PType lt = exp.getType();
	 Errors.error(ErrorType.INVALID_INSTANCEOF, exp.getToken(),
	       "Cannot check if " + rt.typeName() + " in an instance of "
	       + lt.typeName(), false);
      }

   /**
    *	If compiling with the {@code -joos1} option, reports the error that
    *	array method call is used.
    *	@param	exp		the nonstatic invoke expression with an array as reciever
    */
   @SuppressWarnings("unused")
      private static void checkJoos1ArrayMethodCall(ANonstaticInvokeExp exp) {
	 Errors.checkJoos1(ErrorType.JOOS1_ARRAY_METHOD_CALL, exp.getName(),
	       "array method call");
      }

   /**
    * 	If compiling with the {@code -joos1} option, reports the error that
    *	bitwise operation is used.
    *	@param	exp		the binary expression with a bitwise operator
    */
   @SuppressWarnings("unused")
      private static void checkJoos1BitwiseOperations(ABinopExp exp) {
	 Errors.checkJoos1(ErrorType.JOOS1_BITWISE_OPERATIONS, exp.getBinop()
	       .getToken(), "bitwise operations");
      }

   /**
    * 	If compiling with the {@code -joos1} option, reports the error that
    *	closest method overloading is used.
    *	@param	invocation	the invocation using closest method overloading
    */
   @SuppressWarnings("unused")
      private static void checkJoos1ClosestMatchOverloading(Invocation invocation) {
	 Errors.checkJoos1(ErrorType.JOOS1_CLOSEST_MATCH_OVERLOADING, invocation
	       .getToken(), "closest match overloading");
      }

   /**
    * 	If compiling with the {@code -joos1} option, reports the error that
    *	implicit this class for a static method invocation is used.
    *	@param	invocation	the invocation using implicit this class
    */
   @SuppressWarnings("unused")
      private static void checkJoos1ImplicitThisClassStaticMethod(
	    Invocation invocation) {
	 Errors.checkJoos1(ErrorType.JOOS1_IMPLICIT_THIS_CLASS_STATIC_METHOD,
	       invocation.getToken(), "implicit this class for static methods");
	    }

   /**
    * 	Reports the error that no matching constructor was found for the
    *	constructor invocation.
    *	@param	invocation	the constructor invocation
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNoMatchingConstructorFound(Invocation invocation,
	    boolean fatal) {
	 Errors.error(ErrorType.NO_MATCHING_CONSTRUCTOR_FOUND, invocation
	       .getToken(), "Could not find a matching constructor", fatal);
      }

   /**
    * 	Reports the error that no matching method was found for the method
    *	invocation.
    *	@param	invocation	the method invocation
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNoMatchingMethodFound(Invocation invocation,
	    boolean fatal) {
	 Errors.error(ErrorType.NO_MATCHING_METHOD_FOUND, invocation.getToken(),
	       "Could not find a matching method for "
	       + Util.getInvocationSignature(invocation), fatal);
      }

   /**
    *	Reports the error that an array lvalue does not have an array base type.
    *	@param	exp		the array lvalue expression
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNonArrayTypeArrayBase(AArrayLvalue exp,
	    boolean fatal) {
	 Errors.error(ErrorType.NON_ARRAY_TYPE_ARRAY_BASE, exp.getToken(),
	       "Array base must have array type", fatal);
      }

   /**
    * 	Reports the error that a condition does not have a boolean type.
    * 	@param	stm		the statement with the condition
    */
   @SuppressWarnings("unused")
      private static void errorNonBooleanCondition(PStm stm) {
	 Errors.error(ErrorType.NON_BOOLEAN_CONDITION, stm.getToken(),
	       "Condition must have type boolean", false);
      }

   /**
    *	Reports the error that a field has a non-Joos type.
    * 	@param	fieldAccess	the field access of the field with non-Joos type
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNonJoosFieldType(FieldAccess fieldAccess,
	    boolean fatal) {
	 Errors.error(ErrorType.NON_JOOS_FIELD_TYPE, fieldAccess.getName(),
	       "The field " + fieldAccess.getName().getText()
	       + " has non-Joos type", fatal);
      }

   /**
    * 	Reports the error that a parameter on a called constructor or method
    * 	has a non-Joos type.
    * 	@param	invocation	the constructor or method declaration of the called
    * 						method
    */
   @SuppressWarnings("unused")
      private static void errorNonJoosParameterType(Invocation invocation) {
	 Errors.error(ErrorType.NON_JOOS_PARAMETER_TYPE, invocation.getToken(),
	       "Called " + invocation.getInvocationName()
	       + " has non-Joos parameter type", false);
      }

   /**
    * 	Reports the error that the return type on a called method has a non-Joos
    * 	type.
    * 	@param	invocation	the method declaration of the called method
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNonJoosReturnType(Invocation invocation,
	    boolean fatal) {
	 Errors.error(ErrorType.NON_JOOS_RETURN_TYPE, invocation.getToken(),
	       "Called " + invocation.getInvocationName()
	       + " has non-Joos return type", fatal);
      }

   /**
    * 	Reports the error that an array lvalue has a non-numeric index type.
    * 	@param	lvalue	the array lvalue
    */
   @SuppressWarnings("unused")
      private static void errorNonNumericArrayIndex(AArrayLvalue lvalue) {
	 Errors.error(ErrorType.NON_NUMERIC_ARRAY_INDEX, lvalue.getToken(),
	       "Array index must have numeric type", false);
      }

   /**
    * 	Reports the error that an new array expression has a non-numeric size
    * 	type.
    * 	@param	exp		the new array expression
    */
   @SuppressWarnings("unused")
      private static void errorNonNumericArraySize(ANewArrayExp exp) {
	 Errors.error(ErrorType.NON_NUMERIC_ARRAY_SIZE, exp.getToken(),
	       "Array size must have numeric type", false);
      }

   /**
    * 	Reports the error that an inc/dec expression has a non-numeric type.
    * 	@param	exp		the inc/dec expression
    */
   @SuppressWarnings("unused")
      private static void errorNonNumericIncDec(AIncDecExp exp) {
	 Errors.error(ErrorType.NON_NUMERIC_INC_DEC, exp.getIncDecOp()
	       .getToken(), "Inc/dec needs a numeric type", false);
      }

   /**
    * 	Reports the error that a receiver of a nonstatic invoke expression has
    * 	a non-reference type.
    * 	@param	exp		the nonstatic invoke expression
    * 	@param	fatal	if {@code true} the compilation is stopped immediately
    */
   @SuppressWarnings("unused")
      private static void errorNonReferenceReceiver(ANonstaticInvokeExp exp,
	    boolean fatal) {
	 Errors.error(ErrorType.NON_REFERENCE_RECEIVER, exp.getName(),
	       "Only class types can be method invocation receivers", fatal);
      }

   /**
    * 	Reports the error that a nonstatic field is linked as static.
    * 	@param	lvalue	the static field lvalue
    */
   @SuppressWarnings("unused")
      private static void errorNonstaticFieldLinkedAsStatic(
	    AStaticFieldLvalue lvalue) {
	 Errors.error(ErrorType.NONSTATIC_FIELD_LINKED_AS_STATIC, lvalue
	       .getName(), "The field " + lvalue.getName().getText()
	       + " is not static", false);
	    }

   /**
    * 	Reports the error that a nonstatic method is linked as static.
    * 	@param	exp		the static invoke expression
    */
   @SuppressWarnings("unused")
      private static void errorNonstaticMethodLinkedAsStatic(AStaticInvokeExp exp) {
	 Errors.error(ErrorType.NONSTATIC_METHOD_LINKED_AS_STATIC,
	       exp.getName(), "The method " + exp.getName().getText()
	       + " is not static", false);
      }

   /**
    * 	Reports the error that a protected constructor has been invoked from
    * 	outside its package.
    * 	@param	pos		the position at which the error occured
    */
   @SuppressWarnings("unused")
      private static void errorProtectedConstructorInvocation(Token pos) {
	 Errors.error(ErrorType.PROTECTED_CONSTRUCTOR_INVOCATION, pos,
	       "Invocation of protected constructors using new is "
	       + " only allowed from the same package", false);
      }

   /**
    * 	Reports the error that a protected member has been accessed illegally.
    * 	@param	pos		the position at which the error occured
    */
   @SuppressWarnings("unused")
      private static void errorProtectedMemberAccess(Token pos) {
	 Errors.error(ErrorType.PROTECTED_MEMBER_ACCESS, pos,
	       "Protected members can only be accessed from the same "
	       + "package or from a subclass", false);
      }

   /**
    * 	Reports the error that a qualified protected member has been accessed
    *	illegally.
    * 	@param	pos		the position at which the error occured
    */
   @SuppressWarnings("unused")
      private static void errorQualifiedProtectedMemberAccess(Token pos) {
	 Errors.error(ErrorType.PROTECTED_MEMBER_ACCESS, pos,
	       "Qualified access to protected members must be on a subtype "
	       + "of the current class", false);
      }

   /**
    * 	Reports the error that a static field is linked as nonstatic.
    * 	@param	lvalue	the nonstatic field lvalue
    */
   @SuppressWarnings("unused")
      private static void errorStaticFieldLinkedAsNonstatic(
	    ANonstaticFieldLvalue lvalue) {
	 Errors.error(ErrorType.STATIC_FIELD_LINKED_AS_NONSTATIC, lvalue
	       .getName(), "The field " + lvalue.getName().getText()
	       + " is static", false);
	    }

   /**
    * 	Reports the error that a static method is linked as nonstatic.
    * 	@param	exp		the static invoke expression
    */
   @SuppressWarnings("unused")
      private static void errorStaticMethodLinkedAsNonstatic(
	    ANonstaticInvokeExp exp) {
	 Errors.error(ErrorType.STATIC_METHOD_LINKED_AS_NONSTATIC,
	       exp.getName(), "The method " + exp.getName().getText()
	       + " is static", false);
	    }

   /**
    * 	Reports the error that this is used before a super constructor call.
    * 	@param	exp		the this expression
    */
   @SuppressWarnings("unused")
      private static void errorThisBeforeSuperCall(AThisExp exp) {
	 Errors.error(ErrorType.THIS_BEFORE_SUPER_CALL, exp.getToken(),
	       "Cannot refer to this before super constructor call", false);
      }

   /**
    * 	Reports the error that this is used in a static context.
    * 	@param	exp		the this expression
    */
   @SuppressWarnings("unused")
      private static void errorThisInStaticContext(AThisExp exp) {	
	 Errors.error(ErrorType.THIS_IN_STATIC_CONTEXT, exp.getToken(),
	       "Cannot refer to this in a static context", false);
      }

   /**
    * 	Reports the error that a named type in a throws clause is not throwable.
    * 	@param	thrown_type		the not throwable named type 
    */
   @SuppressWarnings("unused")
      private static void errorNonThrowableInThrows(ANamedType thrown_type) {
	 Errors.error(ErrorType.NON_THROWABLE_IN_THROWS, thrown_type.getToken(),
	       "Thrown classes must be throwable", false);
      }

   /**
    * 	Reports the error that a unary operator cannot be used with the type of
    * 	its subexpression.
    * 	@param	exp		the unary expression
    */
   @SuppressWarnings("unused")
      private static void errorUnopType(AUnopExp exp) {
	 Errors.error(ErrorType.UNOP_TYPE, exp.getUnop().getToken(),
	       "The operator " + exp.getUnop().opName()
	       + " cannot be used with the type "
	       + exp.getExp().type.typeName(), false);
      }

   /**
    * 	Reports the error that a non-void method has void return statement.
    * 	@param	stm		the void return statement
    */
   @SuppressWarnings("unused")
      private static void errorVoidReturnInNonVoidMethod(AVoidReturnStm stm) {
	 Errors.error(ErrorType.VOID_RETURN_IN_NON_VOID_METHOD, stm.getToken(),
	       "A non-void method must return a value", false);
      }

}
