package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import java.util.*;

/** Compiler phase to check definite assignment of local
 *  variables.
 */
public aspect DefiniteAssignment extends DepthFirstAdapter {

	// /////////////////////////////////////////////////////////////////////////
	// ERROR MESSAGES
	// /////////////////////////////////////////////////////////////////////////

	/**
	 *	If compiling with the {@code -joos1} option, reports the error that
	 *	a local variable is found in its own initializer.
	 *	@param	lvalue	the lvalue refering to the local variable
	 */
	@SuppressWarnings("unused")
	private static void checkJoos1LocalVariableInOwnInitializer(
			ALocalLvalue lvalue) {
		Errors.checkJoos1(ErrorType.JOOS1_LOCAL_VARIABLE_IN_OWN_INITIALIZER,
				lvalue.getName(), "local variable in own initializer");
	}

	/**
	 *	If compiling with the {@code -joos1} option, reports the error that
	 *	a local variable declaration has no initializer.
	 *	@param	stm		the local variable declaration
	 */
	@SuppressWarnings("unused")
	private static void checkJoos1OmittedLocalInitializer(ALocalDeclStm stm) {
		Errors.checkJoos1(ErrorType.JOOS1_OMITTED_LOCAL_INITIALIZER, stm
				.getToken(), "omitted local initializer");
	}

	/**
	 * 	Reports the error that a local variable is not definitely assigned.
	 * 	@param	lvalue	the lvalue refering to the local variable
	 */
	@SuppressWarnings("unused")
	private static void errorVariableMightNotHaveBeenInitialized(
			ALocalLvalue lvalue) {
		Errors.error(ErrorType.VARIABLE_MIGHT_NOT_HAVE_BEEN_INITIALIZED, lvalue
				.getName(), "Variable " + lvalue.getName().getText()
				+ " might not have been initialized", false);
	}

}
