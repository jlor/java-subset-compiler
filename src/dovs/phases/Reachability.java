package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import static dovs.Util.*;

import java.util.*;

/**
 * Compiler phase to check reachability of statements and definite return from
 * methods and constructors.
 */
public aspect Reachability extends DepthFirstAdapter {

   public boolean PStm.complete;

   public @Override 
      void inAMethodDecl(AMethodDecl decl){
	 if(decl.isAbstract())
	    return;
	 ABlock methodBlock=decl.getBody().getBlock();
	 boolean complete=true;
	 for(PStm stm:methodBlock.getStatements()){
	    complete=recurse(stm,complete);
	 }

	 if(complete){
	    if(decl.getReturnType().kindPType()!=EType.VOID){
	       errorMissingReturnStatement(decl);
	    } else {
	       LinkedList<PStm> childstatements=methodBlock.getStatements();
	       childstatements.addLast(new AVoidReturnStm(new TReturn()));
	       methodBlock.setStatements(childstatements);
	       decl.getBody().getBlock().replaceBy(methodBlock);
	    }
	 }
      }

   public @Override 
      void inAConstructorDecl(AConstructorDecl decl){
	 ABlock methodBlock=decl.getBody().getBlock();
	LinkedList<PStm> childstatements=methodBlock.getStatements();

	 boolean complete=true;
	 for(PStm stm:childstatements){
	    complete=recurse(stm,complete);
	 }
	 if(complete){
	    	    childstatements.addLast(new AVoidReturnStm(new TReturn()));
	    methodBlock.setStatements(childstatements);
	    decl.getBody().getBlock().replaceBy(methodBlock);	  
	 }
      }

   private boolean recurse(PStm stm,boolean reachable){
	//Ment som en eksakt implementation af reglerne...
      if(reachable==false)
	 errorUnreachableStatement(stm);
      stm.complete=reachable;
      switch(stm.kindPStm()){
	 case BLOCK:
	    ABlock block=((ABlockStm)stm).getBlock();
	    for(PStm substm:block.getStatements()){
	       stm.complete=recurse(substm,stm.complete);
	    }
	    break;		   
	 case IF_THEN:
	    PStm substm=((AIfThenStm)stm).getBody();
	    recurse(substm,stm.complete);
	    break;
	 case IF_THEN_ELSE:
	    PStm substm1 = ((AIfThenElseStm)stm).getThenBody();
	    PStm substm2 = ((AIfThenElseStm)stm).getElseBody();
	    stm.complete=recurse(substm1,stm.complete)|recurse(substm2,stm.complete);
	    break;
	 case WHILE:
	    PExp condition=((AWhileStm)stm).getCondition(); 
	    if(condition.kindPExp()==EExp.BOOLEAN_CONST){
		if(((ABooleanConstExp)condition).getBool().kindPBool() == EBool.FALSE){
	       		recurse(((AWhileStm)stm).getBody(),false);
	    	}else{
	       		stm.complete=false;
	       		recurse(((AWhileStm)stm).getBody(),reachable);
	    	} 
	    }
	    break;  
	 case VOID_RETURN:
	 case THROW:
	 case VALUE_RETURN:
	    stm.complete=false;
	    break;	    
      }
      return stm.complete;
   }

   // /////////////////////////////////////////////////////////////////////////
   // ERROR MESSAGES
   // /////////////////////////////////////////////////////////////////////////

   /**
    * Reports the error that the method has a missing return statement.
    * 
    * @param method_or_constructor
    *            the method missing a return statement
    */
   @SuppressWarnings("unused")
      private static void errorMissingReturnStatement(
	    MethodOrConstructor method_or_constructor) {
	 Errors.error(ErrorType.MISSING_RETURN_STATEMENT, method_or_constructor
	       .getName(), "Missing return statement", false);
	    }

   /**
    * Reports the error that a statement is unreachable.
    * 
    * @param stm
    *            the unreachable statement
    */
   @SuppressWarnings("unused")
      private static void errorUnreachableStatement(PStm stm) {
	 Errors.error(ErrorType.UNREACHABLE_STATEMENT, stm.getToken(),
	       "Unreachable statement", false);
      }

}
