package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import dovs.instructions.*;
import java.util.*;
import java.io.*;

/** Final compiler phase to emit the generated code
 *  for all classes and interfaces to jasmin files.
 */
public aspect CodeEmission extends DepthFirstAdapter {
	/** The output stream for the current jasmin file */
	private PrintStream out;

	/** Opens the jasmin file for this class or interface */
	public @Override
	void inASourceFile(ASourceFile sf) {
		String sourcename = sf.getFileName();
		String filename = sourcename.substring(0, sourcename.lastIndexOf('.'))
		+ ".j";
		try {
			out = new PrintStream(new FileOutputStream(filename));
		} catch (IOException e) {
			Errors.fatalErrorMessage(ErrorType.FILE_OPEN_ERROR,
					"Unable to open file " + filename + ": " + e.getMessage());
		}
		// '/' -> File.separator for platform independence
		out.println(".source "
				+ sourcename.substring(sourcename
						.lastIndexOf(File.separatorChar) + 1));
	}

	/** Closes the jasmin file for this class or interface */
	public @Override
	void outASourceFile(ASourceFile sf) {
		out.close();
	}

	/** Writes the class header */
	public @Override
	void inAClassTypeDecl(AClassTypeDecl classd) {
		out.println(".class public " + (classd.isAbstract() ? "abstract " : "")
				+ (classd.isFinal() ? "final " : "") + classd.getSignature());
		out.println(".super " + classd.getSuper().decl.getSignature());
		for (ANamedType itype : classd.getInterfaces()) {
			out.println(".implements " + itype.decl.getSignature());
		}

		// Jasmin needs the fields first
		for (PDecl member : (List<PDecl>) classd.getMembers()) {
			if (member instanceof AFieldDecl) {
				AFieldDecl fieldd = (AFieldDecl) member;
				out.println(".field " + fieldd.getAccess().accessText() + " "
						+ (fieldd.isStatic() ? "static " : "")
						+ (fieldd.isFinal() ? "final " : "") + "\""
						+ fieldd.getName().getText() + "\" "
						+ fieldd.getType().getSignature());
			}
		}
	}

	/** Writes the interface header */
	public @Override
	void inAInterfaceTypeDecl(AInterfaceTypeDecl interfaced) {
		out.println(".interface public abstract " + interfaced.getSignature());
		out.println(".super java/lang/Object");
		for (ANamedType itype : interfaced.getSupers()) {
			out.println(".implements " + itype.decl.getSignature());
		}
	}

	public @Override
	void inAMethodDecl(AMethodDecl methodd) {
		String sig = methodd.getSignature();
		sig = methodd.getName().getText() + sig.substring(sig.indexOf('('));
		out.println();
		out.println(".method " + methodd.getAccess().accessText() + " "
				+ (methodd.isStatic() ? "static " : "")
				+ (methodd.isFinal() ? "final " : "")
				+ (methodd.isAbstract() ? "abstract " : "") + sig);
		for (ANamedType ttype : methodd.getThrows()) {
			out.println(".throws " + ttype.decl.getSignature());
		}
	}

	public @Override
	void outAMethodDecl(AMethodDecl methodd) {
		out.println(".end method");
	}

	public @Override
	void inAConstructorDecl(AConstructorDecl constructord) {
		String sig = constructord.getSignature();
		sig = "<init>" + sig.substring(sig.indexOf('('));
		out.println();
		out.println(".method " + constructord.getAccess().accessText() + " "
				+ sig);
		for (ANamedType ttype : constructord.getThrows()) {
			out.println(".throws " + ttype.decl.getSignature());
		}
	}

	public @Override
	void outAConstructorDecl(AConstructorDecl constructord) {
		out.println(".end method");
	}

	public @Override
	void caseABody(ABody body) {
		out.println(".limit stack " + body.max_stack);
		out.println(".limit locals " + body.max_locals);
		for (Instruction inst : body.instructions) {
			out.println(inst.toAsm());
		}
	}

}
