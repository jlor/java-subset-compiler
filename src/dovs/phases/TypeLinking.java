package dovs.phases;

import java.io.*;
import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import static dovs.Util.*; 
import java.util.*;

/** Compiler phase to link the uses of all types
 *  to their definitions.
 */
public aspect TypeLinking extends DepthFirstAdapter {
	/** The declaration of this named typei */
	public PTypeDecl ANamedType.decl;
	/** The type associated with this class declaration */
	public ANamedType PTypeDecl.type;
	public Map<String,String> ASourceFile.nameToImNames;
	public Map<String,AOnDemandImportDecl> ASourceFile.dmnd;
	public boolean AOnDemandImportDecl.checked;

	public @Override
		void inAClassTypeDecl(AClassTypeDecl classd) {
			classd.type = makeType(classd.canonical_name, false);
			classd.type.decl = classd;
			ASourceFile f=(ASourceFile)classd.getAncestor(ASourceFile.class);	
			String clName=classd.getName().getText();						    		      
			if(f.nameToImNames.containsKey(clName) && !(f.nameToImNames.get(clName)+"."+clName).equals(classd.canonical_name)){
				String out=f.nameToImNames.get(classd.getName().getText())+"."+clName;
				errorSingleTypeImportClashWithClass(classd,out);
			}
		}
	public @Override 
		void inAProgram(AProgram program){
			List<ASourceFile> sfs=program.getSourceFiles();	
			for(ASourceFile f:sfs){	
				List<PImportDecl> impd=f.getImports();
				f.nameToImNames=new HashMap<String,String>();	
				f.dmnd=new HashMap<String,AOnDemandImportDecl>();
			for(PImportDecl p:impd){	
			    
				if(p.kindPImportDecl()==EImportDecl.ON_DEMAND){
						String s=p.getName().nameText();
						if(!ClassEnvironment.packageExists(s) && !findPackageInEnvironment(s,program))
							errorNonExistingPackage((AOnDemandImportDecl)p);
						((AOnDemandImportDecl)p).checked=false;
						if(!f.dmnd.containsKey(s))
							f.dmnd.put(s,(AOnDemandImportDecl)p);			
					}
					else{
					    checkPackage(p.getName());
					    TIdentifier li=((ASingleImportDecl)p).getIdentifier();
					    String ss=p.getName().nameText();	
					    if(Environments.lookupNamedType(ss+"."+li.getText())==null)
						errorUnresolvedType((ASingleImportDecl)p);		    
					    String name=li.getText();
					    if(f.nameToImNames.containsKey(name) && !f.nameToImNames.get(name).equals(ss))
						errorTwoSingleTypeImportsClash((ASingleImportDecl)p,f.nameToImNames.get(name));					    
					    f.nameToImNames.put(name,ss);
					}		    		
				}
				}
			}
	
	private boolean findPackageInEnvironment(String s,AProgram p){	    
	    StringTokenizer names=new StringTokenizer(s,".");		    
	    String curPrefix="";
	    while(names.hasMoreTokens()){		
		curPrefix+=names.nextToken();
		for(String cur:p.type_env.keySet()){		
		    if(cur.length()>curPrefix.length() && cur.indexOf(curPrefix)==0) return true;
		}
		curPrefix+=".";	
	    }	
			    			    
	    return false;
	}

	public @Override void inAPackageDecl(APackageDecl decl){
	    List<TIdentifier> out=new LinkedList<TIdentifier>();
	    StringTokenizer st=new StringTokenizer(decl.getName().nameText(),".");
	    out.add(new TIdentifier(st.nextToken()));
	    while(st.hasMoreTokens()){
		String cur=st.nextToken();
		/*if(!st.hasMoreTokens())
		  break;*/
		out.add(new TIdentifier(cur));
	    }
	    PName pn=new AQualifiedName(out);
	    checkPackage(pn);
	}

	private static void checkPackage(PName p){
	    StringTokenizer st=new StringTokenizer(p.nameText(),".");
	    String cur=st.nextToken()+".";
	    while(st.hasMoreTokens()){
		cur+=st.nextToken();
		if(Environments.lookupNamedType(cur)!=null){
		    errorPackageClashWithType(p,cur);
		}
		cur+=".";
	    }
	}
	public @Override void inANamedType(ANamedType type) {
		type.decl = linkType(type);
		if (type.decl == null) {
			errorUnresolvedType(type);
		}	
	}

	private static PTypeDecl checkQualified(AQualifiedName n,String qualname){
		String cur="";
		List<TIdentifier> ident=n.getIdentifiers(); 		
		cur+=ident.get(0).getText();		
		if(linkTypeFromIdentifier(ident.get(0))!=null){
		    errorPrefixResolvesToType(n,cur);
		}		
		cur+=".";
		int i=1;
		while(i<ident.size()){		    
		    cur+=ident.get(i).getText();
		    if(Environments.lookupNamedType(cur)!=null && !qualname.equals(cur))
			errorPrefixResolvesToType(n,cur);

		    cur+=".";
		    i++;
		}
		
		return Environments.lookupNamedType(qualname);
	}

	public static PTypeDecl linkType(ANamedType type) {
		switch (type.getName().kindPName()) {
			case QUALIFIED:
				// Fully qualified name already
				return checkQualified((AQualifiedName)type.getName(),type.getName().nameText());
			case SIMPLE:
				TIdentifier id = ((ASimpleName)type.getName()).getIdentifier();
				return linkTypeFromIdentifier(id);
			default:
				throw new InternalCompilerError("Unknown name kind");
		}
	}


	/**
	 * Find the declaration of the named type denoted by the given identifier
	 * according to the rules in Section 6.5.5.1 of the JLS.
	 * 
	 * @param id
	 *            the identifier to look up. Its scope is specified by its
	 *            placement in the AST.
	 * @return the corresponding type declaration, or <code>null</code> if the
	 *         identifier could not be resolved as a type.
	 */
	public static PTypeDecl linkTypeFromIdentifier(TIdentifier id) {
		String tID=id.getText();
		AProgram m = (AProgram)id.getAncestor(AProgram.class);
		ASourceFile sf = (ASourceFile)id.getAncestor(ASourceFile.class);
		AClassTypeDecl cl = (AClassTypeDecl)id.getAncestor(AClassTypeDecl.class);
		if(tID.equals(cl.getName().getText()))
			return Environments.lookupNamedType(cl.canonical_name);
	    
		String decl2=sf.nameToImNames.get(tID);

		PTypeDecl ret=null;
		if(decl2!=null){	    		
			ret=Environments.lookupNamedType(decl2+"."+tID);
			if(ret!=null)
				return ret;
			ret=m.type_env.get(decl2+"."+tID);
			if(ret!=null)
				return ret;	
		}
	
		APackageDecl decl=sf.getPackage();
		if(decl!=null){
			PTypeDecl de=Environments.lookupNamedType(decl.getName().nameText()+"."+tID);
			if(de!=null)return de;
		}
		else{
		    ret=m.type_env.get(tID);
		    if(ret!=null) return ret;
		}
	    	    
		boolean found=false;
		PTypeDecl res=null;
		
		for(String p: sf.dmnd.keySet()){
			ret=Environments.lookupNamedType(p+"."+tID);
			if(ret!=null){
			    if(!sf.dmnd.get(p).checked){
				checkPackage(sf.dmnd.get(p).getName());
				sf.dmnd.get(p).checked=true;
			    }
				if(found){
					errorAmbiguousClassName(id);
				}				
				res=ret;
				found=true;
			
			}
		}
				
		if(!sf.dmnd.containsKey("java.lang")){
		    ret=Environments.lookupNamedType("java.lang."+tID);
		    if(ret!=null){		     
			if(found)
			    errorAmbiguousClassName(id);
			return ret;
		    }			 
		}
		if(res!=null)
		    return res;
		return null;
	}



	// /////////////////////////////////////////////////////////////////////////
	// ERROR MESSAGES
	// /////////////////////////////////////////////////////////////////////////

	/**
	 *	Reports the error that a simple name type doesn't resolve unique to a
	 *	type. 
	 *	@param	node	the identifier holding the name of the ambiguous class 
	 */
	@SuppressWarnings("unused")
		private static void errorAmbiguousClassName(TIdentifier node) {
			Errors.error(ErrorType.AMBIGUOUS_CLASS_NAME, node, 
					"Ambiguous class name " + node.getText(), false);
		}

	/**
	 *	Reports the error that a non-existing package is refered in an
	 *	import-on-demand declaration.
	 *	@param	node	the import-on-demand declaration 
	 */
	@SuppressWarnings("unused")
		private static void errorNonExistingPackage(AOnDemandImportDecl node) {
			Errors.error(ErrorType.NON_EXISTING_PACKAGE,node.getName().getToken(), 
					"Package " + node.getName().nameText() + " does not exist", 
					false);
		}

	/**
	 *	Reports the error that a prefix of a package name clashes with the 
	 *	name of a type.
	 *	@param	name	the package name
	 *	@param	prefix	the clashing prefix 
	 */
	@SuppressWarnings("unused") 
		private static void errorPackageClashWithType(PName name, 
				String prefix) {
			Errors.error(ErrorType.PACKAGE_CLASH_WITH_TYPE,
					name.getToken(), 
					"Prefix " + prefix + " of package " + 
					name.nameText() + 
					" clashes with type of the same name.", false);
		}

	/**
	 * 	Reports the error that a prefix of a type name resolves to a type.
	 * 	@param	node	the full type name
	 *	@param	prefix	the prefix which resolves to a type
	 */
	@SuppressWarnings("unused")
		private static void errorPrefixResolvesToType(AQualifiedName node, 
				String prefix) {
			Errors.error(ErrorType.PREFIX_RESOLVES_TO_TYPE, node.getToken(), 
					"Prefix " + prefix + " of type name " + node.nameText() + 
					" is itself a valid type", false);
		}

	/**
	 * 	Reports the error that the name of declared class (or interface) clashes
	 * 	with a single-type-import declaration.
	 *	@param	node		the type declaration
	 *	@param	importName	the name of the imported type 
	 */
	@SuppressWarnings("unused")
		private static void errorSingleTypeImportClashWithClass(PTypeDecl node, 
				String importName) {
			Errors.error(ErrorType.SINGLE_TYPE_IMPORT_CLASH_WITH_CLASS, 
					node.getName(), "Name clash with imported type "+ importName, 
					false);
		}

	/**
	 *	Reports the error that two single-type-imports clash.
	 *	@param	node			one of the single-type-import declarations
	 *	@param	otherImportName	the name of the other imported type
	 */
	@SuppressWarnings("unused")
		private static void errorTwoSingleTypeImportsClash(ASingleImportDecl node, 
				String otherImportName) {
			Errors.error(ErrorType.TWO_SINGLE_TYPE_IMPORTS_CLASH, 
					node.getName().getToken(), "Name clash between " + 
					node.getName().nameText() + " and " + otherImportName, false);
		}

	/**
	 * 	Reports the error that a named type doesn't resolve to a type defined in
	 * 	the program or in the class library.
	 *	@param	node	the named type
	 */
	@SuppressWarnings("unused")
		private static void errorUnresolvedType(ANamedType node) {
			Errors.error(ErrorType.UNRESOLVED_TYPE, node.getName().getToken(), 
					"Could not find type "+node.getName().nameText(), false);
		}

	/**
	 * 	Reports the error that the named type of a single-type-import doesn't
	 *	resolve to a type in the program or in the class library.
	 *	@param	single_type_import	the single-type-import declaration 
	 */
	@SuppressWarnings("unused")
		private static void errorUnresolvedType(ASingleImportDecl single_type_import) {
			String type_name = single_type_import.getName().nameText() + "." + 
				single_type_import.getIdentifier().getText();
			Errors.error(ErrorType.UNRESOLVED_TYPE, 
					single_type_import.getIdentifier(), 
					"Could not find type " + type_name, false);
		}

}
