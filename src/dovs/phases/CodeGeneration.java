package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import dovs.instructions.*;

import java.util.*;

/** Compiler phase to generate Java bytecode for all
 *  method bodies.<p>
 */
public aspect CodeGeneration extends DepthFirstAdapter {
   /** The list of instructions generated for this body */
   public List<Instruction> ABody.instructions;

   /** The list of instructions for the body currently being generated */
   private List<Instruction> current_instrs;

   /** Puts an instruction into the current list */
   private void put(Instruction inst) {
      current_instrs.add(inst);
   }


   public @Override void inABody(ABody body) {
      // Make list of instructions for method
      body.instructions = new LinkedList<Instruction>();
      current_instrs = body.instructions;
   }

   public @Override void caseAFieldDecl(AFieldDecl fieldd) {
      // Inhibit direct traversal of field declarations
   }

   public @Override void outALocalDecl(ALocalDecl locald) {
      // If the local variable being declared has an
      // initializer, code has just been generated
      // for it. Store the result into the variable.
      if (locald.getInit() != null) {
         switch (locald.getType().kindPType()) {
            case BYTE:
            case SHORT:
            case INT:
            case CHAR:
            case BOOLEAN:
               put(new Iistore(locald.index));
               break;
            case NAMED:
            case ARRAY:
               put(new Iastore(locald.index));
               break;
            default:
               throw new InternalCompilerError("Illegal type of local initializer");
         }
      }
   }

   public @Override 
      void caseAIfThenStm(AIfThenStm stm){
         Label label=Label.make("false");
         stm.getCondition().apply(this);
         put(new Iif(Condition.EQ,label));
         stm.getBody().apply(this);
         put(new Ilabel(label));	
      }
   public @Override 
      void caseAIfThenElseStm(AIfThenElseStm stm){
         Label falselabel=Label.make("false");
         Label endiflabel=Label.make("endif");
         stm.getCondition().apply(this);
         put(new Iif(Condition.EQ,falselabel));
         stm.getThenBody().apply(this);
         put(new Igoto(endiflabel));
         put(new Ilabel(falselabel));
         stm.getElseBody().apply(this);
         put(new Ilabel(endiflabel));
         put(new Inop());
      }
   public @Override 
      void caseAWhileStm(AWhileStm stm){
         Label looplabel=Label.make("loop");
         Label condlabel=Label.make("cond");
         // isConstant() == true => while(true), da while(false)
         // vil vaere fanget i TypeChecking da det vil producere
         // unreachable code.
         if(stm.getCondition().isConstant()){
            put(new Ilabel(looplabel));
            stm.getBody().apply(this);
            put(new Igoto(looplabel));
         } else {
            put(new Igoto(condlabel));
            put(new Ilabel(looplabel));
            stm.getBody().apply(this);
            put(new Ilabel(condlabel));
            stm.getCondition().apply(this);
            put(new Iif(Condition.NE,looplabel));					
         }
      }

   /* Denne skal laves faerdig hvis der skal laves Joos2!!!
      public @Override 
      void caseAThisStm(AThisStm stm){
      put(Iaload(0));
      for(PExp arg:stm.getArgs()){
      arg.apply(this);
      }
      PTypeDecl host=stm
      put(new Iinvokespecial())	
      }
      */

   public @Override 
      void caseASuperStm(ASuperStm stm){
         PTypeDecl host=stm.getAncestor(PTypeDecl.class);
         if(host instanceof AClassTypeDecl){
            AClassTypeDecl hostclass=(AClassTypeDecl)host;	   
            put(new Iaload(0));
            for(PExp arg:stm.getArgs()){
               arg.apply(this);
            }
            put(new Iinvokespecial(new MethodSignature(stm.constructor_decl.getSignature())));

            for(AFieldDecl fielddecl: host.getDescendants(AFieldDecl.class)){
               if(fielddecl.getInit()!=null){
                  put(new Iaload(0));
                  fielddecl.getInit().apply(this);
                  put(new Iputfield(new FieldSignature(fielddecl.getSignature(),fielddecl.getType().getSignature())));
               }
            }
            }
         }

         public @Override 
            void caseACastExp(ACastExp exp){
               exp.getExp().apply(this);
               switch(exp.getType().kindPType()){
                  case CHAR:
                     put(new Ii2c());
                     break;
                  case BYTE:
                     put(new Ii2b());
                     break;
                  case SHORT:
                     put(new Ii2s());
                     break;
                  case ARRAY:
                       put(new Icheckcast(new TypeSignature(((AArrayType)exp.type).getSignature()))); 
                       break;
                  case NAMED:
                     if(!((ANamedType)exp.type).decl.canonical_name.equals("java.lang.Object")){
                        put(new Icheckcast(new TypeSignature(((ANamedType)exp.type).decl.getSignature())));
                     }
                     break;
               }
            }

         public @Override 
            void caseAInstanceofExp(AInstanceofExp exp){
               exp.getExp().apply(this);
               switch (exp.getType().kindPType()){
                case NAMED:
                   put(new Iinstanceof(new TypeSignature(((ANamedType)exp.getType()).decl.getSignature())));
                   break;
                case ARRAY:
                   put(new Iinstanceof(new TypeSignature(((AArrayType)exp.getType()).getSignature())));
                   break;
            }	
            }



         public @Override void outAExpStm(AExpStm stm) {
            // A non-void expression statement leaves a
            // value on the stack. This value is not used
            // and so must be popped.
            switch (stm.getExp().type.kindPType()) {
               case VOID:
                  break;
               default:
                  put(new Ipop());
            }
         }


         public @Override void outAVoidReturnStm(AVoidReturnStm stm) {
            put(new Ireturn());
         }

         public @Override void outAValueReturnStm(AValueReturnStm stm) {
            switch (stm.getExp().type.kindPType()) {
               case BYTE:
               case SHORT:
               case INT:
               case CHAR:
               case BOOLEAN:
                  put(new Iireturn());
                  break;
               case NAMED:
               case ARRAY:
               case NULL:
                  put(new Iareturn());
                  break;
               default:
                  throw new InternalCompilerError("Illegal type of return expression");
            }
         }

         public @Override
            void caseAUnopExp(AUnopExp exp){
               switch(exp.getUnop().kindPUnop()){
                  case NEGATE:
                     exp.getExp().apply(this);
                     put(new Iineg());
                     break;
                  case COMPLEMENT:
                     exp.getExp().apply(this);
                     Label endlabel=Label.make("end");
                     Label truelabel=Label.make("true");
                     put(new Iif(Condition.EQ,truelabel));
                     put(new Ildc_int(0));
                     put(new Igoto(endlabel));
                     put(new Ilabel(truelabel));
                     put(new Ildc_int(1));
                     put(new Ilabel(endlabel));
                     break;
                  case BOOLEAN_TO_STRING:
                     exp.getExp().apply(this);
                     put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append(Z)Ljava/lang/StringBuffer;")));

                     break;
                  case INT_TO_STRING:
                  case SHORT_TO_STRING:
                  case BYTE_TO_STRING:
                     exp.getExp().apply(this);
                     put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;")));
                     break;
                  case CHAR_TO_STRING:
                     exp.getExp().apply(this);
                     put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;")));
                     break;
                  case OBJECT_TO_STRING:
                     exp.getExp().apply(this);
                     put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;")));

                     break;
                  default:
                     exp.getExp().apply(this);
                     break;
               }
            }

         public @Override 
            void caseABinopExp(ABinopExp exp){
               switch(exp.getBinop().kindPBinop()){
                  case CONCAT:
                     if(!(exp.parent() instanceof ABinopExp) ||((ABinopExp)exp.parent()).getBinop().kindPBinop() != EBinop.CONCAT){
                        put(new Inew(new TypeSignature("java/lang/StringBuffer")));
                        put(new Idup());
                        put(new Iinvokespecial(new MethodSignature("java/lang/StringBuffer/<init>()V")));
                     }
                     exp.getLeft().apply(this);

                     if(exp.getLeft().kindPExp() !=EExp.UNOP && exp.getLeft().kindPExp() != EExp.BINOP)
                        put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append("+exp.getLeft().type.getSignature()+")Ljava/lang/StringBuffer;")));

                     exp.getRight().apply(this);

                     if(exp.getRight().kindPExp()!=EExp.UNOP && exp.getRight().kindPExp() != EExp.BINOP){
                        put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/append("+exp.getRight().type.getSignature()+")Ljava/lang/StringBuffer;")));
                     }
                     if(!(exp.parent() instanceof ABinopExp)||((ABinopExp)exp.parent()).getBinop().kindPBinop() != EBinop.CONCAT)
                        put(new Iinvokevirtual(new MethodSignature("java/lang/StringBuffer/toString()Ljava/lang/String;")));

                     break;

                  case PLUS:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     put(new Iiadd());break;    	

                  case MINUS:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     put(new Iisub());break;
                  case DIVIDE:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     put(new Iidiv());break;
                  case TIMES:	
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     put(new Iimul());break;
                  case MODULO:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     put(new Iirem());break;
                  case LAZY_OR:
                     exp.getLeft().apply(this);
                     put(new Idup());
                     Label firsttrue=Label.make("firsttrue");
                     put(new Iif(Condition.NE,firsttrue));
                     put(new Ipop());
                     exp.getRight().apply(this);
                     put(new Ilabel(firsttrue));break;	
                  case LAZY_AND:
                     exp.getLeft().apply(this);
                     put(new Idup());
                     Label firstfalse=Label.make("firstfalse");
                     put(new Iif(Condition.EQ,firstfalse));
                     put(new Ipop());
                     exp.getRight().apply(this);
                     put(new Ilabel(firstfalse));break;
		case OR:
			exp.getLeft().apply(this);
			exp.getRight().apply(this);
			put(new Iior());break;
		case AND:
			exp.getLeft().apply(this);
			exp.getRight().apply(this);
			put(new Iiand());break;		
                  case GE:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.GE);
                     break;
                  case GT:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.GT);
                     break;
                  case LT:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.LT);
                     break;	
                  case EQ:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.EQ);	
                     break;
                  case AEQ:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.AEQ);
                     break;
                  case LE:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.LE);
                     break;
                  case NE:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.NE);
                     break;
                  case ANE:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
                     makeConditionalJump(Condition.ANE);
                     break;
                  default:
                     exp.getLeft().apply(this);
                     exp.getRight().apply(this);
               }
            }

         private void makeConditionalJump(Condition ll){
            Label endlabel=Label.make("end");
            Label truelabel=Label.make("true");
            put(new Iifcmp(ll,truelabel));
            put(new Ildc_int(0));
            put(new Igoto(endlabel));
            put(new Ilabel(truelabel));
            put(new Ildc_int(1));
            put(new Ilabel(endlabel));
         }

         public @Override void caseAIntConstExp(AIntConstExp exp) {
            put(new Ildc_int(exp.value));
         }

         public @Override 
            void caseAAssignmentExp(AAssignmentExp exp){
               switch(exp.getLvalue().kindPLvalue()){	    
                  case ARRAY:
                     AArrayLvalue lvalue=(AArrayLvalue)exp.getLvalue();
                     lvalue.getBase().apply(this);
                     lvalue.getIndex().apply(this);
                     exp.getExp().apply(this);
                     put(new Idup_x2());
                     switch(lvalue.type.kindPType()){
                        case NAMED:
                              put(new Iaastore());
                           break;
                        case SHORT:
                           put(new Isastore());break;
                        case CHAR:
                           put(new Icastore());break;
                        case INT:
                           put(new Iiastore());break;
                        case BOOLEAN:
			case BYTE:
                           put(new Ibastore());break;	    	   	   
                     }	
                     break;

                  case NONSTATIC_FIELD:
                     exp.getLvalue().apply(this);
                     exp.getExp().apply(this);
                     put(new Idup_x1());
                     put(new Iputfield(new FieldSignature(((ANonstaticFieldLvalue)exp.getLvalue()).field_decl.getSignature(), ((PLvalue)exp.getLvalue()).type.getSignature())));
                     break;
                  case STATIC_FIELD:
                     exp.getExp().apply(this);
                     put(new Idup());
                     put(new Iputstatic(new FieldSignature(((AStaticFieldLvalue)exp.getLvalue()).field_decl.getSignature(), ((PLvalue)exp.getLvalue()).type.getSignature())));
                     break;

                  case LOCAL:
                     exp.getExp().apply(this);
                     put(new Idup());
                     ALocalLvalue exp2=(ALocalLvalue)exp.getLvalue();
                     switch(exp2.type.kindPType()){
                        case NAMED:
                        case ARRAY:   
                           put(new Iastore(exp2.local_decl.index));break;
                        default:
                           put(new Iistore(exp2.local_decl.index));
                     } break;

                  default:

               }
            } 

         public @Override void
            caseAArrayLvalue(AArrayLvalue lvalue){
               lvalue.getBase().apply(this);
               lvalue.getIndex().apply(this);
               switch(lvalue.type.kindPType()){
                  case NAMED:                    
                        put(new Iaaload());
                     break;		
                  case CHAR:
                     put(new Icaload());break;
                  case SHORT:
                     put(new Isaload());break;   
                  case INT:
                     put(new Iiaload());break;
		  case BYTE:	
                  case BOOLEAN:
                     put(new Ibaload());break;	    	   	   
               }	
            }	

         public @Override void caseAArrayLengthExp(AArrayLengthExp exp){
            exp.getExp().apply(this);	
            put(new Iarraylength());
         }

         public @Override 
            void caseANonstaticFieldLvalue(ANonstaticFieldLvalue lvalue){
               lvalue.getExp().apply(this);
               if(!(lvalue.parent() instanceof AAssignmentExp))
                  put(new Igetfield(new FieldSignature(lvalue.field_decl.getSignature(),lvalue.type.getSignature())));	
            }

         public @Override 
            void caseAStaticFieldLvalue(AStaticFieldLvalue lvalue){
               put(new Igetstatic(new FieldSignature(lvalue.field_decl.getSignature(),lvalue.type.getSignature())));
            }

         public @Override
            void caseAStringConstExp(AStringConstExp exp){
               String s = exp.value;
               put(new Ildc_string(s));
            }
         public @Override
            void caseANewArrayExp(ANewArrayExp exp){
               for(PExp size:exp.getSizes())
                  size.apply(this);	
               put(new Imultianewarray(new TypeSignature(exp.getType().getSignature()),1));
            }
         public @Override void caseABooleanConstExp(ABooleanConstExp exp) {
            put(new Ildc_int(exp.value ? 1 : 0));
         }

         public @Override void caseANullExp(ANullExp exp) {
            put(new Iaconst_null());
         }

         public @Override void caseAThisExp(AThisExp exp) {
            put(new Iaload(0));
         }

         public @Override
            void caseALocalLvalue(ALocalLvalue lvalue){
               switch(lvalue.local_decl.getType().kindPType()){
                  case NAMED:
                  case ARRAY:
                     put(new Iaload(lvalue.local_decl.index));
                     break;
                  default:
                     put(new Iiload(lvalue.local_decl.index));	
               }
            }
         public @Override void outAStaticInvokeExp(AStaticInvokeExp exp) {
            put(new Iinvokestatic(new MethodSignature(exp.method_decl.getSignature())));
         }

         public @Override void outANonstaticInvokeExp(ANonstaticInvokeExp exp) {
            if (exp.method_decl.parent() instanceof AInterfaceTypeDecl) {
               put(new Iinvokeinterface(new MethodSignature(exp.method_decl.getSignature())));
            } else {
               put(new Iinvokevirtual(new MethodSignature(exp.method_decl.getSignature())));
            }
            }

         public @Override void caseANewExp(ANewExp exp) {
            put(new Inew(new TypeSignature(exp.getType().decl.getSignature())));
            put(new Idup());
            for (PExp arg : (List<PExp>)exp.getArgs()) {
               arg.apply(this);
            }
            put(new Iinvokespecial(new MethodSignature(exp.constructor_decl.getSignature())));
         }

         }
