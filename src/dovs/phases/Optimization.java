
package dovs.phases;

import dovs.node.*;
import dovs.analysis.*;

import dovs.peephole.*;

public aspect Optimization extends DepthFirstAdapter {
    private PeepholeDriver driver;

    public @Override void inAProgram(AProgram program) {
	if (dovs.Main.peephole_patterns != null) {
	    driver = new PeepholeDriver(dovs.Main.peephole_patterns);
	} else {
	    driver = null;
	}
    }

    public @Override void caseABody(ABody body) {
	if (driver != null) {
	    driver.optimize(body.instructions);
	}
    }

}
