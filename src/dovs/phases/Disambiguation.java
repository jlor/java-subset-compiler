package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import java.util.*;

/** Compiler phase to disambiguate ambiguous uses of names and link
 *  the uses of local variables to their definitions.
 */
public aspect Disambiguation extends DepthFirstAdapter {
   /** The declaration of the local variable referenced in this lvalue */
   public ALocalDecl ALocalLvalue.local_decl;
   public String Node.scope;
   public @Override 
      void inAClassTypeDecl(AClassTypeDecl decl){
	 decl.scope="";
	 int i=1;
	 for(PDecl pd:decl.getMembers()){
	    setScope(pd,""+i);
	    i++;
	 }			  
      }

   private void setScope(Node pd,String str){
      pd.scope=str;int i=1;
      for(Node n:pd.getChildren(Node.class)){
	 setScope(n,str+"."+i);
	 i++;
      }
   }	
   private int compareScopes(String a, String b){
      StringTokenizer st1=new StringTokenizer(a,"."),st2=new StringTokenizer(b,".");   	
      while(st1.hasMoreTokens() && st2.hasMoreTokens()){
	 int i1=Integer.parseInt(st1.nextToken());	
	 int i2=Integer.parseInt(st2.nextToken());	
	 if(i1<i2){
	    return -1;		       	       	       
	 }
      }	
      return 1;
   }		

   public @Override
      void caseAAmbiguousNameLvalue(AAmbiguousNameLvalue lvalue) {
	 PName name = lvalue.getAmbiguous();
	 PLvalue rep = disambiguateName(name);
	 SOP("Ambiguate name: " + name);
	 if (rep != null) {
	    lvalue.replaceBy(rep);
	 } else {
	    errorVariableNotFound(name);
	 }
      }

   public @Override void outAAmbiguousInvokeExp(AAmbiguousInvokeExp exp) {
      SOP("Jabber!");
      PName name = exp.getAmbiguous();

      // Try as lvalue
      PLvalue lvalue = disambiguateName(name);
      if (lvalue != null) {
	 ALvalueExp receiver = new ALvalueExp(lvalue);
	 ANonstaticInvokeExp invoke = new ANonstaticInvokeExp(receiver, exp.getName(), exp.getArgs());
	 exp.replaceBy(invoke);
	 return;
      }
      // Try as TypeDecl
      PTypeDecl pt=null;
      if(name instanceof ASimpleName){
	 pt=TypeLinking.linkTypeFromIdentifier( ((ASimpleName)name).getIdentifier());
      }else{
	 List<TIdentifier> tin=name.getIdentifiers();
	 String cur=tin.get(0).clone().getText();
	 TIdentifier cc=tin.get(0).clone();
	 pt=TypeLinking.linkTypeFromIdentifier(tin.get(0));
	 tin.get(0).replaceBy(cc);
	 int i=1;
	 while(pt==null && i<tin.size()){
	    cur+="."+tin.get(i).clone().getText();
	    pt=Environments.lookupNamedType(cur);i++;		
	 }
       
	 // Preserved for hilarious purposes.
	 //if(i!=i.size())
	 //   pt=null;
	 /*
	    if(i!=tin.size())
	    pt=null;*/
      }

      if(pt!=null){
	 AStaticInvokeExp invoke = new AStaticInvokeExp(pt.type.clone(),exp.getName(),exp.getArgs());
	 exp.replaceBy(invoke);
	 return;
      }

      errorVariableOrTypeNotFound(name); 

   }
   private PLvalue disambiguateName(PName name) {

      if(name.kindPName()==EName.QUALIFIED){
	 return disambiguateQualified((AQualifiedName) name);
      }else{
	 return disambiguateSimple((ASimpleName) name);
      }
   }

   private ALocalDecl goodOne(ABlock b,String str,String ascope){
      if(b==null)
	 return null;
      if(b.local_env.containsKey(str)){
	 ALocalDecl d=b.local_env.get(str);
	 if(d.parent() instanceof ALocalDeclStm){
	    d.scope=d.parent().scope;
	 } 
	 String sco=d.scope;
	 StringTokenizer st1=new StringTokenizer(sco,"."),st2=new StringTokenizer(ascope,".");
	 while(st1.hasMoreTokens() && st2.hasMoreTokens()){
	    int i1=Integer.parseInt(st1.nextToken());	
	    int i2=Integer.parseInt(st2.nextToken());	
	    if(i1!=i2){
	       if(!st1.hasMoreTokens())
		  if(i1<=i2)return d;	
	       Node c=b.parent();
	       return goodOne(c.getAncestor(ABlock.class),str,ascope);
	    }
	 }						   
	 return d;

      }
      Node c=b.parent();
      return goodOne(c.getAncestor(ABlock.class),str,ascope);		
   }
   private void SOP(String s){
      //System.out.println(s);
   }

   private boolean checkForward(AFieldDecl d, AFieldDecl fdecl,TIdentifier name){
      if(compareScopes(fdecl.scope,d.scope)<0 || d.scope.equals(fdecl.scope)){
	 Node n=name.getAncestor(AAssignmentExp.class);
	 if(n !=null){
	    PLvalue lv=((AAssignmentExp)n).getLvalue();
	    String str=lv.toString().trim();
	    if(!str.equals(name.getText())){
	       return false;
	    }

	 }else{
	    return false;                
	 }
      }
      return true;
   }

   private PLvalue disambiguateSimple(ASimpleName name){

      SOP("==SIMPLE=============");
      SOP("name.getIdentifier(): "+name.getIdentifier());
      SOP("name.getIdentifier().kindToken(): "+name.getIdentifier().kindToken());
      SOP("=====================");

      ABlock bl= name.getAncestor(ABlock.class);
      ALocalDecl good=goodOne(bl,name.getIdentifier().getText(),name.scope);	

      if(good!=null){
	 ALocalLvalue v	= new ALocalLvalue(name.getIdentifier().clone());
	 v.local_decl=good;
	 v.scope=name.scope;
	 return v;
      }	
      PTypeDecl host=name.getAncestor(PTypeDecl.class);
      AFieldDecl d=Hierarchy.lookupField(host,name.getIdentifier());
      if(d!=null){
	 AFieldDecl fdecl=name.getAncestor(AFieldDecl.class);
	 if(fdecl != null){
	    if(!checkForward(d,fdecl,name.getIdentifier()))
	       errorIllegalForwardFieldReference(fdecl);


	 }
	 if (d.getStatic() == null){
	    return new ANonstaticFieldLvalue(new AThisExp(new TThis(name.getToken().getLine(),name.getToken().getPos())),name.getIdentifier().clone());
	 } else{
	    SOP("BRAELLER!");
	    checkJoos1ImplicitThisClassStaticField(name.getIdentifier());
	 }
      }
      return null;
   }

   private PLvalue makeChainNonStatic(PLvalue pl,List<TIdentifier> ll,int i){
      if(ll.size()==i) return pl;
      ANonstaticFieldLvalue n=new ANonstaticFieldLvalue(new ALvalueExp(pl),ll.get(i).clone());
      return makeChainNonStatic(n,ll,i+1);	
   }

   private PLvalue disambiguateQualified(AQualifiedName name){
      ABlock bl= name.getAncestor(ABlock.class);
      PTypeDecl host=name.getAncestor(AClassTypeDecl.class);
      List<TIdentifier> idents=name.getIdentifiers();

      SOP("==DISAMBIG===========");
      SOP("idents empty? "+idents.isEmpty());
      if(!idents.isEmpty()){
	 SOP("idents.get(0).kindToken(): "+idents.get(0).kindToken());
      } else {
	 SOP("BAH!!"+name);
      }

      SOP("=====================");

      if(!idents.isEmpty() && idents.get(0).kindToken() == TokenEnum.STATIC){
	 checkJoos1ImplicitThisClassStaticField(idents.get(0));
      }
      ALocalDecl val=goodOne(bl,idents.get(0).clone().getText(),name.scope);
      if(val!=null){
	 ALocalLvalue l= new ALocalLvalue(idents.get(0).clone());
	 l.local_decl=val;
	 return makeChainNonStatic(l,idents,1);
      }

      AFieldDecl d=Hierarchy.lookupField(host,idents.get(0).clone());
      if(d!=null){
	 AFieldDecl fdecl=name.getAncestor(AFieldDecl.class);
	 if(fdecl!=null){
	    if(!checkForward(d,fdecl,idents.get(0)))
	       errorIllegalForwardFieldReference(fdecl);
	 }
	 ANonstaticFieldLvalue fl=new ANonstaticFieldLvalue(new AThisExp(new TThis(idents.get(0).getLine(),idents.get(0).getPos())),idents.get(0).clone());
	 return makeChainNonStatic(fl,idents,1);
      }

      PTypeDecl td=null;

      int i=1;
      if(td==null){
	 TIdentifier n=idents.get(0);
	 TIdentifier nn=n.clone();
	 td=TypeLinking.linkTypeFromIdentifier(n);
	 if(td!=null)
	 n.replaceBy(nn);

	 String tempname;
	 tempname=idents.get(0).clone().getText();
	 i=1;
	 while(i<idents.size() && td==null){	
	    tempname+="."+idents.get(i).clone().getText();
	    td=Environments.lookupNamedType(tempname);
	    i++;
	 }

	 if(td==null || i==idents.size())
	    return null;
      }
      AStaticFieldLvalue asfa=new AStaticFieldLvalue(td.type.clone(),idents.get(i).clone());
      return makeChainNonStatic(asfa,idents,i+1); 
   }


   // /////////////////////////////////////////////////////////////////////////
   // ERROR MESSAGES
   // /////////////////////////////////////////////////////////////////////////

   /**
    * Reports the error of an illegal forward reference of a nonstatic field.
    * 
    * @param field
    *            the field declaration of the refered field
    */
   @SuppressWarnings("unused")
      private static void errorIllegalForwardFieldReference(AFieldDecl field) {
	 Errors.error(ErrorType.ILLEGAL_FORWARD_FIELD_REFERENCE,
	       field.getName(), "Illegal forward reference of instance field "
	       + field.getName().getText(), false);
      }

   /**
    * Reports the error of an illegal forward reference of a static field.
    * 
    * @param field
    *            the field declaration of the refered field
    */
   @SuppressWarnings("unused")
      private static void errorIllegalForwardStaticFieldReference(AFieldDecl field) {
	 Errors.error(ErrorType.ILLEGAL_FORWARD_STATIC_FIELD_REFERENCE, field
	       .getName(), "Illegal forward reference of static field "
	       + field.getName().getText(), false);
      }

   /**
    * If compiling with the {@code -joos1} option, reports the error that
    * implicit this class is used for static fields.
    * 
    * @param field_id
    *            the identier holding the field name
    */
   @SuppressWarnings("unused")
      private static void checkJoos1ImplicitThisClassStaticField(
	    TIdentifier field_id) {
	 Errors.checkJoos1(ErrorType.JOOS1_IMPLICIT_THIS_CLASS_STATIC_FIELD,
	       field_id, "Implicit this class for static fields");
	    }

   /**
    * Reports the error that a variable could not be found through the
    * disambiguation rules.
    * 
    * @param name
    *            the name of the variable
    */
   @SuppressWarnings("unused")
      private static void errorVariableNotFound(PName name) {
	 Errors.error(ErrorType.VARIABLE_NOT_FOUND, name.getToken(),
	       "Could not find variable " + name.nameText(), false);
      }

   /**
    * Reports the error that a name could not resolved to neither a variable
    * nor a type through the disambiguation rules.
    * 
    * @param name
    *            the name of the variable or type
    */
   @SuppressWarnings("unused")
      private static void errorVariableOrTypeNotFound(PName name) {
	 Errors.error(ErrorType.VARIABLE_OR_TYPE_NOT_FOUND, name.getToken(),
	       "Could not find variable or type " + name.nameText(), false);
      }

}
