package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import static dovs.Util.*;

import java.util.*;

/** Compiler phase to build maps from names to
 *  AST nodes for all named entities in the program.
 */
public aspect Environments extends DepthFirstAdapter {
	/** The canonical name of the class or interface */
	public String PTypeDecl.canonical_name;

	/** The classes and interfaces in the program */
	public Map<String,PTypeDecl> AProgram.type_env 
	= new HashMap<String,PTypeDecl>();
	/** The fields in the class */
	public Map<String,AFieldDecl> PTypeDecl.field_env
	= new HashMap<String,AFieldDecl>();
	/** The methods in the class */
	public Map<String,Set<AMethodDecl>> PTypeDecl.method_env
	= new HashMap<String,Set<AMethodDecl>>();
	/** The locals and formals in the method */
	public Map<String,ALocalDecl> ABlock.local_env
	= new HashMap<String,ALocalDecl>();
	
	private String findCanonical(Node n){
	    if(n instanceof ASourceFile){
		ASourceFile f=(ASourceFile)n;
		if(f.getPackage()!=null){
		    return f.getPackage().getName().nameText()+".";			
		}
		return "";
	    }else if(n instanceof AClassTypeDecl){
	       	AClassTypeDecl n2=(AClassTypeDecl)n;
		return findCanonical(n2.parent())+""+n2.getName().getText()+".";
	    }
	    return findCanonical(n.parent());
	}
	
	public @Override 
	    void inAClassTypeDecl(AClassTypeDecl decl){
	    String s;
	    s=findCanonical(decl);
	    s=s.substring(0,s.length()-1);
	    decl.canonical_name=s;
	    if(decl.getSuper()==null)
		decl.setSuper(Util.makeType("java.lang.Object",false));	
	    Node dad=decl.parent();
	    AProgram dad2=(AProgram)dad.parent();
	    Set<String> myset=dad2.type_env.keySet();
	     if(myset.contains(s))
		errorDuplicateType(decl);	
	    dad2.type_env.put(s,decl);

	}
	
	public @Override  
	    void inAFieldDecl(AFieldDecl decl){
	    String s=decl.getName().getText();
	    AClassTypeDecl dad=(AClassTypeDecl)decl.parent();
	    Set<String> myset=dad.field_env.keySet();
            if(myset.contains(s)){
		errorDuplicateField(decl);
            }
	    dad.field_env.put(s,decl);
	}
	
	private void inAMethodOrConstructor(Util.MethodOrConstructor decl){
		List<ALocalDecl> formals=decl.getFormals();
		if(decl.getBody()!=null){
		    ABlock body=decl.getBody().getBlock();
		    for(ALocalDecl de:formals){
			String str=de.getName().getText();	
			if(body.local_env.keySet().contains(str))
				errorDuplicateVariable(de);
			else
			    body.local_env.put(str,de);		
		    }
		}
	}
	
	public @Override void inAMethodDecl(AMethodDecl decl){
		String s=decl.getName().getText();
		AClassTypeDecl dad=(AClassTypeDecl)decl.parent();
		if(dad.method_env.keySet().contains(s)){
			dad.method_env.get(s).add(decl);
		}else{
			Set<AMethodDecl> nset=new HashSet<AMethodDecl>();
			nset.add(decl);
			dad.method_env.put(s,nset);	
		}
		inAMethodOrConstructor(decl);	
	}	
	
	public @Override void inAConstructorDecl(AConstructorDecl decl){
		inAMethodOrConstructor(decl);
	}	
	public @Override
	   void inALocalDecl(ALocalDecl ld){
	      	Node par=ld.parent();	
		while(!(par instanceof ABlock)){
			if(par instanceof AProgram)
		   		return;
		   	par=par.parent();			
		}
	   	ABlock myblock=(ABlock)par;
		String s=ld.getName().getText();
		if(blockCheck(s,myblock,ld)){
		   myblock.local_env.put(s,ld);
		}
	   }
	
	private boolean blockCheck(String s,ABlock bl,ALocalDecl decl){
		Node par=bl;
		while(!(par instanceof AProgram)){
			if(par instanceof ABlock){								
				ABlock par2=(ABlock)par;
				if(par2.local_env.keySet().contains(s)){
					errorDuplicateVariable(decl);
					return false;
				}	
			}
			par=par.parent();
		}
		return true;
	}
	/**
	 * Look up a class or interface with the given name by searching first the
	 * program and then the classpath.
	 * 
	 * @param full_name
	 *            the fully qualified name of the class or interface to look
	 *            for.
	 */
	public static PTypeDecl lookupNamedType(String full_name) {
		if (dovs.Main.program.type_env.containsKey(full_name)) {
			return dovs.Main.program.type_env.get(full_name);
		} else {
			return ClassEnvironment.lookupNamedType(full_name);
		}
	}
        
	// /////////////////////////////////////////////////////////////////////////
	// ERROR MESSAGES
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Reports the error that two fields in the same class have the same name.
	 * 
	 * @param node
	 *            one of the field declarations
	 */
	@SuppressWarnings("unused")
	private static void errorDuplicateField(AFieldDecl node) {
		Errors.error(
				ErrorType.DUPLICATE_FIELD, 
				node.getName(), 
				"The field '" + node.getName().getText() + 
				"' is already defined.",
				false
		);
	}

	/**
	 * Reports the error that two types with the same name have been declared.
	 * 
	 * @param node
	 *            one of the type declarations
	 */
	@SuppressWarnings("unused")
	private static void errorDuplicateType(PTypeDecl node) {
		Errors.error(
				ErrorType.DUPLICATE_TYPE, 
				node.getName(), 
				"Duplicate type " + node.canonical_name,
				false
		);
	}

	/**
	 * Reports the error that two variables with overlapping scope have the same
	 * name.
	 * 
	 * @param node
	 *            one of the local declarations
	 */
	@SuppressWarnings("unused")
	private static void errorDuplicateVariable(ALocalDecl node) {
		Errors.error(
				ErrorType.DUPLICATE_VARIABLE, 
				node.getName(), 
				"The variable '" + node.getName() + 
				"' is already defined.",
				false
		);
	}

}
