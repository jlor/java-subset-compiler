package dovs.phases;

import dovs.*;
import dovs.analysis.*;
import dovs.node.*;
import static dovs.Util.*;

import java.util.*;

/**
 * Compiler phase to resolve class hierarchy relationships, inheritance and
 * hiding of fields and inheritance, overriding and hiding of methods according
 * to chapters 8 and 9 of the JLS.
 */
public aspect Hierarchy extends DepthFirstAdapter {

   public boolean PTypeDecl.beingVisited;
   public Map<String,Set<AMethodDecl> > PTypeDecl.allMethods;	
   public Map<String,AFieldDecl> PTypeDecl.allFields;	
   public @Override
      void inAClassTypeDecl(AClassTypeDecl decl){
	 //List mldecl.method_env
	 ANamedType td = decl.getSuper();
	 decl.beingVisited=false;
	 if(td.decl.kindPTypeDecl()!=ETypeDecl.CLASS){
	    errorExtendsNonClass(td); 
	 }
	 if (td.decl.kindPTypeDecl()==ETypeDecl.CLASS && ((AClassTypeDecl)td.decl).isFinal()){
	    errorExtendsFinalClass((AClassTypeDecl)td.decl); 
	 }
	 //check circular dependancy
	 if (isSuperType(decl.type,td)){
	    errorCircularInheritance(td.decl,true);
	 }
	 List ll = decl.getInterfaces();
	 Iterator<ANamedType> it = ll.iterator();
	 Set<String> interfaces = new HashSet<String>();
	 while(it.hasNext()){
	    ANamedType current = it.next();
	    if(current.decl.kindPTypeDecl()!=ETypeDecl.INTERFACE){
	       errorImplementsNonInterface(current);
	    }
	    if (interfaces.contains(current.decl.canonical_name)){
	       errorRepeatedInterface(current);
	    } else {
	       interfaces.add(current.decl.canonical_name);
	    }
	 }
	 decl.allMethods=new HashMap<String,Set<AMethodDecl> >();
	 HashMap<String,Set<AMethodDecl> > t= new HashMap<String,Set<AMethodDecl> >();
	 decl.allMethods=FillUpMethods(decl,t);
	 SOP("HE: ALl methods:"+decl.allMethods);
	 SOP("Method environment: "+decl.method_env);
	 Set<Util.MethodOrConstructor> nset=new HashSet<Util.MethodOrConstructor>();
	 for(String str:decl.method_env.keySet()){
	    nset=new HashSet<Util.MethodOrConstructor>();
	    nset.addAll(decl.method_env.get(str));
	    if(checkSimilar(nset)){
	       Iterator<AMethodDecl> iter=decl.method_env.get(str).iterator();
	       errorDuplicateMethod(iter.next());
	    }
	    if(!decl.isAbstract()){
	       for(AMethodDecl mdecl:decl.method_env.get(str)){
		  if(mdecl.isAbstract()){
		     errorClassMustBeAbstract(decl,mdecl);
		  }
	       }
	    }		    
	 }
         Map<String,AFieldDecl> fnames=new HashMap<String,AFieldDecl>();
         decl.allFields = getFields(decl,fnames);
         SOP("HE: ALL FIELDS"+decl.allFields);
	 for(String str:decl.allMethods.keySet()){
	    Set<AMethodDecl> tempset=decl.allMethods.get(str);
	    Map<List<PType>,Set<AMethodDecl>> methodRetTypes= new HashMap<List<PType>,Set<AMethodDecl>>();    
	    AMethodDecl bad=tempset.iterator().next();
	    for(AMethodDecl dec:tempset){
	       List<PType> types=new ArrayList<PType>();
	       for(ALocalDecl dd:dec.getFormals()){
		  types.add(dd.getType());
	       }	
	       SOP(dec.toString());
	       if(methodRetTypes.containsKey(types)){
		  Set<AMethodDecl> cc=methodRetTypes.get(types);
		  cc.add(dec);
		  methodRetTypes.put(types,cc);
		  Set<PType> seen=new HashSet<PType>();
		  for(AMethodDecl mm:methodRetTypes.get(types)){
		     	PType ptype=mm.getReturnType();
			PTypeDecl dee;
			if(ptype.kindPType()==EType.NAMED){
				dee=Environments.lookupNamedType(((ANamedType)ptype).decl.canonical_name);
				ptype=dee.type;
			}
		  	seen.add(ptype);
		  	if(seen.size()>1){
			 	errorDifferentReturnType(decl,mm,dec);
			}
		  }
	  			  
	       }else{
                SOP("DOES CONTAIN SAME RETURN TYPE");
                   HashSet<AMethodDecl> tt=new HashSet<AMethodDecl>();
		  tt.add(dec);
		  methodRetTypes.put(types,tt);
		  SOP("HE: "+types+" tt "+tt);
	       }
	    }
	 }

	 Set<AConstructorDecl> constructors=new HashSet<AConstructorDecl>();
	 Collection<AConstructorDecl> l=decl.getDescendants(AConstructorDecl.class);
	 constructors.addAll(l);	 	
	 nset=new HashSet<Util.MethodOrConstructor>();	
	 nset.addAll(constructors);
	 if(checkSimilar(nset)){
	    Iterator<AConstructorDecl> iter=constructors.iterator();
	    errorDuplicateConstructor(iter.next());
	 }
      }
        
   private static List<PType> formalTypes(AMethodDecl decl){
        List<PType> res=new LinkedList<PType>();
       for(ALocalDecl an:decl.getFormals()){
               PType cur=an.getType();
               if(cur.kindPType()==EType.NAMED){
                      cur=((ANamedType)cur).decl.type;     
               }
               res.add(cur);
        }
       return res;
   }
        
   private static Map<String,Set<AMethodDecl> > FillUpMethods(PTypeDecl host,Map<String,Set<AMethodDecl>> s){
      //Map<String,Set<AMethodDecl> > s = new HashMap<String,Set<AMethodDecl> >();
      for(String str: host.method_env.keySet()){
	 if(s.containsKey(str)){
             SOP("PROTECT: str: "+str);
             for (AMethodDecl md_123: host.method_env.get(str)){
                 List<PType> mtypes=formalTypes(md_123);
                for(AMethodDecl md_321 :s.get(str)){
                        if (md_321.getAccess().kindPAccess()==EAccess.PUBLIC){
                                SOP("ACCESS PAA DENNE ER: PUBLIC");
                        } else {
                            SOP("ACCESS PAA DENNE ER: PROTECTED:"+md_321);
                            if (md_123.getAccess().kindPAccess() == EAccess.PUBLIC){
                                List<PType> ps=formalTypes(md_321);
                                SOP("ACCESS & FORMALS:"+ps.size());
                                if(mtypes.equals(ps) && md_123.getThrows().equals(md_321.getThrows())){
                                        errorProtectedReplacePublic(host, md_123);
                                }
                            }
                        }

/*                        if(md_321.getAbstract() != null && host.kindPTypeDecl() != ETypeDecl.INTERFACE){
                                    if (((AClassTypeDecl)host).getAbstract() != null){
                                    errorClassMustBeAbstract((AClassTypeDecl)host, md_123); 
                              }
                        }
  */
                }
             }
	    Set<AMethodDecl> temp=s.get(str);
	    temp.addAll(host.method_env.get(str));			    
	    s.put(str,temp);
	 }else{
	    Set<AMethodDecl> de=new HashSet<AMethodDecl>();
	    de.addAll(host.method_env.get(str));
	    s.put(str,de);			     
	    }
     	 }
	 if(host.canonical_name.equals("java.lang.Object")){
                SOP("java lang object : settet indeholder:\n====================================== "+s);
             return s;

         }
	 if(host.kindPTypeDecl()==ETypeDecl.CLASS){
	    AClassTypeDecl cl=(AClassTypeDecl)host;
	    FillUpMethods(Environments.lookupNamedType(cl.getSuper().decl.canonical_name),s);
	    LinkedList<ANamedType> an=cl.getInterfaces();
	    for(ANamedType ctype:an){
	       FillUpMethods(Environments.lookupNamedType(ctype.decl.canonical_name),s);
	    }
	 }else{
	    AInterfaceTypeDecl cl=(AInterfaceTypeDecl)host;	
	    LinkedList<ANamedType> an=cl.getSupers();
	    if(an.size()>0){
	    	for(ANamedType ctype:an){
	       	  FillUpMethods(Environments.lookupNamedType(ctype.decl.canonical_name),s);
	    	}
	    }
	    else{
	    	FillUpMethods(Environments.lookupNamedType("java.lang.Object"),s);
	    }
	 }
      	      
	 return s;	
	 
      }

      private boolean checkSimilar(Set<Util.MethodOrConstructor> me){
	 SOP("HE: me: "+me+" .");
	 Set<List<PType> > curSet=new HashSet<List<PType> >();
	 SOP("HE: curSet: "+curSet);
	 boolean clashFound=false;	
	 for(Util.MethodOrConstructor de:me){
	    SOP("HE: in Method: "+de.getName().getText());		 
	    List<PType> setTypes=new ArrayList<PType>();
	    //PTypeDecl nt=Environments.lookupNamedType("java.lang.Object");
	    //setTypes.add(nt.type);//Vores sentinel-Sikrer mod et helvede af Exceptions
	    for(ALocalDecl ld:de.getFormals()){	    
	       PType typ=ld.getType();
	       if(typ.kindPType()==EType.NAMED){
		  PTypeDecl typede=((ANamedType)typ).decl;
		  typ=typede.type;
	       }
	       SOP("HE: Types: "+typ.toString());
	       setTypes.add(typ);
	    }
	 //   SOP("HE: formals: "+setTypes+" curSet: "+curSet);

	    if(curSet.contains(setTypes)){
	       return true;
	    }else{
	       curSet.add(setTypes);
	    }
	 }
	 return false;
      }
      /**
       * Look up a method by name in a class or interface, taking inheritance into
       * account.
       * 
       * @param host
       *            the receiver type
       * @param id
       *            the name of the method
       * @return all methods of the given name that are declared by or inherited
       *         by the given receiver type.
       */
      public static Set<AMethodDecl> lookupMethod(PTypeDecl host, TIdentifier id) {
	 SOP("HE: lookupMethod, host: "+host+" .");
	 if(host.allMethods==null){
		HashMap<String,Set<AMethodDecl>> temp= new HashMap<String,Set<AMethodDecl>>(); 	 
		SOP("HE: is NULL, calling FillUp!");
    	        host.allMethods=FillUpMethods(host,temp);
	 
	 }
	 	SOP("HE: returns");
		return host.allMethods.get(id.getText());
		
	 /*Set<AMethodDecl> s = new HashSet<AMethodDecl>();
	   if(host.canonical_name.equals("java.lang.Object")){
	   if (host.method_env.containsKey(id.getText())){
	   s.addAll(host.method_env.get(id.getText()));
	   }                    
	   }
	   else if (host instanceof AClassTypeDecl){
	   s.addAll(lookupMethodThroughClass(host,id));
	   }
	   else if (host instanceof AInterfaceTypeDecl){
	   s.addAll(lookupMethodThroughInterface(host,id));
	   }
	   return s;*/
      }

      /**
       * Look up a field by name in a class or interface, taking inheritance into
       * account.
       * 
       * @param host
       *            the receiver type
       * @param id
       *            the name of the field
       * @return the field declared by or inherited by the given receiver type, or
       *         <code>null</code> if no such field exists.
       */
      public static AFieldDecl lookupField(PTypeDecl host, TIdentifier id){
        if(host.allFields!=null){ 
                 if (host.allFields.containsKey(id.getText())){
	                return host.allFields.get(id.getText());
	        }      
        }
       
         HashMap<String,AFieldDecl> names=new HashMap<String,AFieldDecl>();
         host.allFields = getFields(host,names);
	 AFieldDecl decls=host.allFields.get(id.getText());
         return decls;
         /*	 if (decls.size() > 0){
	    Iterator<AFieldDecl> it = decls.iterator();
	    if (decls.size() > 1){
	       errorInheritedFieldClash(host, it.next());
	       return null;
	    }
	    return (it.next());
	 }
*/
      }

      private static Map<String,AFieldDecl> getFields(PTypeDecl host,Map<String,AFieldDecl> m){
	 //Map<String,AFieldDecl> s2 = new HashMap<String,AFieldDecl>();
         SOP("SIZE OF HOST FIELD ENV: "+host.field_env.size());
         SOP("MAP:"+m);
         for (String hostf:host.field_env.keySet()){
             if(m.containsKey(hostf)){

                 PTypeDecl p1=m.get(hostf).getType().getAncestor(PTypeDecl.class);      
                 if (!(m.get(hostf).getType().equals(host.field_env.get(hostf).getType())) && !isSuperType(host.type,p1.type)){
                        SOP("==========================____________________+==================="+host.type+"||||"+p1.type);
                        SOP("ABE "+host.field_env);
                        errorInheritedFieldClash(host, host.field_env.get(hostf));
                 }
             } 
             SOP("PASSED THE FIELD CHECKER");   
             SOP("ABE "+host.field_env);
            //m.put(hostf,host.field_env.get(hostf));
            if(!m.containsKey(hostf))
                m.put(hostf,host.field_env.get(hostf));
         }	
	     LinkedList<ANamedType> ll;
             if(host.canonical_name.equals("java.lang.Object"))
                return m;

	    if(host.kindPTypeDecl()==ETypeDecl.CLASS){
	       ANamedType t=((AClassTypeDecl)host).getSuper();
	       if (t!=null){                                                               
		  PTypeDecl sup=((AClassTypeDecl)host).getSuper().decl;
		  getFields(sup,m);
	       }
	       ll = ((AClassTypeDecl)host).getInterfaces();
	    } else {
	       ll = ((AInterfaceTypeDecl)host).getSupers();
	    }
	    for (int i=0;i<ll.size() ;i++ ){
	       PTypeDecl sup_interface = ll.get(i).decl;
	       getFields(sup_interface,m);
	    }	 
	 return m;
      }

      /**
       * Query the type hierarchy relation.
       * 
       * @param super_type
       *            the possible supertype
       * @param sub_type
       *            the possible subtype
       * @return <code>true</code> if <code>super_type</code> is a supertype
       *         of <code>sub_type</code>, including <code>sub_type</code>
       *         itself.
       */
      public static boolean isSuperType(ANamedType super_type, ANamedType sub_type) {
	 PTypeDecl super_soaker = super_type.decl;
	 PTypeDecl sub_soaker = sub_type.decl;
	 List<ANamedType> supers=new LinkedList<ANamedType>();
	 if(sub_soaker.beingVisited){
	    return true;//Only used for circular inheritance...
	 }
	 sub_soaker.beingVisited=true;
	 boolean ret=false; //Ugly, but javac is dumb.

	 if(super_soaker.canonical_name.equals(sub_soaker.canonical_name)){
	    ret=true;
	 }
	 else if(sub_soaker.canonical_name.equals("java.lang.Object")){
	    ret=false;
	 } else {
	    if(sub_soaker.kindPTypeDecl()==ETypeDecl.CLASS){
	       ANamedType temp=((AClassTypeDecl)sub_soaker).getSuper();
	       if(temp!=null){
		  ret = (ret || isSuperType(super_type,temp)); 
		  supers=((AClassTypeDecl)sub_soaker).getInterfaces();
	       }
	    } else{
	       supers=((AInterfaceTypeDecl)sub_soaker).getSupers();
	    }

	    if(supers.size()==0){
	       PTypeDecl obj=Environments.lookupNamedType("java.lang.Object");
	       ret=(ret || isSuperType(super_type,obj.type));
	    } else{
	       for(ANamedType p : supers){
		  if(isSuperType(super_type,p)){
		     ret=true;
		     break;
		  }
	       }
	    }
	 }
	 sub_soaker.beingVisited=false;
	 return ret;      
      }
      public static void SOP(String s){
//	 System.out.println(s);
      }
      // /////////////////////////////////////////////////////////////////////////
      // ERROR MESSAGES
      // /////////////////////////////////////////////////////////////////////////

      /**
       * Reports the error that a class or interface depends on itself, i.e.
       * circular inheritance.
       * 
       * @param node
       *            the type declaration
       * @param fatal
       *            if {@code true} the compilation is stopped immediately
       */
      @SuppressWarnings("unused")
	 private static void errorCircularInheritance(PTypeDecl node, boolean fatal) {
	    Errors.error(ErrorType.CIRCULAR_INHERITANCE, node.getName(),
		  "Circular inheritance", fatal);
	 }

      /**
       * Report the error that a class that has abstract methods must itself be
       * abstract.
       * 
       * @param node
       *            the class declaration
       * @param method
       *            the method declaration of the abstract method
       */
      @SuppressWarnings("unused")
	 private static void errorClassMustBeAbstract(AClassTypeDecl node, 
	       AMethodDecl method) {
	    Errors.error(ErrorType.CLASS_MUST_BE_ABSTRACT, node.getName(), 
		  "The class " + node.getName().getText() + 
		  " must be declared abstract because of abstract method " + 
		  Util.getMethodSignature(method), false);
	 }

      /**
       * Reports the error that a class must not contain two different methods
       * with the same signature but different return types.
       * 
       * @param currentType
       *            the current type declaration
       * @param replacingMethod
       *            the replacing method declaration
       * @param replaceMethod
       *            the replaced method declaration
       */
      @SuppressWarnings("unused")
	 private static void errorDifferentReturnType(PTypeDecl currentType, 
	       AMethodDecl replacingMethod, AMethodDecl replaceMethod) {
	    boolean replace = !(replacingMethod.isAbstract() && replacingMethod.isAbstract());
	    boolean override = !(replacingMethod.isStatic());
	    Errors.error(
		  ErrorType.DIFFERENT_RETURN_TYPE, 
		  currentType.getName(), 
		  "The method " + Util.getMethodSignature(replacingMethod) + 
		  " must have same return type as " + 
		  (replace ? (override ? "overridden" : "hidden") : "other inherited") + 
		  " method " + Util.getMethodSignature(replaceMethod),
		  false
		  );
	 }

      /**
       * Reports the error that a class must not declare two constructors with the
       * same signature.
       * 
       * @param node
       *            one of the constructor declarations
       */
      @SuppressWarnings("unused")
	 private static void errorDuplicateConstructor(AConstructorDecl node) {
	    Errors.error(ErrorType.DUPLICATE_CONSTRUCTOR,
		  node.getName(), 
		  "Duplicate constructor",
		  false
		  );
	 }

      /**
       * Reports the error that a class must not declare two methods with the same
       * signature.
       * 
       * @param node
       *            one of the method declarations
       */
      @SuppressWarnings("unused")
	 private static void errorDuplicateMethod(AMethodDecl node) {
	    Errors.error(ErrorType.DUPLICATE_METHOD, 
		  node.getName(), "The method " + Util.getMethodSignature(node) + 
		  " is already declared", false);
	 }

      /**
       * Reports the error that a class cannot extend a final class.
       * 
       * @param node
       *            the class declaration
       */
      @SuppressWarnings("unused")
	 private static void errorExtendsFinalClass(AClassTypeDecl node) {
	    Errors.error(ErrorType.EXTENDS_FINAL_CLASS, node.getName(), 
		  "The class " + node.getName().getText() + 
		  " cannot extend a final class", false);
	 }

      /**
       * Reports the error that a class cannot extend a non-class
       * 
       * @param node
       *            the named type of the non-class
       */
      @SuppressWarnings("unused")
	 private static void errorExtendsNonClass(ANamedType node) {
	    Errors.error(ErrorType.EXTENDS_NON_CLASS, node.getName().getToken(), 
		  node.typeName() + " is not a class", false);
	 }

      /**
       * Reports the error that a replacing method cannot declare an checked
       * exception in its throws clause which is not declared by the replaced
       * method.
       * 
       * @param methodDecl
       *            the method declaration
       * @param exception
       *            the named type of the exception
       */
      @SuppressWarnings("unused")
	 private static void errorIllegalThrowsInReplace(PTypeDecl currentType, AMethodDecl methodDecl, 
	       ANamedType exception) {
	    boolean override = !(methodDecl.isStatic());
	    Errors.error(ErrorType.ILLEGAL_THROWS_IN_REPLACE, currentType.getName(), 
		  "The method " + Util.getMethodSignature(methodDecl) + 
		  " declares checked exception " + exception.typeName() + 
		  " which is not declared by " + 
		  (override ? "overridden" : "hidden") + " method", false);
	 }

      /**
       * Reports the error that a class cannot implement a non-interface.
       * 
       * @param node
       *            the named type of the non-interface
       */
      @SuppressWarnings("unused")
	 private static void errorImplementsNonInterface(ANamedType node) {
	    Errors.error(ErrorType.IMPLEMENTS_NON_INTERFACE, node.getName().getToken(), 
		  node.typeName() + " is not an interface", false);
	 }
      /**
       * Reports the error that two different inherited fields clash.
       * 
       * @param currentType
       *            the current type declaration
       * @param fieldDecl
       *            one of the field declarations
       */
      @SuppressWarnings("unused")
	 private static void errorInheritedFieldClash(PTypeDecl currentType, 
	       AFieldDecl fieldDecl) {
	    Errors.error(ErrorType.INHERITED_FIELD_CLASH, currentType.getName(), 
		  "Multiply inherited field name " + fieldDecl.getName().getText(),
		  false);
	 }

      /**
       * Reports the error that a nonstatic method cannot replace a static method.
       * 
       * @param methodDecl
       *            the method declaration of the nonstatic method
       */
      @SuppressWarnings("unused")
	 private static void errorNonstaticReplaceStatic(AMethodDecl methodDecl) {
	    Errors.error(ErrorType.NONSTATIC_REPLACE_STATIC, methodDecl.getName(), 
		  "The nonstatic method " + Util.getMethodSignature(methodDecl) + 
		  " cannot replace a static method", false);
	 }

      /**
       * Reports the error that a method cannot replace a final method.
       * 
       * @param node
       *            the method declaration of the replacing method
       */
      @SuppressWarnings("unused")
	 private static void errorReplaceFinal(AMethodDecl node) {
	    boolean override = !(node.isStatic());
	    Errors.error(ErrorType.REPLACE_FINAL, node.getName(), "The method " + 
		  Util.getMethodSignature(node) + " cannot " + 
		  (override ? "override" : "hide") + " a final method", false);
	 }

      /**
       * Reports the error that a protected method cannot replace a public method.
       * 
       * @param currentType
       *            the current type declaration
       * @param methodDecl
       *            the method declaration of the protected method
       */
      @SuppressWarnings("unused")
	 private static void errorProtectedReplacePublic(PTypeDecl currentType, 
	       AMethodDecl methodDecl) {
	    boolean override = !(methodDecl.isStatic());
	    Errors.error(ErrorType.PROTECTED_REPLACE_PUBLIC, currentType.getName(), 
		  "The method " + Util.getMethodSignature(methodDecl) + 
		  " cannot have stricter access privileges than " + 
		  (override ? "overridden" : "hidden") + " method", false);
	 }

      /**
       * Reports the error that the same interface is found twice in the
       * implements clause of a class or in the extends clause of an interface.
       * 
       * @param type
       *            the named type of the interface
       */
      @SuppressWarnings("unused")
	 private static void errorRepeatedInterface(ANamedType type) {
	    Errors.error(ErrorType.REPEATED_INTERFACE, type.getName().getToken(), 
		  "Repeated interface " + type.typeName(), false);
	 }

      /**
       * Reports the error that a static method cannot replace a nonstatic method.
       * 
       * @param method
       *            the method declaration of the static method
       */
      @SuppressWarnings("unused")
	 private static void errorStaticReplaceNonstatic(AMethodDecl method) {
	    Errors.error(ErrorType.STATIC_REPLACE_NONSTATIC, method.getName(),
		  "The static method " + Util.getMethodSignature(method) + 
		  " cannot hide a nonstatic method",false);
	 }

      }
