package dovs;

import dovs.phases.*;
import dovs.node.*;
import dovs.lexer.*;
import dovs.parser.*;

import dovs.peephole.*;
import dovs.peephole.node.PPatternDecl;
import dovs.peephole.node.APatternCollection;

import java.io.*;
import java.util.*;

/**
 * Commandline interface for the compiler.
 * <p>
 */
public class Main {
	public static String classpath;

	public static AProgram program;

	public static boolean joos1;

	public static boolean noverify;

	public static APatternCollection peephole_patterns;

	public static String peephole_filename;
	
	/**
	 * Compile a program.
	 * <p>
	 * 
	 * @param args
	 *            the commandline arguments given to the compiler
	 * @return <code>0</code> if the compilation succeeded,<br>
	 *         <code>5</code> if the program contains an error, or<br>
	 *         <code>10</code> if the compiler encounters an internal compiler
	 *         error.
	 */
	public static void main(String[] args) throws IOException {
		int exitcode = 5;
		List<ErrorType> errors = compile(args);
		if (errors.isEmpty()) {
			exitcode = 0;
		} else if (errors.contains(ErrorType.INTERNAL_COMPILER_ERROR)) {
			exitcode = 10;
		}
		if (exitcode == 0 && peephole_patterns != null) {
			System.out.println("Peephole pattern hitlist:");
			PeepholeDriver.printStatistics(peephole_patterns, System.out);
		}
		System.exit(exitcode);
	}

	/**
	 * Compile a program.
	 * <p>
	 * The compiler is designed to be re-executable, that is, this method can be
	 * called multiple times with different programs and is responsible for
	 * resetting all persistent state.
	 * 
	 * @param args
	 *            the commandline arguments given to the compiler
	 * @return a list containing the types of the errors in the order they
	 *         occured during compilation
	 */
	public static List<ErrorType> compile(String[] args) throws IOException {
		List<ErrorType> errors = new LinkedList<ErrorType>();
		classpath = null;
		program = null;
		joos1 = false;
		noverify = false;
		Errors.reset();
		try {
			program = parseProgram(args);
			Errors.check();
			System.out.print("Weeding...");
			program.apply(Weeding.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("Environment...");
			program.apply(Environments.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("TypeLinking...");
			program.apply(TypeLinking.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("Hierarchy...");
			program.apply(Hierarchy.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("Disambiguation...");
			program.apply(Disambiguation.aspectOf());
			Errors.check();			
			System.out.println("\t\tOK");
			System.out.print("TypeChecking...");
                        program.apply(TypeChecking.aspectOf());
			Errors.check();	
			System.out.println("\t\tOK");
			System.out.print("ConstantFolding...");
			program.apply(ConstantFolding.aspectOf());
			Errors.check();			
			System.out.println("\t\tOK");
			System.out.print("Reachability...");
			program.apply(Reachability.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("DefiniteAssignment...");
			program.apply(DefiniteAssignment.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("Resources...");
			program.apply(Resources.aspectOf());
			Errors.check();			
			System.out.println("\t\tOK");
			System.out.print("CodeGeneration...");
			program.apply(CodeGeneration.aspectOf());
			Errors.check();			
			System.out.println("\t\tOK");
			System.out.print("Optimization...");
			program.apply(Optimization.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("Limits...");
			program.apply(Limits.aspectOf());
			Errors.check();
			System.out.println("\t\tOK");
			System.out.print("CodeEmission...");
			program.apply(CodeEmission.aspectOf());
			System.out.println("\t\tOK");
		} catch (SourceError e) {
			System.err.println("Errors during compilation.");
			return e.getErrors();
		} catch (PeepholeException e) {
			PPatternDecl pattern = PeepholeDriver.current_pattern;
			if (pattern != null) {
				System.err.println("Error in peephole pattern "
						+ pattern.getName().getText() + ":");
			} else {
				System.err.println("Error in peephole patterns:");
			}
			System.err.println(e.getMessage());
			errors.add(ErrorType.PEEPHOLE);
		} catch (InternalCompilerError e) {
			System.err.println("Internal compiler error: " + e.getMessage());
			errors.add(ErrorType.INTERNAL_COMPILER_ERROR);
		}
		return errors;
	}

	private static AProgram parseProgram(String[] args) throws IOException {
		List<ASourceFile> sources = new ArrayList<ASourceFile>();
		boolean parse_classpath = false;
		boolean parse_patterns = false;
		for (String arg : args) {
			if (parse_classpath) {
				classpath = arg;
				parse_classpath = false;
			} else if (parse_patterns) {
				// Only load patterns if not already loaded
				if (peephole_filename == null || !peephole_filename.equals(arg)) {
					try {
						peephole_patterns = Loader.load(arg);
						peephole_filename = arg;
					} catch (FileNotFoundException e) {
						Errors.errorMessage(ErrorType.PEEPHOLE,
								"Peephole pattern file " + arg + " not found");
					}
				}
				parse_patterns = false;
			} else if (arg.equals("-classpath") || arg.equals("-cp")) {
				parse_classpath = true;
			} else if (arg.equals("-joos1")) {
				joos1 = true;
			} else if (arg.equals("-noverify")) {
				noverify = true;
			} else if (arg.equals("-O")) {
				parse_patterns = true;
			} else {
				String filename = arg;
				try {
					Parser parser = new Parser(new Lexer(
							new PushbackReader(
									new InputStreamReader(new FileInputStream(
											filename), "ISO-8859-1"), 4096)));
					Start startsym = parser.parse();
					ASourceFile sf = startsym.getSourceFile();
					sf.setFileName(filename);
					
					sources.add(sf);
				} catch (FileNotFoundException e) {
					Errors.errorMessage(ErrorType.FILE_OPEN_ERROR, "File "
							+ filename + " not found");
				} catch (LexerException e) {
					Errors.errorMessage(ErrorType.LEXER_EXCEPTION,
							"Syntax error at " + filename + " "
									+ e.getMessage());
				} catch (ParserException e) {
					Errors.errorMessage(ErrorType.PARSER_EXCEPTION,
							"Syntax error at " + filename + " "
									+ e.getMessage());
				}
			}
		}
		if (parse_classpath) {
			Errors.errorMessage(ErrorType.ARGUMENT_ERROR,
					"Classpath argument expected");
		}
		if (parse_patterns) {
			Errors.errorMessage(ErrorType.ARGUMENT_ERROR,
					"Peephole pattern file argument expected");
		}
		Errors.check();
		if (sources.size() == 0) {
			Errors.errorMessage(ErrorType.ARGUMENT_ERROR,
					"No source files given");
		}	
		
		if (classpath == null) {
			classpath = System.getenv("CLASSPATH");
		}
		ClassEnvironment.init(classpath);

		return new AProgram(sources);
	}
}

