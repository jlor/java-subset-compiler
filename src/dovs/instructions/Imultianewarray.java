
package dovs.instructions;

/** A <code>multianewarray</code> instruction. */
public class Imultianewarray extends Instruction {
    private TypeSignature sig;
    private int dims;

    public Imultianewarray(TypeSignature sig, int dims) {
	this.sig = sig;
	this.dims = dims;
    }

    public @Override String toAsm() {
	return "multianewarray "+sig+" "+dims;
    }

    public @Override int stackChange() {
	return -dims+1;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return sig;
	case 1:
	    return dims;
	default:
	    return null;
	}
    }
}
