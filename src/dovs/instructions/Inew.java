
package dovs.instructions;

/** A <code>new</code> instruction. */
public class Inew extends Instruction {
    private TypeSignature sig;

    public Inew(TypeSignature sig) {
	this.sig = sig;
    }

    public @Override String toAsm() {
	return "new "+sig;
    }

    public @Override int stackChange() {
	return 1;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return sig;
	default:
	    return null;
	}
    }
}
