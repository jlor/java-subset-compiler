
package dovs.instructions;

/** An <code>saload</code> instruction. */
public class Isaload extends Instruction {
    public Isaload() {}

    public @Override String toAsm() {
	return "saload";
    }

    public @Override int stackChange() {
	return -1;
    }
}
