
package dovs.instructions;

/** An <code>sastore</code> instruction. */
public class Isastore extends Instruction {
    public Isastore() {}

    public @Override String toAsm() {
	return "sastore";
    }

    public @Override int stackChange() {
	return -3;
    }
}
