
package dovs.instructions;

/** A <code>dup2</code> instruction. */
public class Idup2 extends Instruction {
    public Idup2() {}

    public @Override String toAsm() {
	return "dup2";
    }

    public @Override int stackChange() {
	return 2;
    }
}
