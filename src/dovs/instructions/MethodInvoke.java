
package dovs.instructions;

/** Common superclass for all invoke instructions.
 */
public abstract class MethodInvoke extends Instruction {
    protected MethodSignature sig;

    public MethodInvoke(MethodSignature sig) {
	this.sig = sig;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return sig;
	default:
	    return null;
	}
    }
}
