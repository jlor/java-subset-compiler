
package dovs.instructions;

/** An <code>iastore</code> instruction. */
public class Iiastore extends Instruction {
    public Iiastore() {}

    public @Override String toAsm() {
	return "iastore";
    }

    public @Override int stackChange() {
	return -3;
    }
}
