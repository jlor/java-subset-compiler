
package dovs.instructions;

/** A <code>getstatic</code> instruction. */
public class Igetstatic extends FieldAccess {
    public Igetstatic(FieldSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "getstatic "+sig;
    }

    public @Override int stackChange() {
	return 1;
    }
}
