
package dovs.instructions;

/** An <code>istore</code> instruction.
 *  Depending on the index parameter of the instruction,
 *  an <code>istore</code> or <code>istore_&lt;n&gt;</code>
 *  instruction is emitted.
 */
public class Iistore extends LocalAccess {
    public Iistore(int index) {
	super(index);
    }

    public @Override String toAsm() {
	if (index <= 3) {
	    return "istore_"+index;
	} else {
	    return "istore "+index;
	}
    }

    public @Override int stackChange() {
	return -1;
    }
}
