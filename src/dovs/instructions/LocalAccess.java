
package dovs.instructions;

/** Common superclass for all load and store instructions.
 */
public abstract class LocalAccess extends Instruction {
    protected int index;

    public LocalAccess(int index) {
	this.index = index;
    }

    public @Override int localAccess() {
	return index;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return index;
	default:
	    return null;
	}
    }
}
