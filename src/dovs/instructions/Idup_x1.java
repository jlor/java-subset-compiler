
package dovs.instructions;

/** A <code>dup_x1</code> instruction. */
public class Idup_x1 extends Instruction {
    public Idup_x1() {}

    public @Override String toAsm() {
	return "dup_x1";
    }

    public @Override int stackChange() {
	return 1;
    }
}
