
package dovs.instructions;

/** An <code>invokespecial</code> instruction. */
public class Iinvokespecial extends MethodInvoke {
    public Iinvokespecial(MethodSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "invokespecial "+sig;
    }

    public @Override int stackChange() {
	return -1-sig.numberOfArguments()+sig.numberOfReturns();
    }
}
