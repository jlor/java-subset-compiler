
package dovs.instructions;

/** An <code>ior</code> instruction. */
public class Iior extends Instruction {
    public Iior() {}

    public @Override String toAsm() {
	return "ior";
    }

    public @Override int stackChange() {
	return -1;
    }
}
