
package dovs.instructions;

/** An <code>imul</code> instruction. */
public class Iimul extends Instruction {
    public Iimul() {}

    public @Override String toAsm() {
	return "imul";
    }

    public @Override int stackChange() {
	return -1;
    }
}
