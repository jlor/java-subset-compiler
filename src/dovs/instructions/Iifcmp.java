
package dovs.instructions;

/** An <code>if</code> instruction comparing two values.
 *  Depending on the condition
 *  parameter of the instruction, this instruction
 *  corresponds to the actual JVM instruction
 *  <code>if_cmplt</code>, <code>if_cmple</code>,
 *  <code>if_cmpgt</code>, <code>if_cmpge</code>,
 *  <code>if_acmpeq</code> or <code>if_acmpne</code>.
 */
public class Iifcmp extends ConditionalJump {
    public Iifcmp(Condition cond, Label target) {
	super(cond, target);
    }

    public @Override String toAsm() {
	return Condition.ifcmpName(cond)+" "+target;
    }

    public @Override int stackChange() {
	return -2;
    }
}
