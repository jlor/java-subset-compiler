
package dovs.instructions;

/** An <code>aload</code> instruction.
 *  Depending on the index parameter of the instruction,
 *  an <code>aload</code> or <code>aload_&lt;n&gt;</code>
 *  instruction is emitted.
 */
public class Iaload extends LocalAccess {
    public Iaload(int index) {
	super(index);
    }

    public @Override String toAsm() {
	if (index <= 3) {
	    return "aload_"+index;
	} else {
	    return "aload "+index;
	}
    }

    public @Override int stackChange() {
	return 1;
    }
}
