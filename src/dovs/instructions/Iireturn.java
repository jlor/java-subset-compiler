
package dovs.instructions;

/** An <code>ireturn</code> instruction. */
public class Iireturn extends Instruction {
    public Iireturn() {}

    public @Override String toAsm() {
	return "ireturn";
    }

    public @Override int stackChange() {
	return -1;
    }

    public @Override boolean canFallThrough() {
	return false;
    }
}
