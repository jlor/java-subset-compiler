
package dovs.instructions;

/** A <code>pop</code> instruction. */
public class Ipop extends Instruction {
    public Ipop() {}

    public @Override String toAsm() {
	return "pop";
    }

    public @Override int stackChange() {
	return -1;
    }
}
