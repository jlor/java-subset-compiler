
package dovs.instructions;

/** Common superclass for all get and put instructions.
 */
public abstract class FieldAccess extends Instruction {
    protected FieldSignature sig;

    public FieldAccess(FieldSignature sig) {
	this.sig = sig;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return sig;
	default:
	    return null;
	}
    }
}
