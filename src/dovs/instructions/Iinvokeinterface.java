
package dovs.instructions;

/** An <code>invokeinterface</code> instruction. */
public class Iinvokeinterface extends MethodInvoke {
    public Iinvokeinterface(MethodSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "invokeinterface "+sig+" "+(1+sig.numberOfArguments());
    }

    public @Override int stackChange() {
	return -1-sig.numberOfArguments()+sig.numberOfReturns();
    }
}
