
package dovs.instructions;

/** An <code>aconst_null</code> instruction. */
public class Iaconst_null extends Instruction {
    public Iaconst_null() {}

    public @Override String toAsm() {
	return "aconst_null";
    }

    public @Override int stackChange() {
	return 1;
    }
}
