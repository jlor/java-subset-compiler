
package dovs.instructions;

/** A <code>return</code> instruction. */
public class Ireturn extends Instruction {
    public Ireturn() {}

    public @Override String toAsm() {
	return "return";
    }

    public @Override int stackChange() {
	return 0;
    }

    public @Override boolean canFallThrough() {
	return false;
    }
}
