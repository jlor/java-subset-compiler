
package dovs.instructions;

/** A <code>checkcast</code> instruction. */
public class Icheckcast extends Instruction {
    private TypeSignature sig;

    public Icheckcast(TypeSignature sig) {
	this.sig = sig;
    }

    public @Override String toAsm() {
	return "checkcast "+sig;
    }

    public @Override int stackChange() {
	return 0;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return sig;
	default:
	    return null;
	}
    }
}
