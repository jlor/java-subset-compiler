
package dovs.instructions;

/** An integer constant load instruction.
 *  Depending on the value of the integer constant, this
 *  instruction is emitted as an
 *  <code>iconst_m1</code>, <code>iconst_&lt;n&gt;</code>,
 *  <code>bipush</code>, <code>sipush</code> or
 *  <code>ldc</code> instruction.
 */
public class Ildc_int extends Instruction {
    private int value;

    public Ildc_int(int value) {
	this.value = value;
    }

    public @Override String toAsm() {
	if (value == -1) {
	    return "iconst_m1";
	} else if (value >= 0 && value <= 5) {
	    return "iconst_"+value;
	} else if (value >= -128 && value <= 127) {
	    return "bipush "+value;
	} else if (value >= -32768 && value <= 32767) {
	    return "sipush "+value;
	} else {
	    return "ldc "+value;
	}
    }

    public @Override int stackChange() {
	return 1;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return value;
	default:
	    return null;
	}
    }
}
