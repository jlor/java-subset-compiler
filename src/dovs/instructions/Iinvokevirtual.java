
package dovs.instructions;

/** An <code>invokevirtual</code> instruction. */
public class Iinvokevirtual extends MethodInvoke {
    public Iinvokevirtual(MethodSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "invokevirtual "+sig;
    }

    public @Override int stackChange() {
	return -1-sig.numberOfArguments()+sig.numberOfReturns();
    }
}
