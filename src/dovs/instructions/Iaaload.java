
package dovs.instructions;

/** An <code>aaload</code> instruction. */
public class Iaaload extends Instruction {
    public Iaaload() {}

    public @Override String toAsm() {
	return "aaload";
    }

    public @Override int stackChange() {
	return -1;
    }
}
