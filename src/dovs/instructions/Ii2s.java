
package dovs.instructions;

/** An <code>i2s</code> instruction. */
public class Ii2s extends Instruction {
    public Ii2s() {}

    public @Override String toAsm() {
	return "i2s";
    }

    public @Override int stackChange() {
	return 0;
    }
}
