
package dovs.instructions;

/** A field signature. Used to encapsulate field signature
 *  parameters to instructions.
 */
public class FieldSignature {
    private String sig;
    private String type;

    /** Create a field signature.
     *  @param sig the signature of the field as returned by
     *  <code>AFieldDecl.getSignature()</code>.
     *  @param type the type signature of the field type as returned by
     *  <code>PType.getSignature()</code>.
     */
    public FieldSignature(String sig, String type) {
	this.sig = sig;
	this.type = type;
    }

    public String getSignature() {
	return sig;
    }

    public String getType() {
	return type;
    }

    public String toString() {
	return getSignature()+" "+getType();
    }

    public boolean equals(Object o) {
	return sig.equals(((FieldSignature)o).sig) &&
	    type.equals(((FieldSignature)o).type);
    }

    public int hashCode() {
	return sig.hashCode()+type.hashCode();
    }
}
