
package dovs.instructions;

/** A <code>getfield</code> instruction. */
public class Igetfield extends FieldAccess {
    public Igetfield(FieldSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "getfield "+sig;
    }

    public @Override int stackChange() {
	return 0;
    }
}
