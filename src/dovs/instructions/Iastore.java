
package dovs.instructions;

/** An <code>astore</code> instruction.
 *  Depending on the index parameter of the instruction,
 *  an <code>astore</code> or <code>astore_&lt;n&gt;</code>
 *  instruction is emitted.
 */
public class Iastore extends LocalAccess {
    public Iastore(int index) {
	super(index);
    }

    public @Override String toAsm() {
	if (index <= 3) {
	    return "astore_"+index;
	} else {
	    return "astore "+index;
	}
    }

    public @Override int stackChange() {
	return -1;
    }
}
