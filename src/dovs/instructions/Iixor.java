
package dovs.instructions;

/** An <code>ixor</code> instruction. */
public class Iixor extends Instruction {
    public Iixor() {}

    public @Override String toAsm() {
	return "ixor";
    }

    public @Override int stackChange() {
	return -1;
    }
}
