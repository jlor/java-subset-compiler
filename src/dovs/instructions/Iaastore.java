
package dovs.instructions;

/** An <code>aastore</code> instruction. */
public class Iaastore extends Instruction {
    public Iaastore() {}

    public @Override String toAsm() {
	return "aastore";
    }

    public @Override int stackChange() {
	return -3;
    }
}
