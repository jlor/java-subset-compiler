
package dovs.instructions;

/** A <code>swap</code> instruction. */
public class Iswap extends Instruction {
    public Iswap() {}

    public @Override String toAsm() {
	return "swap";
    }

    public @Override int stackChange() {
	return 0;
    }
}
