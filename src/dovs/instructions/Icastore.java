
package dovs.instructions;

/** An <code>castore</code> instruction. */
public class Icastore extends Instruction {
    public Icastore() {}

    public @Override String toAsm() {
	return "castore";
    }

    public @Override int stackChange() {
	return -3;
    }
}
