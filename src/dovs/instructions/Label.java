
package dovs.instructions;

/** A label. Note that the actual occurrence of
 *  the label is represented by the {@link Ilabel}
 *  instruction.
 */
public class Label {
    private String name;
    private static int next_label_id;

    public Label(String name) {
	this.name = name;
    }

    public String getName() {
	return name;
    }

    public String toString() {
	return getName();
    }

    /** Make a unique label whose name starts
     *  with the given string. The label name will be
     *  the given prefix followed by a number.
     *  @param prefix the desired label name prefix
     *  @return a fresh, unique label
     */
    public static Label make(String prefix) {
	return new Label(prefix+(next_label_id++));
    }

    public boolean equals(Object o) {
	return name.equals(((Label)o).name);
    }

    public int hashCode() {
	return name.hashCode();
    }
}
