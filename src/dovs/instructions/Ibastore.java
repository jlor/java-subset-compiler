
package dovs.instructions;

/** An <code>bastore</code> instruction. */
public class Ibastore extends Instruction {
    public Ibastore() {}

    public @Override String toAsm() {
	return "bastore";
    }

    public @Override int stackChange() {
	return -3;
    }
}
