
package dovs.instructions;

/** An <code>irem</code> instruction. */
public class Iirem extends Instruction {
    public Iirem() {}

    public @Override String toAsm() {
	return "irem";
    }

    public @Override int stackChange() {
	return -1;
    }
}
