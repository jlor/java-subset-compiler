
package dovs.instructions;

/** An <code>iaload</code> instruction. */
public class Iiaload extends Instruction {
    public Iiaload() {}

    public @Override String toAsm() {
	return "iaload";
    }

    public @Override int stackChange() {
	return -1;
    }
}
