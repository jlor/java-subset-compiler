
package dovs.instructions;

/** A <code>putfield</code> instruction. */
public class Iputfield extends FieldAccess {
    public Iputfield(FieldSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "putfield "+sig;
    }

    public @Override int stackChange() {
	return -2;
    }
}
