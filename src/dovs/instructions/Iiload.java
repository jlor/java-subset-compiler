
package dovs.instructions;

/** An <code>iload</code> instruction.
 *  Depending on the index parameter of the instruction,
 *  an <code>iload</code> or <code>iload_&lt;n&gt;</code>
 *  instruction is emitted.
 */
public class Iiload extends LocalAccess {
    public Iiload(int index) {
	super(index);
    }

    public @Override String toAsm() {
	if (index <= 3) {
	    return "iload_"+index;
	} else {
	    return "iload "+index;
	}
    }

    public @Override int stackChange() {
	return 1;
    }
}
