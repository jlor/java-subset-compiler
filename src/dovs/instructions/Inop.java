
package dovs.instructions;

/** A <code>nop</code> instruction. */
public class Inop extends Instruction {
    public Inop() {}

    public @Override String toAsm() {
	return "nop";
    }

    public @Override int stackChange() {
	return 0;
    }
}
