
package dovs.instructions;

/** A type signature. Used to encapsulate type signature
 *  parameters to instructions.
 */
public class TypeSignature {
    private String sig;

    /** Create a type signature.
     *  @param sig the signature itself.
     *  This should be either the signature of a class or interface
     *  as returned by <code>PTypeDecl.getSignature()</code>
     *  (e.g. <code>&quot;java/lang/Object&quot;</code>) or the
     *  type signature of an array type as returned by
     *  <code>AArrayType.getSignature()</code>
     *  (e.g. <code>&quot;[I&quot;</code> or
     *  <code>&quot;[Ljava/lang/Object;&quot;</code>).
     */
    public TypeSignature(String sig) {
	this.sig = sig;
    }

    public String getSignature() {
	return sig;
    }

    public String toString() {
	return getSignature();
    }

    public boolean equals(Object o) {
	return sig.equals(((TypeSignature)o).sig);
    }

    public int hashCode() {
	return sig.hashCode();
    }
}
