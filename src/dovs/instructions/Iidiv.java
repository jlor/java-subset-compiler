
package dovs.instructions;

/** An <code>idiv</code> instruction. */
public class Iidiv extends Instruction {
    public Iidiv() {}

    public @Override String toAsm() {
	return "idiv";
    }

    public @Override int stackChange() {
	return -1;
    }
}
