
package dovs.instructions;

/** A <code>dup_x2</code> instruction. */
public class Idup_x2 extends Instruction {
    public Idup_x2() {}

    public @Override String toAsm() {
	return "dup_x2";
    }

    public @Override int stackChange() {
	return 1;
    }
}
