
package dovs.instructions;

/** An <code>isub</code> instruction. */
public class Iisub extends Instruction {
    public Iisub() {}

    public @Override String toAsm() {
	return "isub";
    }

    public @Override int stackChange() {
	return -1;
    }
}
