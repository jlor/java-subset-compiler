
package dovs.instructions;

/** An <code>baload</code> instruction. */
public class Ibaload extends Instruction {
    public Ibaload() {}

    public @Override String toAsm() {
	return "baload";
    }

    public @Override int stackChange() {
	return -1;
    }
}
