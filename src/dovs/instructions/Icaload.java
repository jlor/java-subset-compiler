
package dovs.instructions;

/** An <code>caload</code> instruction. */
public class Icaload extends Instruction {
    public Icaload() {}

    public @Override String toAsm() {
	return "caload";
    }

    public @Override int stackChange() {
	return -1;
    }
}
