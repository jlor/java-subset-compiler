
package dovs.instructions;

/** An <code>athrow</code> instruction. */
public class Iathrow extends Instruction {
    public Iathrow() {}

    public @Override String toAsm() {
	return "athrow";
    }

    public @Override int stackChange() {
	return -1;
    }

    public @Override boolean canFallThrough() {
	return false;
    }
}
