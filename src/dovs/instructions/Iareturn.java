
package dovs.instructions;

/** An <code>areturn</code> instruction. */
public class Iareturn extends Instruction {
    public Iareturn() {}

    public @Override String toAsm() {
	return "areturn";
    }

    public @Override int stackChange() {
	return -1;
    }

    public @Override boolean canFallThrough() {
	return false;
    }
}
