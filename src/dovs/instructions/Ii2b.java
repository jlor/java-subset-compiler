
package dovs.instructions;

/** An <code>i2b</code> instruction. */
public class Ii2b extends Instruction {
    public Ii2b() {}

    public @Override String toAsm() {
	return "i2b";
    }

    public @Override int stackChange() {
	return 0;
    }
}
