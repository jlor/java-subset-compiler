
package dovs.instructions;

/** A <code>arraylength</code> instruction. */
public class Iarraylength extends Instruction {
    public Iarraylength() {}

    public @Override String toAsm() {
	return "arraylength";
    }

    public @Override int stackChange() {
	return 0;
    }
}
