
package dovs.instructions;

/** A <code>putstatic</code> instruction. */
public class Iputstatic extends FieldAccess {
    public Iputstatic(FieldSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "putstatic "+sig;
    }

    public @Override int stackChange() {
	return -1;
    }
}
