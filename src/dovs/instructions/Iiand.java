
package dovs.instructions;

/** An <code>iand</code> instruction. */
public class Iiand extends Instruction {
    public Iiand() {}

    public @Override String toAsm() {
	return "iand";
    }

    public @Override int stackChange() {
	return -1;
    }
}
