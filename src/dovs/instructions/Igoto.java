
package dovs.instructions;

/** A <code>goto</code> instruction. */
public class Igoto extends Jump {
    public Igoto(Label target) {
	super(target);
    }

    public @Override String toAsm() {
	return "goto "+target;
    }

    public @Override int stackChange() {
	return 0;
    }

    public @Override boolean canFallThrough() {
	return false;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return target;
	default:
	    return null;
	}
    }
}
