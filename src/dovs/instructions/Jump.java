
package dovs.instructions;

/** Common superclass for all conditional and unconditional
 *  jump instructions.
 */
public abstract class Jump extends Instruction {
    protected Label target;

    public Jump(Label target) {
	this.target = target;
    }

    public @Override boolean canJump() {
	return true;
    }

    public Label getTarget() {
	return target;
    }

}
