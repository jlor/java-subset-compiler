
package dovs.instructions;

/** An <code>invokestatic</code> instruction. */
public class Iinvokestatic extends MethodInvoke {
    public Iinvokestatic(MethodSignature sig) {
	super(sig);
    }

    public @Override String toAsm() {
	return "invokestatic "+sig;
    }

    public @Override int stackChange() {
	return -sig.numberOfArguments()+sig.numberOfReturns();
    }
}
