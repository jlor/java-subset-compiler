
package dovs.instructions;

/** An <code>i2c</code> instruction. */
public class Ii2c extends Instruction {
    public Ii2c() {}

    public @Override String toAsm() {
	return "i2c";
    }

    public @Override int stackChange() {
	return 0;
    }
}
