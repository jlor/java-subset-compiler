
package dovs.instructions;

/** An <code>ineg</code> instruction. */
public class Iineg extends Instruction {
    public Iineg() {}

    public @Override String toAsm() {
	return "ineg";
    }

    public @Override int stackChange() {
	return 0;
    }
}
