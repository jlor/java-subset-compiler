
package dovs.instructions;

/** Common superclass for all conditional jump instructions. */
public abstract class ConditionalJump extends Jump {
    protected Condition cond;

    public ConditionalJump(Condition cond, Label target) {
	super(target);
	this.cond = cond;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return cond;
	case 1:
	    return target;
	default:
	    return null;
	}
    }
}
