
package dovs.instructions;

/** An <code>iadd</code> instruction. */
public class Iiadd extends Instruction {
    public Iiadd() {}

    public @Override String toAsm() {
	return "iadd";
    }

    public @Override int stackChange() {
	return -1;
    }
}
