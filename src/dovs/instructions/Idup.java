
package dovs.instructions;

/** A <code>dup</code> instruction. */
public class Idup extends Instruction {
    public Idup() {}

    public @Override String toAsm() {
	return "dup";
    }

    public @Override int stackChange() {
	return 1;
    }
}
