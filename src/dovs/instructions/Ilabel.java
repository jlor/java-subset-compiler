
package dovs.instructions;

/** An occurrence of a label. */
public class Ilabel extends Instruction {
    private Label label;

    public Ilabel(Label label) {
	this.label = label;
    }

    public Label getLabel() {
	return label;
    }

    public @Override String toAsm() {
	return label+":";
    }

    public @Override int stackChange() {
	return 0;
    }

    public @Override Object getArg(int i) {
	switch (i) {
	case 0:
	    return label;
	default:
	    return null;
	}
    }
}
