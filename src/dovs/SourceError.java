package dovs;

import java.util.*;

/** Thrown to indicate that an error has occurred
 *  during the compilation that inhibits the compiler
 *  from continuing the compilation.
 */
@SuppressWarnings("serial")
public class SourceError extends RuntimeException {
	private LinkedList<ErrorType> errors = new LinkedList<ErrorType>();

	public SourceError() {
		this.errors.add(ErrorType.UNKNOWN);
	}

	public SourceError(List<ErrorType> errors) {
		this.errors.addAll(errors);
	}

	public List<ErrorType> getErrors() {
		return errors;
	}
}
