
joos2:		parser peephole
		rm -rf classes
		mkdir classes
		ajc -1.5 -d classes src/dovs/*.java src/dovs/*/*.java generated/dovs/*/*.java generated/dovs/peephole/*/*.java
		cp generated/dovs/parser/parser.dat classes/dovs/parser/
		cp generated/dovs/lexer/lexer.dat classes/dovs/lexer/
		cp generated/dovs/peephole/parser/parser.dat classes/dovs/peephole/parser/
		cp generated/dovs/peephole/lexer/lexer.dat classes/dovs/peephole/lexer/
#		cp ASTPrinter.java classes/ASTPrinter.java
#		cd classes && ajc ASTPrinter.java

parser:		joos.sablecc
		rm -rf generated/dovs/{analysis,lexer,node,parser}
		mkdir -p generated
		sablecc -D generated --number-alts joos.sablecc
		chmod a-w generated/dovs/*/*.java
		touch parser

peephole:	peephole.sablecc
		rm -rf generated/dovs/peephole/{analysis,lexer,node,parser}
		mkdir -p generated
		sablecc -D generated --number-alts peephole.sablecc
		chmod a-w generated/dovs/peephole/*/*.java
		touch peephole


clean:
		rm -rf classes/* generated/* parser peephole
