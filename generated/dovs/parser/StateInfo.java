/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.parser;

import java.util.ArrayList;
import java.util.LinkedList;

final class StateInfo
{
	ArrayList nodes;
	LinkedList tokens;

	StateInfo(@SuppressWarnings("hiding") ArrayList nodes, @SuppressWarnings("hiding") LinkedList tokens)
	{
		this.nodes = nodes;
		this.tokens = tokens;
	}
	
	public String toString() {
		return "{" + nodes + "," + tokens + "}";
	}
}
