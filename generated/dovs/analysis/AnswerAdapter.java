
package dovs.analysis;

import dovs.node.*;

/**
 * {@code AnswerAdapter} implements the default behaviour of the {@link Answer}
 * interface. All calls return {@code null} by default.
 *
 * @param <A> the type of the answers
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class AnswerAdapter<A> implements Answer<A> {

	/**
	 * Returns the default answer for a {@link Node}.
	 * @param node the calling {@link Node}
	 * @return the default answer for a {@link Node}
	 */
	public A defaultNode(Node node) {
		return null;
	}
	
	/**
	 * Returns the default answer for a {@link Token}. The call is deferred to
	 * {@link AnswerAdapter#defaultNode(Node)}.
	 * @param token the calling {@link Token}
	 * @return the default answer for a {@link Token}
	 */
	public A defaultToken(Token token) {
		return defaultNode(token);
	}
	
	/**
	 * Returns the default answer for a {@link Start} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link Start} node
	 * @return the default answer for a {@link Start} node
	 */
    public A caseStart(Start node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link ASourceFile} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPSourceFile(PSourceFile)}.
	 * @param node the calling {@link ASourceFile} node
	 * @return the default answer for a {@link ASourceFile} node
	 */
	public A caseASourceFile(ASourceFile node) {
		return defaultPSourceFile(node);
	}
	
	/**
	 * Returns the default answer for a {@link AProgram} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPProgram(PProgram)}.
	 * @param node the calling {@link AProgram} node
	 * @return the default answer for a {@link AProgram} node
	 */
	public A caseAProgram(AProgram node) {
		return defaultPProgram(node);
	}
	
	/**
	 * Returns the default answer for a {@link ASimpleName} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPName(PName)}.
	 * @param node the calling {@link ASimpleName} node
	 * @return the default answer for a {@link ASimpleName} node
	 */
	public A caseASimpleName(ASimpleName node) {
		return defaultPName(node);
	}
	
	/**
	 * Returns the default answer for a {@link AQualifiedName} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPName(PName)}.
	 * @param node the calling {@link AQualifiedName} node
	 * @return the default answer for a {@link AQualifiedName} node
	 */
	public A caseAQualifiedName(AQualifiedName node) {
		return defaultPName(node);
	}
	
	/**
	 * Returns the default answer for a {@link APublicAccess} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPAccess(PAccess)}.
	 * @param node the calling {@link APublicAccess} node
	 * @return the default answer for a {@link APublicAccess} node
	 */
	public A caseAPublicAccess(APublicAccess node) {
		return defaultPAccess(node);
	}
	
	/**
	 * Returns the default answer for a {@link AProtectedAccess} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPAccess(PAccess)}.
	 * @param node the calling {@link AProtectedAccess} node
	 * @return the default answer for a {@link AProtectedAccess} node
	 */
	public A caseAProtectedAccess(AProtectedAccess node) {
		return defaultPAccess(node);
	}
	
	/**
	 * Returns the default answer for a {@link APackageDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPPackageDecl(PPackageDecl)}.
	 * @param node the calling {@link APackageDecl} node
	 * @return the default answer for a {@link APackageDecl} node
	 */
	public A caseAPackageDecl(APackageDecl node) {
		return defaultPPackageDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AOnDemandImportDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPImportDecl(PImportDecl)}.
	 * @param node the calling {@link AOnDemandImportDecl} node
	 * @return the default answer for a {@link AOnDemandImportDecl} node
	 */
	public A caseAOnDemandImportDecl(AOnDemandImportDecl node) {
		return defaultPImportDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link ASingleImportDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPImportDecl(PImportDecl)}.
	 * @param node the calling {@link ASingleImportDecl} node
	 * @return the default answer for a {@link ASingleImportDecl} node
	 */
	public A caseASingleImportDecl(ASingleImportDecl node) {
		return defaultPImportDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AClassTypeDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPTypeDecl(PTypeDecl)}.
	 * @param node the calling {@link AClassTypeDecl} node
	 * @return the default answer for a {@link AClassTypeDecl} node
	 */
	public A caseAClassTypeDecl(AClassTypeDecl node) {
		return defaultPTypeDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AInterfaceTypeDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPTypeDecl(PTypeDecl)}.
	 * @param node the calling {@link AInterfaceTypeDecl} node
	 * @return the default answer for a {@link AInterfaceTypeDecl} node
	 */
	public A caseAInterfaceTypeDecl(AInterfaceTypeDecl node) {
		return defaultPTypeDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AFieldDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPDecl(PDecl)}.
	 * @param node the calling {@link AFieldDecl} node
	 * @return the default answer for a {@link AFieldDecl} node
	 */
	public A caseAFieldDecl(AFieldDecl node) {
		return defaultPDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AInterfaceMethodDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPDecl(PDecl)}.
	 * @param node the calling {@link AInterfaceMethodDecl} node
	 * @return the default answer for a {@link AInterfaceMethodDecl} node
	 */
	public A caseAInterfaceMethodDecl(AInterfaceMethodDecl node) {
		return defaultPDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AMethodDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPDecl(PDecl)}.
	 * @param node the calling {@link AMethodDecl} node
	 * @return the default answer for a {@link AMethodDecl} node
	 */
	public A caseAMethodDecl(AMethodDecl node) {
		return defaultPDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AConstructorDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPDecl(PDecl)}.
	 * @param node the calling {@link AConstructorDecl} node
	 * @return the default answer for a {@link AConstructorDecl} node
	 */
	public A caseAConstructorDecl(AConstructorDecl node) {
		return defaultPDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLocalDecl(PLocalDecl)}.
	 * @param node the calling {@link ALocalDecl} node
	 * @return the default answer for a {@link ALocalDecl} node
	 */
	public A caseALocalDecl(ALocalDecl node) {
		return defaultPLocalDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AVoidType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AVoidType} node
	 * @return the default answer for a {@link AVoidType} node
	 */
	public A caseAVoidType(AVoidType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link AByteType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AByteType} node
	 * @return the default answer for a {@link AByteType} node
	 */
	public A caseAByteType(AByteType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link AShortType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AShortType} node
	 * @return the default answer for a {@link AShortType} node
	 */
	public A caseAShortType(AShortType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIntType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AIntType} node
	 * @return the default answer for a {@link AIntType} node
	 */
	public A caseAIntType(AIntType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALongType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ALongType} node
	 * @return the default answer for a {@link ALongType} node
	 */
	public A caseALongType(ALongType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACharType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ACharType} node
	 * @return the default answer for a {@link ACharType} node
	 */
	public A caseACharType(ACharType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link AFloatType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AFloatType} node
	 * @return the default answer for a {@link AFloatType} node
	 */
	public A caseAFloatType(AFloatType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ADoubleType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ADoubleType} node
	 * @return the default answer for a {@link ADoubleType} node
	 */
	public A caseADoubleType(ADoubleType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ABooleanType} node
	 * @return the default answer for a {@link ABooleanType} node
	 */
	public A caseABooleanType(ABooleanType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link AArrayType} node
	 * @return the default answer for a {@link AArrayType} node
	 */
	public A caseAArrayType(AArrayType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANamedType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ANamedType} node
	 * @return the default answer for a {@link ANamedType} node
	 */
	public A caseANamedType(ANamedType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANullType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPType(PType)}.
	 * @param node the calling {@link ANullType} node
	 * @return the default answer for a {@link ANullType} node
	 */
	public A caseANullType(ANullType node) {
		return defaultPType(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABody} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBody(PBody)}.
	 * @param node the calling {@link ABody} node
	 * @return the default answer for a {@link ABody} node
	 */
	public A caseABody(ABody node) {
		return defaultPBody(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABlock} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBlock(PBlock)}.
	 * @param node the calling {@link ABlock} node
	 * @return the default answer for a {@link ABlock} node
	 */
	public A caseABlock(ABlock node) {
		return defaultPBlock(node);
	}
	
	/**
	 * Returns the default answer for a {@link AExpStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AExpStm} node
	 * @return the default answer for a {@link AExpStm} node
	 */
	public A caseAExpStm(AExpStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIfThenStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AIfThenStm} node
	 * @return the default answer for a {@link AIfThenStm} node
	 */
	public A caseAIfThenStm(AIfThenStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIfThenElseStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AIfThenElseStm} node
	 * @return the default answer for a {@link AIfThenElseStm} node
	 */
	public A caseAIfThenElseStm(AIfThenElseStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AWhileStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AWhileStm} node
	 * @return the default answer for a {@link AWhileStm} node
	 */
	public A caseAWhileStm(AWhileStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AEmptyStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AEmptyStm} node
	 * @return the default answer for a {@link AEmptyStm} node
	 */
	public A caseAEmptyStm(AEmptyStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABlockStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link ABlockStm} node
	 * @return the default answer for a {@link ABlockStm} node
	 */
	public A caseABlockStm(ABlockStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AVoidReturnStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AVoidReturnStm} node
	 * @return the default answer for a {@link AVoidReturnStm} node
	 */
	public A caseAVoidReturnStm(AVoidReturnStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AValueReturnStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AValueReturnStm} node
	 * @return the default answer for a {@link AValueReturnStm} node
	 */
	public A caseAValueReturnStm(AValueReturnStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalDeclStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link ALocalDeclStm} node
	 * @return the default answer for a {@link ALocalDeclStm} node
	 */
	public A caseALocalDeclStm(ALocalDeclStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AThrowStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AThrowStm} node
	 * @return the default answer for a {@link AThrowStm} node
	 */
	public A caseAThrowStm(AThrowStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link ASuperStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link ASuperStm} node
	 * @return the default answer for a {@link ASuperStm} node
	 */
	public A caseASuperStm(ASuperStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AThisStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AThisStm} node
	 * @return the default answer for a {@link AThisStm} node
	 */
	public A caseAThisStm(AThisStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link AForStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPStm(PStm)}.
	 * @param node the calling {@link AForStm} node
	 * @return the default answer for a {@link AForStm} node
	 */
	public A caseAForStm(AForStm node) {
		return defaultPStm(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABinopExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ABinopExp} node
	 * @return the default answer for a {@link ABinopExp} node
	 */
	public A caseABinopExp(ABinopExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AUnopExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AUnopExp} node
	 * @return the default answer for a {@link AUnopExp} node
	 */
	public A caseAUnopExp(AUnopExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AParensExpExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AParensExpExp} node
	 * @return the default answer for a {@link AParensExpExp} node
	 */
	public A caseAParensExpExp(AParensExpExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIntConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @return the default answer for a {@link AIntConstExp} node
	 */
	public A caseAIntConstExp(AIntConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACharConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ACharConstExp} node
	 * @return the default answer for a {@link ACharConstExp} node
	 */
	public A caseACharConstExp(ACharConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AStringConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AStringConstExp} node
	 * @return the default answer for a {@link AStringConstExp} node
	 */
	public A caseAStringConstExp(AStringConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ABooleanConstExp} node
	 * @return the default answer for a {@link ABooleanConstExp} node
	 */
	public A caseABooleanConstExp(ABooleanConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANullExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ANullExp} node
	 * @return the default answer for a {@link ANullExp} node
	 */
	public A caseANullExp(ANullExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AThisExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AThisExp} node
	 * @return the default answer for a {@link AThisExp} node
	 */
	public A caseAThisExp(AThisExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AStaticInvokeExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AStaticInvokeExp} node
	 * @return the default answer for a {@link AStaticInvokeExp} node
	 */
	public A caseAStaticInvokeExp(AStaticInvokeExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANonstaticInvokeExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ANonstaticInvokeExp} node
	 * @return the default answer for a {@link ANonstaticInvokeExp} node
	 */
	public A caseANonstaticInvokeExp(ANonstaticInvokeExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ASimpleInvokeExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ASimpleInvokeExp} node
	 * @return the default answer for a {@link ASimpleInvokeExp} node
	 */
	public A caseASimpleInvokeExp(ASimpleInvokeExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAmbiguousInvokeExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AAmbiguousInvokeExp} node
	 * @return the default answer for a {@link AAmbiguousInvokeExp} node
	 */
	public A caseAAmbiguousInvokeExp(AAmbiguousInvokeExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANewExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ANewExp} node
	 * @return the default answer for a {@link ANewExp} node
	 */
	public A caseANewExp(ANewExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANewArrayExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ANewArrayExp} node
	 * @return the default answer for a {@link ANewArrayExp} node
	 */
	public A caseANewArrayExp(ANewArrayExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALvalueExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ALvalueExp} node
	 * @return the default answer for a {@link ALvalueExp} node
	 */
	public A caseALvalueExp(ALvalueExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAssignmentExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AAssignmentExp} node
	 * @return the default answer for a {@link AAssignmentExp} node
	 */
	public A caseAAssignmentExp(AAssignmentExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIncDecExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AIncDecExp} node
	 * @return the default answer for a {@link AIncDecExp} node
	 */
	public A caseAIncDecExp(AIncDecExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACastExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ACastExp} node
	 * @return the default answer for a {@link ACastExp} node
	 */
	public A caseACastExp(ACastExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACastRefTypeExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ACastRefTypeExp} node
	 * @return the default answer for a {@link ACastRefTypeExp} node
	 */
	public A caseACastRefTypeExp(ACastRefTypeExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AInstanceofExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AInstanceofExp} node
	 * @return the default answer for a {@link AInstanceofExp} node
	 */
	public A caseAInstanceofExp(AInstanceofExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayLengthExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AArrayLengthExp} node
	 * @return the default answer for a {@link AArrayLengthExp} node
	 */
	public A caseAArrayLengthExp(AArrayLengthExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayCloneExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AArrayCloneExp} node
	 * @return the default answer for a {@link AArrayCloneExp} node
	 */
	public A caseAArrayCloneExp(AArrayCloneExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLvalue(PLvalue)}.
	 * @param node the calling {@link ALocalLvalue} node
	 * @return the default answer for a {@link ALocalLvalue} node
	 */
	public A caseALocalLvalue(ALocalLvalue node) {
		return defaultPLvalue(node);
	}
	
	/**
	 * Returns the default answer for a {@link AStaticFieldLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLvalue(PLvalue)}.
	 * @param node the calling {@link AStaticFieldLvalue} node
	 * @return the default answer for a {@link AStaticFieldLvalue} node
	 */
	public A caseAStaticFieldLvalue(AStaticFieldLvalue node) {
		return defaultPLvalue(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANonstaticFieldLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLvalue(PLvalue)}.
	 * @param node the calling {@link ANonstaticFieldLvalue} node
	 * @return the default answer for a {@link ANonstaticFieldLvalue} node
	 */
	public A caseANonstaticFieldLvalue(ANonstaticFieldLvalue node) {
		return defaultPLvalue(node);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLvalue(PLvalue)}.
	 * @param node the calling {@link AArrayLvalue} node
	 * @return the default answer for a {@link AArrayLvalue} node
	 */
	public A caseAArrayLvalue(AArrayLvalue node) {
		return defaultPLvalue(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAmbiguousNameLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPLvalue(PLvalue)}.
	 * @param node the calling {@link AAmbiguousNameLvalue} node
	 * @return the default answer for a {@link AAmbiguousNameLvalue} node
	 */
	public A caseAAmbiguousNameLvalue(AAmbiguousNameLvalue node) {
		return defaultPLvalue(node);
	}
	
	/**
	 * Returns the default answer for a {@link ATrueBool} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBool(PBool)}.
	 * @param node the calling {@link ATrueBool} node
	 * @return the default answer for a {@link ATrueBool} node
	 */
	public A caseATrueBool(ATrueBool node) {
		return defaultPBool(node);
	}
	
	/**
	 * Returns the default answer for a {@link AFalseBool} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBool(PBool)}.
	 * @param node the calling {@link AFalseBool} node
	 * @return the default answer for a {@link AFalseBool} node
	 */
	public A caseAFalseBool(AFalseBool node) {
		return defaultPBool(node);
	}
	
	/**
	 * Returns the default answer for a {@link APlusBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link APlusBinop} node
	 * @return the default answer for a {@link APlusBinop} node
	 */
	public A caseAPlusBinop(APlusBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AMinusBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @return the default answer for a {@link AMinusBinop} node
	 */
	public A caseAMinusBinop(AMinusBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ATimesBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @return the default answer for a {@link ATimesBinop} node
	 */
	public A caseATimesBinop(ATimesBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ADivideBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @return the default answer for a {@link ADivideBinop} node
	 */
	public A caseADivideBinop(ADivideBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AModuloBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @return the default answer for a {@link AModuloBinop} node
	 */
	public A caseAModuloBinop(AModuloBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AEqBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AEqBinop} node
	 * @return the default answer for a {@link AEqBinop} node
	 */
	public A caseAEqBinop(AEqBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ANeBinop} node
	 * @return the default answer for a {@link ANeBinop} node
	 */
	public A caseANeBinop(ANeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALtBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALtBinop} node
	 * @return the default answer for a {@link ALtBinop} node
	 */
	public A caseALtBinop(ALtBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALeBinop} node
	 * @return the default answer for a {@link ALeBinop} node
	 */
	public A caseALeBinop(ALeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGtBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AGtBinop} node
	 * @return the default answer for a {@link AGtBinop} node
	 */
	public A caseAGtBinop(AGtBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AGeBinop} node
	 * @return the default answer for a {@link AGeBinop} node
	 */
	public A caseAGeBinop(AGeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAndBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AAndBinop} node
	 * @return the default answer for a {@link AAndBinop} node
	 */
	public A caseAAndBinop(AAndBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AOrBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AOrBinop} node
	 * @return the default answer for a {@link AOrBinop} node
	 */
	public A caseAOrBinop(AOrBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AXorBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AXorBinop} node
	 * @return the default answer for a {@link AXorBinop} node
	 */
	public A caseAXorBinop(AXorBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALazyAndBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALazyAndBinop} node
	 * @return the default answer for a {@link ALazyAndBinop} node
	 */
	public A caseALazyAndBinop(ALazyAndBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALazyOrBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALazyOrBinop} node
	 * @return the default answer for a {@link ALazyOrBinop} node
	 */
	public A caseALazyOrBinop(ALazyOrBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAeqBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AAeqBinop} node
	 * @return the default answer for a {@link AAeqBinop} node
	 */
	public A caseAAeqBinop(AAeqBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAneBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AAneBinop} node
	 * @return the default answer for a {@link AAneBinop} node
	 */
	public A caseAAneBinop(AAneBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AConcatBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AConcatBinop} node
	 * @return the default answer for a {@link AConcatBinop} node
	 */
	public A caseAConcatBinop(AConcatBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @return the default answer for a {@link ANegateUnop} node
	 */
	public A caseANegateUnop(ANegateUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AComplementUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @return the default answer for a {@link AComplementUnop} node
	 */
	public A caseAComplementUnop(AComplementUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link ABooleanToStringUnop} node
	 * @return the default answer for a {@link ABooleanToStringUnop} node
	 */
	public A caseABooleanToStringUnop(ABooleanToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AByteToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AByteToStringUnop} node
	 * @return the default answer for a {@link AByteToStringUnop} node
	 */
	public A caseAByteToStringUnop(AByteToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AShortToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AShortToStringUnop} node
	 * @return the default answer for a {@link AShortToStringUnop} node
	 */
	public A caseAShortToStringUnop(AShortToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIntToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AIntToStringUnop} node
	 * @return the default answer for a {@link AIntToStringUnop} node
	 */
	public A caseAIntToStringUnop(AIntToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACharToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link ACharToStringUnop} node
	 * @return the default answer for a {@link ACharToStringUnop} node
	 */
	public A caseACharToStringUnop(ACharToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AObjectToStringUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AObjectToStringUnop} node
	 * @return the default answer for a {@link AObjectToStringUnop} node
	 */
	public A caseAObjectToStringUnop(AObjectToStringUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link APreIncIncDecOp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPIncDecOp(PIncDecOp)}.
	 * @param node the calling {@link APreIncIncDecOp} node
	 * @return the default answer for a {@link APreIncIncDecOp} node
	 */
	public A caseAPreIncIncDecOp(APreIncIncDecOp node) {
		return defaultPIncDecOp(node);
	}
	
	/**
	 * Returns the default answer for a {@link APreDecIncDecOp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPIncDecOp(PIncDecOp)}.
	 * @param node the calling {@link APreDecIncDecOp} node
	 * @return the default answer for a {@link APreDecIncDecOp} node
	 */
	public A caseAPreDecIncDecOp(APreDecIncDecOp node) {
		return defaultPIncDecOp(node);
	}
	
	/**
	 * Returns the default answer for a {@link APostIncIncDecOp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPIncDecOp(PIncDecOp)}.
	 * @param node the calling {@link APostIncIncDecOp} node
	 * @return the default answer for a {@link APostIncIncDecOp} node
	 */
	public A caseAPostIncIncDecOp(APostIncIncDecOp node) {
		return defaultPIncDecOp(node);
	}
	
	/**
	 * Returns the default answer for a {@link APostDecIncDecOp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPIncDecOp(PIncDecOp)}.
	 * @param node the calling {@link APostDecIncDecOp} node
	 * @return the default answer for a {@link APostDecIncDecOp} node
	 */
	public A caseAPostDecIncDecOp(APostDecIncDecOp node) {
		return defaultPIncDecOp(node);
	}
	

	/**
	 * Returns the default answer for a {@link TWhiteSpace} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @return the default answer for a {@link TWhiteSpace} node
	 */
	public A caseTWhiteSpace(TWhiteSpace node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTraditionalComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @return the default answer for a {@link TTraditionalComment} node
	 */
	public A caseTTraditionalComment(TTraditionalComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDocumentationComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @return the default answer for a {@link TDocumentationComment} node
	 */
	public A caseTDocumentationComment(TDocumentationComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TEndOfLineComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @return the default answer for a {@link TEndOfLineComment} node
	 */
	public A caseTEndOfLineComment(TEndOfLineComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAbstract} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAbstract} node
	 * @return the default answer for a {@link TAbstract} node
	 */
	public A caseTAbstract(TAbstract node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TBoolean} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TBoolean} node
	 * @return the default answer for a {@link TBoolean} node
	 */
	public A caseTBoolean(TBoolean node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TBreak} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TBreak} node
	 * @return the default answer for a {@link TBreak} node
	 */
	public A caseTBreak(TBreak node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TByte} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TByte} node
	 * @return the default answer for a {@link TByte} node
	 */
	public A caseTByte(TByte node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TCase} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TCase} node
	 * @return the default answer for a {@link TCase} node
	 */
	public A caseTCase(TCase node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TCatch} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TCatch} node
	 * @return the default answer for a {@link TCatch} node
	 */
	public A caseTCatch(TCatch node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TChar} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TChar} node
	 * @return the default answer for a {@link TChar} node
	 */
	public A caseTChar(TChar node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TClass} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TClass} node
	 * @return the default answer for a {@link TClass} node
	 */
	public A caseTClass(TClass node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TConst} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TConst} node
	 * @return the default answer for a {@link TConst} node
	 */
	public A caseTConst(TConst node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TContinue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TContinue} node
	 * @return the default answer for a {@link TContinue} node
	 */
	public A caseTContinue(TContinue node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDefault} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDefault} node
	 * @return the default answer for a {@link TDefault} node
	 */
	public A caseTDefault(TDefault node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDo} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDo} node
	 * @return the default answer for a {@link TDo} node
	 */
	public A caseTDo(TDo node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDouble} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDouble} node
	 * @return the default answer for a {@link TDouble} node
	 */
	public A caseTDouble(TDouble node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TElse} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TElse} node
	 * @return the default answer for a {@link TElse} node
	 */
	public A caseTElse(TElse node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TExtends} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TExtends} node
	 * @return the default answer for a {@link TExtends} node
	 */
	public A caseTExtends(TExtends node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFinal} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFinal} node
	 * @return the default answer for a {@link TFinal} node
	 */
	public A caseTFinal(TFinal node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFinally} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFinally} node
	 * @return the default answer for a {@link TFinally} node
	 */
	public A caseTFinally(TFinally node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFloat} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFloat} node
	 * @return the default answer for a {@link TFloat} node
	 */
	public A caseTFloat(TFloat node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFor} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFor} node
	 * @return the default answer for a {@link TFor} node
	 */
	public A caseTFor(TFor node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TGoto} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TGoto} node
	 * @return the default answer for a {@link TGoto} node
	 */
	public A caseTGoto(TGoto node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TIf} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TIf} node
	 * @return the default answer for a {@link TIf} node
	 */
	public A caseTIf(TIf node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TImplements} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TImplements} node
	 * @return the default answer for a {@link TImplements} node
	 */
	public A caseTImplements(TImplements node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TImport} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TImport} node
	 * @return the default answer for a {@link TImport} node
	 */
	public A caseTImport(TImport node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TInstanceof} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TInstanceof} node
	 * @return the default answer for a {@link TInstanceof} node
	 */
	public A caseTInstanceof(TInstanceof node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TInt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TInt} node
	 * @return the default answer for a {@link TInt} node
	 */
	public A caseTInt(TInt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TInterface} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TInterface} node
	 * @return the default answer for a {@link TInterface} node
	 */
	public A caseTInterface(TInterface node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLong} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLong} node
	 * @return the default answer for a {@link TLong} node
	 */
	public A caseTLong(TLong node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNative} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNative} node
	 * @return the default answer for a {@link TNative} node
	 */
	public A caseTNative(TNative node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNew} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNew} node
	 * @return the default answer for a {@link TNew} node
	 */
	public A caseTNew(TNew node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPackage} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPackage} node
	 * @return the default answer for a {@link TPackage} node
	 */
	public A caseTPackage(TPackage node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPrivate} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPrivate} node
	 * @return the default answer for a {@link TPrivate} node
	 */
	public A caseTPrivate(TPrivate node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TProtected} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TProtected} node
	 * @return the default answer for a {@link TProtected} node
	 */
	public A caseTProtected(TProtected node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPublic} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPublic} node
	 * @return the default answer for a {@link TPublic} node
	 */
	public A caseTPublic(TPublic node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TReturn} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TReturn} node
	 * @return the default answer for a {@link TReturn} node
	 */
	public A caseTReturn(TReturn node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TShort} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TShort} node
	 * @return the default answer for a {@link TShort} node
	 */
	public A caseTShort(TShort node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TStatic} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TStatic} node
	 * @return the default answer for a {@link TStatic} node
	 */
	public A caseTStatic(TStatic node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TStrictfp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TStrictfp} node
	 * @return the default answer for a {@link TStrictfp} node
	 */
	public A caseTStrictfp(TStrictfp node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TSuper} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TSuper} node
	 * @return the default answer for a {@link TSuper} node
	 */
	public A caseTSuper(TSuper node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TSwitch} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TSwitch} node
	 * @return the default answer for a {@link TSwitch} node
	 */
	public A caseTSwitch(TSwitch node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TSynchronized} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TSynchronized} node
	 * @return the default answer for a {@link TSynchronized} node
	 */
	public A caseTSynchronized(TSynchronized node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TThis} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TThis} node
	 * @return the default answer for a {@link TThis} node
	 */
	public A caseTThis(TThis node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TThrow} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TThrow} node
	 * @return the default answer for a {@link TThrow} node
	 */
	public A caseTThrow(TThrow node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TThrows} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TThrows} node
	 * @return the default answer for a {@link TThrows} node
	 */
	public A caseTThrows(TThrows node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTransient} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTransient} node
	 * @return the default answer for a {@link TTransient} node
	 */
	public A caseTTransient(TTransient node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTry} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTry} node
	 * @return the default answer for a {@link TTry} node
	 */
	public A caseTTry(TTry node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TVoid} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TVoid} node
	 * @return the default answer for a {@link TVoid} node
	 */
	public A caseTVoid(TVoid node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TVolatile} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TVolatile} node
	 * @return the default answer for a {@link TVolatile} node
	 */
	public A caseTVolatile(TVolatile node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TWhile} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TWhile} node
	 * @return the default answer for a {@link TWhile} node
	 */
	public A caseTWhile(TWhile node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTrue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTrue} node
	 * @return the default answer for a {@link TTrue} node
	 */
	public A caseTTrue(TTrue node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFalse} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFalse} node
	 * @return the default answer for a {@link TFalse} node
	 */
	public A caseTFalse(TFalse node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNull} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNull} node
	 * @return the default answer for a {@link TNull} node
	 */
	public A caseTNull(TNull node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLParenthese} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLParenthese} node
	 * @return the default answer for a {@link TLParenthese} node
	 */
	public A caseTLParenthese(TLParenthese node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TRParenthese} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TRParenthese} node
	 * @return the default answer for a {@link TRParenthese} node
	 */
	public A caseTRParenthese(TRParenthese node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLBrace} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLBrace} node
	 * @return the default answer for a {@link TLBrace} node
	 */
	public A caseTLBrace(TLBrace node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TRBrace} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TRBrace} node
	 * @return the default answer for a {@link TRBrace} node
	 */
	public A caseTRBrace(TRBrace node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLBracket} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLBracket} node
	 * @return the default answer for a {@link TLBracket} node
	 */
	public A caseTLBracket(TLBracket node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TRBracket} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TRBracket} node
	 * @return the default answer for a {@link TRBracket} node
	 */
	public A caseTRBracket(TRBracket node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TSemicolon} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TSemicolon} node
	 * @return the default answer for a {@link TSemicolon} node
	 */
	public A caseTSemicolon(TSemicolon node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TComma} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TComma} node
	 * @return the default answer for a {@link TComma} node
	 */
	public A caseTComma(TComma node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDot} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDot} node
	 * @return the default answer for a {@link TDot} node
	 */
	public A caseTDot(TDot node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAssign} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAssign} node
	 * @return the default answer for a {@link TAssign} node
	 */
	public A caseTAssign(TAssign node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TComplement} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TComplement} node
	 * @return the default answer for a {@link TComplement} node
	 */
	public A caseTComplement(TComplement node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAndAnd} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAndAnd} node
	 * @return the default answer for a {@link TAndAnd} node
	 */
	public A caseTAndAnd(TAndAnd node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOrOr} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOrOr} node
	 * @return the default answer for a {@link TOrOr} node
	 */
	public A caseTOrOr(TOrOr node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLt} node
	 * @return the default answer for a {@link TLt} node
	 */
	public A caseTLt(TLt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TGt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TGt} node
	 * @return the default answer for a {@link TGt} node
	 */
	public A caseTGt(TGt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TEq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TEq} node
	 * @return the default answer for a {@link TEq} node
	 */
	public A caseTEq(TEq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLteq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLteq} node
	 * @return the default answer for a {@link TLteq} node
	 */
	public A caseTLteq(TLteq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TGteq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TGteq} node
	 * @return the default answer for a {@link TGteq} node
	 */
	public A caseTGteq(TGteq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNeq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNeq} node
	 * @return the default answer for a {@link TNeq} node
	 */
	public A caseTNeq(TNeq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPlus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPlus} node
	 * @return the default answer for a {@link TPlus} node
	 */
	public A caseTPlus(TPlus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TMinus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TMinus} node
	 * @return the default answer for a {@link TMinus} node
	 */
	public A caseTMinus(TMinus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TStar} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TStar} node
	 * @return the default answer for a {@link TStar} node
	 */
	public A caseTStar(TStar node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDiv} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDiv} node
	 * @return the default answer for a {@link TDiv} node
	 */
	public A caseTDiv(TDiv node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAnd} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAnd} node
	 * @return the default answer for a {@link TAnd} node
	 */
	public A caseTAnd(TAnd node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOr} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOr} node
	 * @return the default answer for a {@link TOr} node
	 */
	public A caseTOr(TOr node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TXor} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TXor} node
	 * @return the default answer for a {@link TXor} node
	 */
	public A caseTXor(TXor node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TMod} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TMod} node
	 * @return the default answer for a {@link TMod} node
	 */
	public A caseTMod(TMod node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPlusPlus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPlusPlus} node
	 * @return the default answer for a {@link TPlusPlus} node
	 */
	public A caseTPlusPlus(TPlusPlus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TMinusMinus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TMinusMinus} node
	 * @return the default answer for a {@link TMinusMinus} node
	 */
	public A caseTMinusMinus(TMinusMinus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TIntegerLiteral} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @return the default answer for a {@link TIntegerLiteral} node
	 */
	public A caseTIntegerLiteral(TIntegerLiteral node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TCharLiteral} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TCharLiteral} node
	 * @return the default answer for a {@link TCharLiteral} node
	 */
	public A caseTCharLiteral(TCharLiteral node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TStringLiteral} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TStringLiteral} node
	 * @return the default answer for a {@link TStringLiteral} node
	 */
	public A caseTStringLiteral(TStringLiteral node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TIdentifier} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TIdentifier} node
	 * @return the default answer for a {@link TIdentifier} node
	 */
	public A caseTIdentifier(TIdentifier node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link PSourceFile} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PSourceFile} node
	 * @return the default answer for a {@link PSourceFile} node
	 */
	public A defaultPSourceFile(PSourceFile node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PProgram} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PProgram} node
	 * @return the default answer for a {@link PProgram} node
	 */
	public A defaultPProgram(PProgram node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PName} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PName} node
	 * @return the default answer for a {@link PName} node
	 */
	public A defaultPName(PName node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PAccess} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PAccess} node
	 * @return the default answer for a {@link PAccess} node
	 */
	public A defaultPAccess(PAccess node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PPackageDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PPackageDecl} node
	 * @return the default answer for a {@link PPackageDecl} node
	 */
	public A defaultPPackageDecl(PPackageDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PImportDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PImportDecl} node
	 * @return the default answer for a {@link PImportDecl} node
	 */
	public A defaultPImportDecl(PImportDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PTypeDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PTypeDecl} node
	 * @return the default answer for a {@link PTypeDecl} node
	 */
	public A defaultPTypeDecl(PTypeDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PDecl} node
	 * @return the default answer for a {@link PDecl} node
	 */
	public A defaultPDecl(PDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PLocalDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PLocalDecl} node
	 * @return the default answer for a {@link PLocalDecl} node
	 */
	public A defaultPLocalDecl(PLocalDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PType} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PType} node
	 * @return the default answer for a {@link PType} node
	 */
	public A defaultPType(PType node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBody} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBody} node
	 * @return the default answer for a {@link PBody} node
	 */
	public A defaultPBody(PBody node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBlock} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBlock} node
	 * @return the default answer for a {@link PBlock} node
	 */
	public A defaultPBlock(PBlock node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PStm} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PStm} node
	 * @return the default answer for a {@link PStm} node
	 */
	public A defaultPStm(PStm node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PExp} node
	 * @return the default answer for a {@link PExp} node
	 */
	public A defaultPExp(PExp node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PLvalue} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PLvalue} node
	 * @return the default answer for a {@link PLvalue} node
	 */
	public A defaultPLvalue(PLvalue node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBool} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBool} node
	 * @return the default answer for a {@link PBool} node
	 */
	public A defaultPBool(PBool node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBinop} node
	 * @return the default answer for a {@link PBinop} node
	 */
	public A defaultPBinop(PBinop node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PUnop} node
	 * @return the default answer for a {@link PUnop} node
	 */
	public A defaultPUnop(PUnop node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PIncDecOp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PIncDecOp} node
	 * @return the default answer for a {@link PIncDecOp} node
	 */
	public A defaultPIncDecOp(PIncDecOp node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link EOF} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link EOF} node
	 * @return the default answer for a {@link EOF} node
	 */
    public A caseEOF(EOF node) {
		return defaultToken(node);
	}

}
