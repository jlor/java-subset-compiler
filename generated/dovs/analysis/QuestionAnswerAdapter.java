
package dovs.analysis;

import dovs.node.*;

/**
 * {@code QuestionAnswerAdapter} implements the default behaviour of the
 * {@link QuestionAnswer} interface. All calls return {@code null} by default.
 *
 * @param <Q> the type of the questions
 * @param <A> the type of the answers
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class QuestionAnswerAdapter<Q,A> implements QuestionAnswer<Q,A> {

	/**
	 * Returns the default answer for a {@link Node}.
	 * @param node the calling {@link Node}
	 * @param question the question
	 * @return the default answer for a {@link Node}
	 */
	public A defaultNode(Node node, Q question) {
		return null;
	}
	
	/**
	 * Returns the default answer for a {@link Token}. The call is deferred to
	 * {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param token the calling {@link Token}
	 * @param question the question
	 * @return the default answer for a {@link Token}
	 */
	public A defaultToken(Token token, Q question) {
		return defaultNode(token, question);
	}
	
	/**
	 * Returns the default answer for a {@link Start} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link Start} node
	 * @param question the question
	 * @return the default answer for a {@link Start} node
	 */
    public A caseStart(Start node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ASourceFile} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPSourceFile(PSourceFile,Object)}.
	 * @param node the calling {@link ASourceFile} node
	 * @param question the question
	 * @return the default answer for a {@link ASourceFile} node
	 */
	public A caseASourceFile(ASourceFile node, Q question) {
		return defaultPSourceFile(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AProgram} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPProgram(PProgram,Object)}.
	 * @param node the calling {@link AProgram} node
	 * @param question the question
	 * @return the default answer for a {@link AProgram} node
	 */
	public A caseAProgram(AProgram node, Q question) {
		return defaultPProgram(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ASimpleName} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPName(PName,Object)}.
	 * @param node the calling {@link ASimpleName} node
	 * @param question the question
	 * @return the default answer for a {@link ASimpleName} node
	 */
	public A caseASimpleName(ASimpleName node, Q question) {
		return defaultPName(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AQualifiedName} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPName(PName,Object)}.
	 * @param node the calling {@link AQualifiedName} node
	 * @param question the question
	 * @return the default answer for a {@link AQualifiedName} node
	 */
	public A caseAQualifiedName(AQualifiedName node, Q question) {
		return defaultPName(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APublicAccess} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPAccess(PAccess,Object)}.
	 * @param node the calling {@link APublicAccess} node
	 * @param question the question
	 * @return the default answer for a {@link APublicAccess} node
	 */
	public A caseAPublicAccess(APublicAccess node, Q question) {
		return defaultPAccess(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AProtectedAccess} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPAccess(PAccess,Object)}.
	 * @param node the calling {@link AProtectedAccess} node
	 * @param question the question
	 * @return the default answer for a {@link AProtectedAccess} node
	 */
	public A caseAProtectedAccess(AProtectedAccess node, Q question) {
		return defaultPAccess(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APackageDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPPackageDecl(PPackageDecl,Object)}.
	 * @param node the calling {@link APackageDecl} node
	 * @param question the question
	 * @return the default answer for a {@link APackageDecl} node
	 */
	public A caseAPackageDecl(APackageDecl node, Q question) {
		return defaultPPackageDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AOnDemandImportDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPImportDecl(PImportDecl,Object)}.
	 * @param node the calling {@link AOnDemandImportDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AOnDemandImportDecl} node
	 */
	public A caseAOnDemandImportDecl(AOnDemandImportDecl node, Q question) {
		return defaultPImportDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ASingleImportDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPImportDecl(PImportDecl,Object)}.
	 * @param node the calling {@link ASingleImportDecl} node
	 * @param question the question
	 * @return the default answer for a {@link ASingleImportDecl} node
	 */
	public A caseASingleImportDecl(ASingleImportDecl node, Q question) {
		return defaultPImportDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AClassTypeDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPTypeDecl(PTypeDecl,Object)}.
	 * @param node the calling {@link AClassTypeDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AClassTypeDecl} node
	 */
	public A caseAClassTypeDecl(AClassTypeDecl node, Q question) {
		return defaultPTypeDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AInterfaceTypeDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPTypeDecl(PTypeDecl,Object)}.
	 * @param node the calling {@link AInterfaceTypeDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AInterfaceTypeDecl} node
	 */
	public A caseAInterfaceTypeDecl(AInterfaceTypeDecl node, Q question) {
		return defaultPTypeDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AFieldDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AFieldDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AFieldDecl} node
	 */
	public A caseAFieldDecl(AFieldDecl node, Q question) {
		return defaultPDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AInterfaceMethodDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AInterfaceMethodDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AInterfaceMethodDecl} node
	 */
	public A caseAInterfaceMethodDecl(AInterfaceMethodDecl node, Q question) {
		return defaultPDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AMethodDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AMethodDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AMethodDecl} node
	 */
	public A caseAMethodDecl(AMethodDecl node, Q question) {
		return defaultPDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AConstructorDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AConstructorDecl} node
	 * @param question the question
	 * @return the default answer for a {@link AConstructorDecl} node
	 */
	public A caseAConstructorDecl(AConstructorDecl node, Q question) {
		return defaultPDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLocalDecl(PLocalDecl,Object)}.
	 * @param node the calling {@link ALocalDecl} node
	 * @param question the question
	 * @return the default answer for a {@link ALocalDecl} node
	 */
	public A caseALocalDecl(ALocalDecl node, Q question) {
		return defaultPLocalDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AVoidType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AVoidType} node
	 * @param question the question
	 * @return the default answer for a {@link AVoidType} node
	 */
	public A caseAVoidType(AVoidType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AByteType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AByteType} node
	 * @param question the question
	 * @return the default answer for a {@link AByteType} node
	 */
	public A caseAByteType(AByteType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AShortType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AShortType} node
	 * @param question the question
	 * @return the default answer for a {@link AShortType} node
	 */
	public A caseAShortType(AShortType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIntType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AIntType} node
	 * @param question the question
	 * @return the default answer for a {@link AIntType} node
	 */
	public A caseAIntType(AIntType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALongType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ALongType} node
	 * @param question the question
	 * @return the default answer for a {@link ALongType} node
	 */
	public A caseALongType(ALongType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACharType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ACharType} node
	 * @param question the question
	 * @return the default answer for a {@link ACharType} node
	 */
	public A caseACharType(ACharType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AFloatType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AFloatType} node
	 * @param question the question
	 * @return the default answer for a {@link AFloatType} node
	 */
	public A caseAFloatType(AFloatType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ADoubleType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ADoubleType} node
	 * @param question the question
	 * @return the default answer for a {@link ADoubleType} node
	 */
	public A caseADoubleType(ADoubleType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ABooleanType} node
	 * @param question the question
	 * @return the default answer for a {@link ABooleanType} node
	 */
	public A caseABooleanType(ABooleanType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AArrayType} node
	 * @param question the question
	 * @return the default answer for a {@link AArrayType} node
	 */
	public A caseAArrayType(AArrayType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANamedType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ANamedType} node
	 * @param question the question
	 * @return the default answer for a {@link ANamedType} node
	 */
	public A caseANamedType(ANamedType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANullType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ANullType} node
	 * @param question the question
	 * @return the default answer for a {@link ANullType} node
	 */
	public A caseANullType(ANullType node, Q question) {
		return defaultPType(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABody} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBody(PBody,Object)}.
	 * @param node the calling {@link ABody} node
	 * @param question the question
	 * @return the default answer for a {@link ABody} node
	 */
	public A caseABody(ABody node, Q question) {
		return defaultPBody(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABlock} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBlock(PBlock,Object)}.
	 * @param node the calling {@link ABlock} node
	 * @param question the question
	 * @return the default answer for a {@link ABlock} node
	 */
	public A caseABlock(ABlock node, Q question) {
		return defaultPBlock(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AExpStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AExpStm} node
	 * @param question the question
	 * @return the default answer for a {@link AExpStm} node
	 */
	public A caseAExpStm(AExpStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIfThenStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AIfThenStm} node
	 * @param question the question
	 * @return the default answer for a {@link AIfThenStm} node
	 */
	public A caseAIfThenStm(AIfThenStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIfThenElseStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AIfThenElseStm} node
	 * @param question the question
	 * @return the default answer for a {@link AIfThenElseStm} node
	 */
	public A caseAIfThenElseStm(AIfThenElseStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AWhileStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AWhileStm} node
	 * @param question the question
	 * @return the default answer for a {@link AWhileStm} node
	 */
	public A caseAWhileStm(AWhileStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AEmptyStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AEmptyStm} node
	 * @param question the question
	 * @return the default answer for a {@link AEmptyStm} node
	 */
	public A caseAEmptyStm(AEmptyStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABlockStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ABlockStm} node
	 * @param question the question
	 * @return the default answer for a {@link ABlockStm} node
	 */
	public A caseABlockStm(ABlockStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AVoidReturnStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AVoidReturnStm} node
	 * @param question the question
	 * @return the default answer for a {@link AVoidReturnStm} node
	 */
	public A caseAVoidReturnStm(AVoidReturnStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AValueReturnStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AValueReturnStm} node
	 * @param question the question
	 * @return the default answer for a {@link AValueReturnStm} node
	 */
	public A caseAValueReturnStm(AValueReturnStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalDeclStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ALocalDeclStm} node
	 * @param question the question
	 * @return the default answer for a {@link ALocalDeclStm} node
	 */
	public A caseALocalDeclStm(ALocalDeclStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AThrowStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AThrowStm} node
	 * @param question the question
	 * @return the default answer for a {@link AThrowStm} node
	 */
	public A caseAThrowStm(AThrowStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ASuperStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ASuperStm} node
	 * @param question the question
	 * @return the default answer for a {@link ASuperStm} node
	 */
	public A caseASuperStm(ASuperStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AThisStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AThisStm} node
	 * @param question the question
	 * @return the default answer for a {@link AThisStm} node
	 */
	public A caseAThisStm(AThisStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AForStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AForStm} node
	 * @param question the question
	 * @return the default answer for a {@link AForStm} node
	 */
	public A caseAForStm(AForStm node, Q question) {
		return defaultPStm(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABinopExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABinopExp} node
	 * @param question the question
	 * @return the default answer for a {@link ABinopExp} node
	 */
	public A caseABinopExp(ABinopExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AUnopExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AUnopExp} node
	 * @param question the question
	 * @return the default answer for a {@link AUnopExp} node
	 */
	public A caseAUnopExp(AUnopExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AParensExpExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AParensExpExp} node
	 * @param question the question
	 * @return the default answer for a {@link AParensExpExp} node
	 */
	public A caseAParensExpExp(AParensExpExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIntConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link AIntConstExp} node
	 */
	public A caseAIntConstExp(AIntConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACharConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACharConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link ACharConstExp} node
	 */
	public A caseACharConstExp(ACharConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AStringConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AStringConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link AStringConstExp} node
	 */
	public A caseAStringConstExp(AStringConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABooleanConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link ABooleanConstExp} node
	 */
	public A caseABooleanConstExp(ABooleanConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANullExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANullExp} node
	 * @param question the question
	 * @return the default answer for a {@link ANullExp} node
	 */
	public A caseANullExp(ANullExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AThisExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AThisExp} node
	 * @param question the question
	 * @return the default answer for a {@link AThisExp} node
	 */
	public A caseAThisExp(AThisExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AStaticInvokeExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AStaticInvokeExp} node
	 * @param question the question
	 * @return the default answer for a {@link AStaticInvokeExp} node
	 */
	public A caseAStaticInvokeExp(AStaticInvokeExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANonstaticInvokeExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANonstaticInvokeExp} node
	 * @param question the question
	 * @return the default answer for a {@link ANonstaticInvokeExp} node
	 */
	public A caseANonstaticInvokeExp(ANonstaticInvokeExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ASimpleInvokeExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ASimpleInvokeExp} node
	 * @param question the question
	 * @return the default answer for a {@link ASimpleInvokeExp} node
	 */
	public A caseASimpleInvokeExp(ASimpleInvokeExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAmbiguousInvokeExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AAmbiguousInvokeExp} node
	 * @param question the question
	 * @return the default answer for a {@link AAmbiguousInvokeExp} node
	 */
	public A caseAAmbiguousInvokeExp(AAmbiguousInvokeExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANewExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANewExp} node
	 * @param question the question
	 * @return the default answer for a {@link ANewExp} node
	 */
	public A caseANewExp(ANewExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANewArrayExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANewArrayExp} node
	 * @param question the question
	 * @return the default answer for a {@link ANewArrayExp} node
	 */
	public A caseANewArrayExp(ANewArrayExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALvalueExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ALvalueExp} node
	 * @param question the question
	 * @return the default answer for a {@link ALvalueExp} node
	 */
	public A caseALvalueExp(ALvalueExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAssignmentExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AAssignmentExp} node
	 * @param question the question
	 * @return the default answer for a {@link AAssignmentExp} node
	 */
	public A caseAAssignmentExp(AAssignmentExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIncDecExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIncDecExp} node
	 * @param question the question
	 * @return the default answer for a {@link AIncDecExp} node
	 */
	public A caseAIncDecExp(AIncDecExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACastExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACastExp} node
	 * @param question the question
	 * @return the default answer for a {@link ACastExp} node
	 */
	public A caseACastExp(ACastExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACastRefTypeExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACastRefTypeExp} node
	 * @param question the question
	 * @return the default answer for a {@link ACastRefTypeExp} node
	 */
	public A caseACastRefTypeExp(ACastRefTypeExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AInstanceofExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AInstanceofExp} node
	 * @param question the question
	 * @return the default answer for a {@link AInstanceofExp} node
	 */
	public A caseAInstanceofExp(AInstanceofExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayLengthExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AArrayLengthExp} node
	 * @param question the question
	 * @return the default answer for a {@link AArrayLengthExp} node
	 */
	public A caseAArrayLengthExp(AArrayLengthExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayCloneExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AArrayCloneExp} node
	 * @param question the question
	 * @return the default answer for a {@link AArrayCloneExp} node
	 */
	public A caseAArrayCloneExp(AArrayCloneExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALocalLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link ALocalLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link ALocalLvalue} node
	 */
	public A caseALocalLvalue(ALocalLvalue node, Q question) {
		return defaultPLvalue(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AStaticFieldLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AStaticFieldLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link AStaticFieldLvalue} node
	 */
	public A caseAStaticFieldLvalue(AStaticFieldLvalue node, Q question) {
		return defaultPLvalue(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANonstaticFieldLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link ANonstaticFieldLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link ANonstaticFieldLvalue} node
	 */
	public A caseANonstaticFieldLvalue(ANonstaticFieldLvalue node, Q question) {
		return defaultPLvalue(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AArrayLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AArrayLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link AArrayLvalue} node
	 */
	public A caseAArrayLvalue(AArrayLvalue node, Q question) {
		return defaultPLvalue(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAmbiguousNameLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AAmbiguousNameLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link AAmbiguousNameLvalue} node
	 */
	public A caseAAmbiguousNameLvalue(AAmbiguousNameLvalue node, Q question) {
		return defaultPLvalue(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ATrueBool} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBool(PBool,Object)}.
	 * @param node the calling {@link ATrueBool} node
	 * @param question the question
	 * @return the default answer for a {@link ATrueBool} node
	 */
	public A caseATrueBool(ATrueBool node, Q question) {
		return defaultPBool(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AFalseBool} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBool(PBool,Object)}.
	 * @param node the calling {@link AFalseBool} node
	 * @param question the question
	 * @return the default answer for a {@link AFalseBool} node
	 */
	public A caseAFalseBool(AFalseBool node, Q question) {
		return defaultPBool(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APlusBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link APlusBinop} node
	 * @param question the question
	 * @return the default answer for a {@link APlusBinop} node
	 */
	public A caseAPlusBinop(APlusBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AMinusBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AMinusBinop} node
	 */
	public A caseAMinusBinop(AMinusBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ATimesBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ATimesBinop} node
	 */
	public A caseATimesBinop(ATimesBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ADivideBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ADivideBinop} node
	 */
	public A caseADivideBinop(ADivideBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AModuloBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AModuloBinop} node
	 */
	public A caseAModuloBinop(AModuloBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AEqBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AEqBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AEqBinop} node
	 */
	public A caseAEqBinop(AEqBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ANeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ANeBinop} node
	 */
	public A caseANeBinop(ANeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALtBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALtBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALtBinop} node
	 */
	public A caseALtBinop(ALtBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALeBinop} node
	 */
	public A caseALeBinop(ALeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGtBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGtBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AGtBinop} node
	 */
	public A caseAGtBinop(AGtBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AGeBinop} node
	 */
	public A caseAGeBinop(AGeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAndBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAndBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AAndBinop} node
	 */
	public A caseAAndBinop(AAndBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AOrBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AOrBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AOrBinop} node
	 */
	public A caseAOrBinop(AOrBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AXorBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AXorBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AXorBinop} node
	 */
	public A caseAXorBinop(AXorBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALazyAndBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALazyAndBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALazyAndBinop} node
	 */
	public A caseALazyAndBinop(ALazyAndBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALazyOrBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALazyOrBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALazyOrBinop} node
	 */
	public A caseALazyOrBinop(ALazyOrBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAeqBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAeqBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AAeqBinop} node
	 */
	public A caseAAeqBinop(AAeqBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAneBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAneBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AAneBinop} node
	 */
	public A caseAAneBinop(AAneBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AConcatBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AConcatBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AConcatBinop} node
	 */
	public A caseAConcatBinop(AConcatBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @param question the question
	 * @return the default answer for a {@link ANegateUnop} node
	 */
	public A caseANegateUnop(ANegateUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AComplementUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AComplementUnop} node
	 */
	public A caseAComplementUnop(AComplementUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABooleanToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ABooleanToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link ABooleanToStringUnop} node
	 */
	public A caseABooleanToStringUnop(ABooleanToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AByteToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AByteToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AByteToStringUnop} node
	 */
	public A caseAByteToStringUnop(AByteToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AShortToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AShortToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AShortToStringUnop} node
	 */
	public A caseAShortToStringUnop(AShortToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIntToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AIntToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AIntToStringUnop} node
	 */
	public A caseAIntToStringUnop(AIntToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACharToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ACharToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link ACharToStringUnop} node
	 */
	public A caseACharToStringUnop(ACharToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AObjectToStringUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AObjectToStringUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AObjectToStringUnop} node
	 */
	public A caseAObjectToStringUnop(AObjectToStringUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APreIncIncDecOp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APreIncIncDecOp} node
	 * @param question the question
	 * @return the default answer for a {@link APreIncIncDecOp} node
	 */
	public A caseAPreIncIncDecOp(APreIncIncDecOp node, Q question) {
		return defaultPIncDecOp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APreDecIncDecOp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APreDecIncDecOp} node
	 * @param question the question
	 * @return the default answer for a {@link APreDecIncDecOp} node
	 */
	public A caseAPreDecIncDecOp(APreDecIncDecOp node, Q question) {
		return defaultPIncDecOp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APostIncIncDecOp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APostIncIncDecOp} node
	 * @param question the question
	 * @return the default answer for a {@link APostIncIncDecOp} node
	 */
	public A caseAPostIncIncDecOp(APostIncIncDecOp node, Q question) {
		return defaultPIncDecOp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APostDecIncDecOp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APostDecIncDecOp} node
	 * @param question the question
	 * @return the default answer for a {@link APostDecIncDecOp} node
	 */
	public A caseAPostDecIncDecOp(APostDecIncDecOp node, Q question) {
		return defaultPIncDecOp(node, question);
	}
	

	/**
	 * Returns the default answer for a {@link TWhiteSpace} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @param question the question
	 * @return the default answer for a {@link TWhiteSpace} node
	 */
	public A caseTWhiteSpace(TWhiteSpace node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTraditionalComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @param question the question
	 * @return the default answer for a {@link TTraditionalComment} node
	 */
	public A caseTTraditionalComment(TTraditionalComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDocumentationComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @param question the question
	 * @return the default answer for a {@link TDocumentationComment} node
	 */
	public A caseTDocumentationComment(TDocumentationComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TEndOfLineComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @param question the question
	 * @return the default answer for a {@link TEndOfLineComment} node
	 */
	public A caseTEndOfLineComment(TEndOfLineComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAbstract} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAbstract} node
	 * @param question the question
	 * @return the default answer for a {@link TAbstract} node
	 */
	public A caseTAbstract(TAbstract node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TBoolean} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TBoolean} node
	 * @param question the question
	 * @return the default answer for a {@link TBoolean} node
	 */
	public A caseTBoolean(TBoolean node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TBreak} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TBreak} node
	 * @param question the question
	 * @return the default answer for a {@link TBreak} node
	 */
	public A caseTBreak(TBreak node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TByte} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TByte} node
	 * @param question the question
	 * @return the default answer for a {@link TByte} node
	 */
	public A caseTByte(TByte node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TCase} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCase} node
	 * @param question the question
	 * @return the default answer for a {@link TCase} node
	 */
	public A caseTCase(TCase node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TCatch} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCatch} node
	 * @param question the question
	 * @return the default answer for a {@link TCatch} node
	 */
	public A caseTCatch(TCatch node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TChar} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TChar} node
	 * @param question the question
	 * @return the default answer for a {@link TChar} node
	 */
	public A caseTChar(TChar node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TClass} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TClass} node
	 * @param question the question
	 * @return the default answer for a {@link TClass} node
	 */
	public A caseTClass(TClass node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TConst} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TConst} node
	 * @param question the question
	 * @return the default answer for a {@link TConst} node
	 */
	public A caseTConst(TConst node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TContinue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TContinue} node
	 * @param question the question
	 * @return the default answer for a {@link TContinue} node
	 */
	public A caseTContinue(TContinue node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDefault} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDefault} node
	 * @param question the question
	 * @return the default answer for a {@link TDefault} node
	 */
	public A caseTDefault(TDefault node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDo} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDo} node
	 * @param question the question
	 * @return the default answer for a {@link TDo} node
	 */
	public A caseTDo(TDo node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDouble} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDouble} node
	 * @param question the question
	 * @return the default answer for a {@link TDouble} node
	 */
	public A caseTDouble(TDouble node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TElse} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TElse} node
	 * @param question the question
	 * @return the default answer for a {@link TElse} node
	 */
	public A caseTElse(TElse node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TExtends} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TExtends} node
	 * @param question the question
	 * @return the default answer for a {@link TExtends} node
	 */
	public A caseTExtends(TExtends node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFinal} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFinal} node
	 * @param question the question
	 * @return the default answer for a {@link TFinal} node
	 */
	public A caseTFinal(TFinal node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFinally} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFinally} node
	 * @param question the question
	 * @return the default answer for a {@link TFinally} node
	 */
	public A caseTFinally(TFinally node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFloat} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFloat} node
	 * @param question the question
	 * @return the default answer for a {@link TFloat} node
	 */
	public A caseTFloat(TFloat node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFor} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFor} node
	 * @param question the question
	 * @return the default answer for a {@link TFor} node
	 */
	public A caseTFor(TFor node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TGoto} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGoto} node
	 * @param question the question
	 * @return the default answer for a {@link TGoto} node
	 */
	public A caseTGoto(TGoto node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TIf} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIf} node
	 * @param question the question
	 * @return the default answer for a {@link TIf} node
	 */
	public A caseTIf(TIf node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TImplements} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TImplements} node
	 * @param question the question
	 * @return the default answer for a {@link TImplements} node
	 */
	public A caseTImplements(TImplements node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TImport} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TImport} node
	 * @param question the question
	 * @return the default answer for a {@link TImport} node
	 */
	public A caseTImport(TImport node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TInstanceof} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInstanceof} node
	 * @param question the question
	 * @return the default answer for a {@link TInstanceof} node
	 */
	public A caseTInstanceof(TInstanceof node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TInt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInt} node
	 * @param question the question
	 * @return the default answer for a {@link TInt} node
	 */
	public A caseTInt(TInt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TInterface} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInterface} node
	 * @param question the question
	 * @return the default answer for a {@link TInterface} node
	 */
	public A caseTInterface(TInterface node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLong} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLong} node
	 * @param question the question
	 * @return the default answer for a {@link TLong} node
	 */
	public A caseTLong(TLong node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNative} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNative} node
	 * @param question the question
	 * @return the default answer for a {@link TNative} node
	 */
	public A caseTNative(TNative node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNew} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNew} node
	 * @param question the question
	 * @return the default answer for a {@link TNew} node
	 */
	public A caseTNew(TNew node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPackage} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPackage} node
	 * @param question the question
	 * @return the default answer for a {@link TPackage} node
	 */
	public A caseTPackage(TPackage node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPrivate} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPrivate} node
	 * @param question the question
	 * @return the default answer for a {@link TPrivate} node
	 */
	public A caseTPrivate(TPrivate node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TProtected} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TProtected} node
	 * @param question the question
	 * @return the default answer for a {@link TProtected} node
	 */
	public A caseTProtected(TProtected node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPublic} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPublic} node
	 * @param question the question
	 * @return the default answer for a {@link TPublic} node
	 */
	public A caseTPublic(TPublic node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TReturn} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TReturn} node
	 * @param question the question
	 * @return the default answer for a {@link TReturn} node
	 */
	public A caseTReturn(TReturn node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TShort} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TShort} node
	 * @param question the question
	 * @return the default answer for a {@link TShort} node
	 */
	public A caseTShort(TShort node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TStatic} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStatic} node
	 * @param question the question
	 * @return the default answer for a {@link TStatic} node
	 */
	public A caseTStatic(TStatic node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TStrictfp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStrictfp} node
	 * @param question the question
	 * @return the default answer for a {@link TStrictfp} node
	 */
	public A caseTStrictfp(TStrictfp node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TSuper} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSuper} node
	 * @param question the question
	 * @return the default answer for a {@link TSuper} node
	 */
	public A caseTSuper(TSuper node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TSwitch} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSwitch} node
	 * @param question the question
	 * @return the default answer for a {@link TSwitch} node
	 */
	public A caseTSwitch(TSwitch node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TSynchronized} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSynchronized} node
	 * @param question the question
	 * @return the default answer for a {@link TSynchronized} node
	 */
	public A caseTSynchronized(TSynchronized node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TThis} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThis} node
	 * @param question the question
	 * @return the default answer for a {@link TThis} node
	 */
	public A caseTThis(TThis node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TThrow} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThrow} node
	 * @param question the question
	 * @return the default answer for a {@link TThrow} node
	 */
	public A caseTThrow(TThrow node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TThrows} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThrows} node
	 * @param question the question
	 * @return the default answer for a {@link TThrows} node
	 */
	public A caseTThrows(TThrows node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTransient} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTransient} node
	 * @param question the question
	 * @return the default answer for a {@link TTransient} node
	 */
	public A caseTTransient(TTransient node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTry} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTry} node
	 * @param question the question
	 * @return the default answer for a {@link TTry} node
	 */
	public A caseTTry(TTry node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TVoid} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TVoid} node
	 * @param question the question
	 * @return the default answer for a {@link TVoid} node
	 */
	public A caseTVoid(TVoid node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TVolatile} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TVolatile} node
	 * @param question the question
	 * @return the default answer for a {@link TVolatile} node
	 */
	public A caseTVolatile(TVolatile node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TWhile} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhile} node
	 * @param question the question
	 * @return the default answer for a {@link TWhile} node
	 */
	public A caseTWhile(TWhile node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTrue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTrue} node
	 * @param question the question
	 * @return the default answer for a {@link TTrue} node
	 */
	public A caseTTrue(TTrue node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFalse} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFalse} node
	 * @param question the question
	 * @return the default answer for a {@link TFalse} node
	 */
	public A caseTFalse(TFalse node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNull} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNull} node
	 * @param question the question
	 * @return the default answer for a {@link TNull} node
	 */
	public A caseTNull(TNull node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLParenthese} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLParenthese} node
	 * @param question the question
	 * @return the default answer for a {@link TLParenthese} node
	 */
	public A caseTLParenthese(TLParenthese node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TRParenthese} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRParenthese} node
	 * @param question the question
	 * @return the default answer for a {@link TRParenthese} node
	 */
	public A caseTRParenthese(TRParenthese node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLBrace} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLBrace} node
	 * @param question the question
	 * @return the default answer for a {@link TLBrace} node
	 */
	public A caseTLBrace(TLBrace node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TRBrace} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRBrace} node
	 * @param question the question
	 * @return the default answer for a {@link TRBrace} node
	 */
	public A caseTRBrace(TRBrace node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLBracket} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLBracket} node
	 * @param question the question
	 * @return the default answer for a {@link TLBracket} node
	 */
	public A caseTLBracket(TLBracket node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TRBracket} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRBracket} node
	 * @param question the question
	 * @return the default answer for a {@link TRBracket} node
	 */
	public A caseTRBracket(TRBracket node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TSemicolon} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSemicolon} node
	 * @param question the question
	 * @return the default answer for a {@link TSemicolon} node
	 */
	public A caseTSemicolon(TSemicolon node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TComma} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComma} node
	 * @param question the question
	 * @return the default answer for a {@link TComma} node
	 */
	public A caseTComma(TComma node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDot} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDot} node
	 * @param question the question
	 * @return the default answer for a {@link TDot} node
	 */
	public A caseTDot(TDot node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAssign} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAssign} node
	 * @param question the question
	 * @return the default answer for a {@link TAssign} node
	 */
	public A caseTAssign(TAssign node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TComplement} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComplement} node
	 * @param question the question
	 * @return the default answer for a {@link TComplement} node
	 */
	public A caseTComplement(TComplement node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAndAnd} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAndAnd} node
	 * @param question the question
	 * @return the default answer for a {@link TAndAnd} node
	 */
	public A caseTAndAnd(TAndAnd node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOrOr} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOrOr} node
	 * @param question the question
	 * @return the default answer for a {@link TOrOr} node
	 */
	public A caseTOrOr(TOrOr node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLt} node
	 * @param question the question
	 * @return the default answer for a {@link TLt} node
	 */
	public A caseTLt(TLt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TGt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGt} node
	 * @param question the question
	 * @return the default answer for a {@link TGt} node
	 */
	public A caseTGt(TGt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TEq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEq} node
	 * @param question the question
	 * @return the default answer for a {@link TEq} node
	 */
	public A caseTEq(TEq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLteq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLteq} node
	 * @param question the question
	 * @return the default answer for a {@link TLteq} node
	 */
	public A caseTLteq(TLteq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TGteq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGteq} node
	 * @param question the question
	 * @return the default answer for a {@link TGteq} node
	 */
	public A caseTGteq(TGteq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNeq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNeq} node
	 * @param question the question
	 * @return the default answer for a {@link TNeq} node
	 */
	public A caseTNeq(TNeq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPlus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPlus} node
	 * @param question the question
	 * @return the default answer for a {@link TPlus} node
	 */
	public A caseTPlus(TPlus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TMinus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMinus} node
	 * @param question the question
	 * @return the default answer for a {@link TMinus} node
	 */
	public A caseTMinus(TMinus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TStar} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStar} node
	 * @param question the question
	 * @return the default answer for a {@link TStar} node
	 */
	public A caseTStar(TStar node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDiv} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDiv} node
	 * @param question the question
	 * @return the default answer for a {@link TDiv} node
	 */
	public A caseTDiv(TDiv node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAnd} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAnd} node
	 * @param question the question
	 * @return the default answer for a {@link TAnd} node
	 */
	public A caseTAnd(TAnd node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOr} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOr} node
	 * @param question the question
	 * @return the default answer for a {@link TOr} node
	 */
	public A caseTOr(TOr node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TXor} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TXor} node
	 * @param question the question
	 * @return the default answer for a {@link TXor} node
	 */
	public A caseTXor(TXor node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TMod} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMod} node
	 * @param question the question
	 * @return the default answer for a {@link TMod} node
	 */
	public A caseTMod(TMod node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPlusPlus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPlusPlus} node
	 * @param question the question
	 * @return the default answer for a {@link TPlusPlus} node
	 */
	public A caseTPlusPlus(TPlusPlus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TMinusMinus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMinusMinus} node
	 * @param question the question
	 * @return the default answer for a {@link TMinusMinus} node
	 */
	public A caseTMinusMinus(TMinusMinus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TIntegerLiteral} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @param question the question
	 * @return the default answer for a {@link TIntegerLiteral} node
	 */
	public A caseTIntegerLiteral(TIntegerLiteral node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TCharLiteral} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCharLiteral} node
	 * @param question the question
	 * @return the default answer for a {@link TCharLiteral} node
	 */
	public A caseTCharLiteral(TCharLiteral node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TStringLiteral} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStringLiteral} node
	 * @param question the question
	 * @return the default answer for a {@link TStringLiteral} node
	 */
	public A caseTStringLiteral(TStringLiteral node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TIdentifier} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIdentifier} node
	 * @param question the question
	 * @return the default answer for a {@link TIdentifier} node
	 */
	public A caseTIdentifier(TIdentifier node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PSourceFile} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PSourceFile} node
	 * @param question the question
	 * @return the default answer for a {@link PSourceFile} node
	 */
	public A defaultPSourceFile(PSourceFile node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PProgram} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PProgram} node
	 * @param question the question
	 * @return the default answer for a {@link PProgram} node
	 */
	public A defaultPProgram(PProgram node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PName} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PName} node
	 * @param question the question
	 * @return the default answer for a {@link PName} node
	 */
	public A defaultPName(PName node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PAccess} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PAccess} node
	 * @param question the question
	 * @return the default answer for a {@link PAccess} node
	 */
	public A defaultPAccess(PAccess node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PPackageDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPackageDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PPackageDecl} node
	 */
	public A defaultPPackageDecl(PPackageDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PImportDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PImportDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PImportDecl} node
	 */
	public A defaultPImportDecl(PImportDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PTypeDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PTypeDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PTypeDecl} node
	 */
	public A defaultPTypeDecl(PTypeDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PDecl} node
	 */
	public A defaultPDecl(PDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PLocalDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PLocalDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PLocalDecl} node
	 */
	public A defaultPLocalDecl(PLocalDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PType} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PType} node
	 * @param question the question
	 * @return the default answer for a {@link PType} node
	 */
	public A defaultPType(PType node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBody} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBody} node
	 * @param question the question
	 * @return the default answer for a {@link PBody} node
	 */
	public A defaultPBody(PBody node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBlock} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBlock} node
	 * @param question the question
	 * @return the default answer for a {@link PBlock} node
	 */
	public A defaultPBlock(PBlock node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PStm} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PStm} node
	 * @param question the question
	 * @return the default answer for a {@link PStm} node
	 */
	public A defaultPStm(PStm node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PExp} node
	 * @param question the question
	 * @return the default answer for a {@link PExp} node
	 */
	public A defaultPExp(PExp node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PLvalue} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PLvalue} node
	 * @param question the question
	 * @return the default answer for a {@link PLvalue} node
	 */
	public A defaultPLvalue(PLvalue node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBool} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBool} node
	 * @param question the question
	 * @return the default answer for a {@link PBool} node
	 */
	public A defaultPBool(PBool node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBinop} node
	 * @param question the question
	 * @return the default answer for a {@link PBinop} node
	 */
	public A defaultPBinop(PBinop node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PUnop} node
	 * @param question the question
	 * @return the default answer for a {@link PUnop} node
	 */
	public A defaultPUnop(PUnop node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PIncDecOp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PIncDecOp} node
	 * @param question the question
	 * @return the default answer for a {@link PIncDecOp} node
	 */
	public A defaultPIncDecOp(PIncDecOp node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link EOF} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link EOF} node
	 * @param question the question
	 * @return the default answer for a {@link EOF} node
	 */
    public A caseEOF(EOF node, Q question) {
		return defaultToken(node, question);
	}

}
