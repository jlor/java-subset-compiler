
package dovs.analysis;

import dovs.node.*;

/**
 * {@code QuestionAdapter} implements the default behaviour of the {@link Question}
 * interface.
 *
 * @param <Q> the type of the questions
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class QuestionAdapter<Q> implements Question<Q> {

	/**
	 * Default action for a question to a {@link Node}.
	 * @param node the calling {@link Node}
	 * @param question the provided question
	 */
	public void defaultNode(Node node, Q question) {
		// empty body
	}
	
	/**
	 * Default action for a question to a {@link Token}. 
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param token the calling {@link Token}
	 * @param question the provided question
	 */
	public void defaultToken(Token token, Q question) {
		defaultNode(token, question);
	}
	
	/**
	 * Action for applying a question to a {@link Start} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link Start} node
	 * @param question the provided question
	 */
    public void caseStart(Start node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ASourceFile} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPSourceFile(PSourceFile,Object)}.
	 * @param node the calling {@link ASourceFile} node
	 * @param question the provided question
	 */
	public void caseASourceFile(ASourceFile node, Q question) {
		defaultPSourceFile(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AProgram} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPProgram(PProgram,Object)}.
	 * @param node the calling {@link AProgram} node
	 * @param question the provided question
	 */
	public void caseAProgram(AProgram node, Q question) {
		defaultPProgram(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ASimpleName} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPName(PName,Object)}.
	 * @param node the calling {@link ASimpleName} node
	 * @param question the provided question
	 */
	public void caseASimpleName(ASimpleName node, Q question) {
		defaultPName(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AQualifiedName} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPName(PName,Object)}.
	 * @param node the calling {@link AQualifiedName} node
	 * @param question the provided question
	 */
	public void caseAQualifiedName(AQualifiedName node, Q question) {
		defaultPName(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APublicAccess} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPAccess(PAccess,Object)}.
	 * @param node the calling {@link APublicAccess} node
	 * @param question the provided question
	 */
	public void caseAPublicAccess(APublicAccess node, Q question) {
		defaultPAccess(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AProtectedAccess} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPAccess(PAccess,Object)}.
	 * @param node the calling {@link AProtectedAccess} node
	 * @param question the provided question
	 */
	public void caseAProtectedAccess(AProtectedAccess node, Q question) {
		defaultPAccess(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APackageDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPPackageDecl(PPackageDecl,Object)}.
	 * @param node the calling {@link APackageDecl} node
	 * @param question the provided question
	 */
	public void caseAPackageDecl(APackageDecl node, Q question) {
		defaultPPackageDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AOnDemandImportDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPImportDecl(PImportDecl,Object)}.
	 * @param node the calling {@link AOnDemandImportDecl} node
	 * @param question the provided question
	 */
	public void caseAOnDemandImportDecl(AOnDemandImportDecl node, Q question) {
		defaultPImportDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ASingleImportDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPImportDecl(PImportDecl,Object)}.
	 * @param node the calling {@link ASingleImportDecl} node
	 * @param question the provided question
	 */
	public void caseASingleImportDecl(ASingleImportDecl node, Q question) {
		defaultPImportDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AClassTypeDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPTypeDecl(PTypeDecl,Object)}.
	 * @param node the calling {@link AClassTypeDecl} node
	 * @param question the provided question
	 */
	public void caseAClassTypeDecl(AClassTypeDecl node, Q question) {
		defaultPTypeDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AInterfaceTypeDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPTypeDecl(PTypeDecl,Object)}.
	 * @param node the calling {@link AInterfaceTypeDecl} node
	 * @param question the provided question
	 */
	public void caseAInterfaceTypeDecl(AInterfaceTypeDecl node, Q question) {
		defaultPTypeDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AFieldDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AFieldDecl} node
	 * @param question the provided question
	 */
	public void caseAFieldDecl(AFieldDecl node, Q question) {
		defaultPDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AInterfaceMethodDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AInterfaceMethodDecl} node
	 * @param question the provided question
	 */
	public void caseAInterfaceMethodDecl(AInterfaceMethodDecl node, Q question) {
		defaultPDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AMethodDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AMethodDecl} node
	 * @param question the provided question
	 */
	public void caseAMethodDecl(AMethodDecl node, Q question) {
		defaultPDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AConstructorDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPDecl(PDecl,Object)}.
	 * @param node the calling {@link AConstructorDecl} node
	 * @param question the provided question
	 */
	public void caseAConstructorDecl(AConstructorDecl node, Q question) {
		defaultPDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALocalDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLocalDecl(PLocalDecl,Object)}.
	 * @param node the calling {@link ALocalDecl} node
	 * @param question the provided question
	 */
	public void caseALocalDecl(ALocalDecl node, Q question) {
		defaultPLocalDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AVoidType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AVoidType} node
	 * @param question the provided question
	 */
	public void caseAVoidType(AVoidType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AByteType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AByteType} node
	 * @param question the provided question
	 */
	public void caseAByteType(AByteType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AShortType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AShortType} node
	 * @param question the provided question
	 */
	public void caseAShortType(AShortType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIntType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AIntType} node
	 * @param question the provided question
	 */
	public void caseAIntType(AIntType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALongType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ALongType} node
	 * @param question the provided question
	 */
	public void caseALongType(ALongType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACharType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ACharType} node
	 * @param question the provided question
	 */
	public void caseACharType(ACharType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AFloatType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AFloatType} node
	 * @param question the provided question
	 */
	public void caseAFloatType(AFloatType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ADoubleType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ADoubleType} node
	 * @param question the provided question
	 */
	public void caseADoubleType(ADoubleType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABooleanType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ABooleanType} node
	 * @param question the provided question
	 */
	public void caseABooleanType(ABooleanType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AArrayType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link AArrayType} node
	 * @param question the provided question
	 */
	public void caseAArrayType(AArrayType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANamedType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ANamedType} node
	 * @param question the provided question
	 */
	public void caseANamedType(ANamedType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANullType} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPType(PType,Object)}.
	 * @param node the calling {@link ANullType} node
	 * @param question the provided question
	 */
	public void caseANullType(ANullType node, Q question) {
		defaultPType(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABody} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBody(PBody,Object)}.
	 * @param node the calling {@link ABody} node
	 * @param question the provided question
	 */
	public void caseABody(ABody node, Q question) {
		defaultPBody(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABlock} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBlock(PBlock,Object)}.
	 * @param node the calling {@link ABlock} node
	 * @param question the provided question
	 */
	public void caseABlock(ABlock node, Q question) {
		defaultPBlock(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AExpStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AExpStm} node
	 * @param question the provided question
	 */
	public void caseAExpStm(AExpStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIfThenStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AIfThenStm} node
	 * @param question the provided question
	 */
	public void caseAIfThenStm(AIfThenStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIfThenElseStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AIfThenElseStm} node
	 * @param question the provided question
	 */
	public void caseAIfThenElseStm(AIfThenElseStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AWhileStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AWhileStm} node
	 * @param question the provided question
	 */
	public void caseAWhileStm(AWhileStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AEmptyStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AEmptyStm} node
	 * @param question the provided question
	 */
	public void caseAEmptyStm(AEmptyStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABlockStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ABlockStm} node
	 * @param question the provided question
	 */
	public void caseABlockStm(ABlockStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AVoidReturnStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AVoidReturnStm} node
	 * @param question the provided question
	 */
	public void caseAVoidReturnStm(AVoidReturnStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AValueReturnStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AValueReturnStm} node
	 * @param question the provided question
	 */
	public void caseAValueReturnStm(AValueReturnStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALocalDeclStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ALocalDeclStm} node
	 * @param question the provided question
	 */
	public void caseALocalDeclStm(ALocalDeclStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AThrowStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AThrowStm} node
	 * @param question the provided question
	 */
	public void caseAThrowStm(AThrowStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ASuperStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link ASuperStm} node
	 * @param question the provided question
	 */
	public void caseASuperStm(ASuperStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AThisStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AThisStm} node
	 * @param question the provided question
	 */
	public void caseAThisStm(AThisStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AForStm} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPStm(PStm,Object)}.
	 * @param node the calling {@link AForStm} node
	 * @param question the provided question
	 */
	public void caseAForStm(AForStm node, Q question) {
		defaultPStm(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABinopExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABinopExp} node
	 * @param question the provided question
	 */
	public void caseABinopExp(ABinopExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AUnopExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AUnopExp} node
	 * @param question the provided question
	 */
	public void caseAUnopExp(AUnopExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AParensExpExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AParensExpExp} node
	 * @param question the provided question
	 */
	public void caseAParensExpExp(AParensExpExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIntConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @param question the provided question
	 */
	public void caseAIntConstExp(AIntConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACharConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACharConstExp} node
	 * @param question the provided question
	 */
	public void caseACharConstExp(ACharConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AStringConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AStringConstExp} node
	 * @param question the provided question
	 */
	public void caseAStringConstExp(AStringConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABooleanConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABooleanConstExp} node
	 * @param question the provided question
	 */
	public void caseABooleanConstExp(ABooleanConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANullExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANullExp} node
	 * @param question the provided question
	 */
	public void caseANullExp(ANullExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AThisExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AThisExp} node
	 * @param question the provided question
	 */
	public void caseAThisExp(AThisExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AStaticInvokeExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AStaticInvokeExp} node
	 * @param question the provided question
	 */
	public void caseAStaticInvokeExp(AStaticInvokeExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANonstaticInvokeExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANonstaticInvokeExp} node
	 * @param question the provided question
	 */
	public void caseANonstaticInvokeExp(ANonstaticInvokeExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ASimpleInvokeExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ASimpleInvokeExp} node
	 * @param question the provided question
	 */
	public void caseASimpleInvokeExp(ASimpleInvokeExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAmbiguousInvokeExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AAmbiguousInvokeExp} node
	 * @param question the provided question
	 */
	public void caseAAmbiguousInvokeExp(AAmbiguousInvokeExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANewExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANewExp} node
	 * @param question the provided question
	 */
	public void caseANewExp(ANewExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANewArrayExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ANewArrayExp} node
	 * @param question the provided question
	 */
	public void caseANewArrayExp(ANewArrayExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALvalueExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ALvalueExp} node
	 * @param question the provided question
	 */
	public void caseALvalueExp(ALvalueExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAssignmentExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AAssignmentExp} node
	 * @param question the provided question
	 */
	public void caseAAssignmentExp(AAssignmentExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIncDecExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIncDecExp} node
	 * @param question the provided question
	 */
	public void caseAIncDecExp(AIncDecExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACastExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACastExp} node
	 * @param question the provided question
	 */
	public void caseACastExp(ACastExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACastRefTypeExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACastRefTypeExp} node
	 * @param question the provided question
	 */
	public void caseACastRefTypeExp(ACastRefTypeExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AInstanceofExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AInstanceofExp} node
	 * @param question the provided question
	 */
	public void caseAInstanceofExp(AInstanceofExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AArrayLengthExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AArrayLengthExp} node
	 * @param question the provided question
	 */
	public void caseAArrayLengthExp(AArrayLengthExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AArrayCloneExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AArrayCloneExp} node
	 * @param question the provided question
	 */
	public void caseAArrayCloneExp(AArrayCloneExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALocalLvalue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link ALocalLvalue} node
	 * @param question the provided question
	 */
	public void caseALocalLvalue(ALocalLvalue node, Q question) {
		defaultPLvalue(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AStaticFieldLvalue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AStaticFieldLvalue} node
	 * @param question the provided question
	 */
	public void caseAStaticFieldLvalue(AStaticFieldLvalue node, Q question) {
		defaultPLvalue(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANonstaticFieldLvalue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link ANonstaticFieldLvalue} node
	 * @param question the provided question
	 */
	public void caseANonstaticFieldLvalue(ANonstaticFieldLvalue node, Q question) {
		defaultPLvalue(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AArrayLvalue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AArrayLvalue} node
	 * @param question the provided question
	 */
	public void caseAArrayLvalue(AArrayLvalue node, Q question) {
		defaultPLvalue(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAmbiguousNameLvalue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPLvalue(PLvalue,Object)}.
	 * @param node the calling {@link AAmbiguousNameLvalue} node
	 * @param question the provided question
	 */
	public void caseAAmbiguousNameLvalue(AAmbiguousNameLvalue node, Q question) {
		defaultPLvalue(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ATrueBool} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBool(PBool,Object)}.
	 * @param node the calling {@link ATrueBool} node
	 * @param question the provided question
	 */
	public void caseATrueBool(ATrueBool node, Q question) {
		defaultPBool(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AFalseBool} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBool(PBool,Object)}.
	 * @param node the calling {@link AFalseBool} node
	 * @param question the provided question
	 */
	public void caseAFalseBool(AFalseBool node, Q question) {
		defaultPBool(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APlusBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link APlusBinop} node
	 * @param question the provided question
	 */
	public void caseAPlusBinop(APlusBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AMinusBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @param question the provided question
	 */
	public void caseAMinusBinop(AMinusBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ATimesBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @param question the provided question
	 */
	public void caseATimesBinop(ATimesBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ADivideBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @param question the provided question
	 */
	public void caseADivideBinop(ADivideBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AModuloBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @param question the provided question
	 */
	public void caseAModuloBinop(AModuloBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AEqBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AEqBinop} node
	 * @param question the provided question
	 */
	public void caseAEqBinop(AEqBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ANeBinop} node
	 * @param question the provided question
	 */
	public void caseANeBinop(ANeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALtBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALtBinop} node
	 * @param question the provided question
	 */
	public void caseALtBinop(ALtBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALeBinop} node
	 * @param question the provided question
	 */
	public void caseALeBinop(ALeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGtBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGtBinop} node
	 * @param question the provided question
	 */
	public void caseAGtBinop(AGtBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGeBinop} node
	 * @param question the provided question
	 */
	public void caseAGeBinop(AGeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAndBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAndBinop} node
	 * @param question the provided question
	 */
	public void caseAAndBinop(AAndBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AOrBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AOrBinop} node
	 * @param question the provided question
	 */
	public void caseAOrBinop(AOrBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AXorBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AXorBinop} node
	 * @param question the provided question
	 */
	public void caseAXorBinop(AXorBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALazyAndBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALazyAndBinop} node
	 * @param question the provided question
	 */
	public void caseALazyAndBinop(ALazyAndBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALazyOrBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALazyOrBinop} node
	 * @param question the provided question
	 */
	public void caseALazyOrBinop(ALazyOrBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAeqBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAeqBinop} node
	 * @param question the provided question
	 */
	public void caseAAeqBinop(AAeqBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAneBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAneBinop} node
	 * @param question the provided question
	 */
	public void caseAAneBinop(AAneBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AConcatBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AConcatBinop} node
	 * @param question the provided question
	 */
	public void caseAConcatBinop(AConcatBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANegateUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @param question the provided question
	 */
	public void caseANegateUnop(ANegateUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AComplementUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @param question the provided question
	 */
	public void caseAComplementUnop(AComplementUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABooleanToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ABooleanToStringUnop} node
	 * @param question the provided question
	 */
	public void caseABooleanToStringUnop(ABooleanToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AByteToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AByteToStringUnop} node
	 * @param question the provided question
	 */
	public void caseAByteToStringUnop(AByteToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AShortToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AShortToStringUnop} node
	 * @param question the provided question
	 */
	public void caseAShortToStringUnop(AShortToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIntToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AIntToStringUnop} node
	 * @param question the provided question
	 */
	public void caseAIntToStringUnop(AIntToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACharToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ACharToStringUnop} node
	 * @param question the provided question
	 */
	public void caseACharToStringUnop(ACharToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AObjectToStringUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AObjectToStringUnop} node
	 * @param question the provided question
	 */
	public void caseAObjectToStringUnop(AObjectToStringUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APreIncIncDecOp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APreIncIncDecOp} node
	 * @param question the provided question
	 */
	public void caseAPreIncIncDecOp(APreIncIncDecOp node, Q question) {
		defaultPIncDecOp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APreDecIncDecOp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APreDecIncDecOp} node
	 * @param question the provided question
	 */
	public void caseAPreDecIncDecOp(APreDecIncDecOp node, Q question) {
		defaultPIncDecOp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APostIncIncDecOp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APostIncIncDecOp} node
	 * @param question the provided question
	 */
	public void caseAPostIncIncDecOp(APostIncIncDecOp node, Q question) {
		defaultPIncDecOp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APostDecIncDecOp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPIncDecOp(PIncDecOp,Object)}.
	 * @param node the calling {@link APostDecIncDecOp} node
	 * @param question the provided question
	 */
	public void caseAPostDecIncDecOp(APostDecIncDecOp node, Q question) {
		defaultPIncDecOp(node, question);
	}
	

	/**
	 * Action for applying a question to a {@link TWhiteSpace} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @param question the provided question
	 */
	public void caseTWhiteSpace(TWhiteSpace node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTraditionalComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @param question the provided question
	 */
	public void caseTTraditionalComment(TTraditionalComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDocumentationComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @param question the provided question
	 */
	public void caseTDocumentationComment(TDocumentationComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TEndOfLineComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @param question the provided question
	 */
	public void caseTEndOfLineComment(TEndOfLineComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAbstract} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAbstract} node
	 * @param question the provided question
	 */
	public void caseTAbstract(TAbstract node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TBoolean} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TBoolean} node
	 * @param question the provided question
	 */
	public void caseTBoolean(TBoolean node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TBreak} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TBreak} node
	 * @param question the provided question
	 */
	public void caseTBreak(TBreak node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TByte} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TByte} node
	 * @param question the provided question
	 */
	public void caseTByte(TByte node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TCase} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCase} node
	 * @param question the provided question
	 */
	public void caseTCase(TCase node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TCatch} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCatch} node
	 * @param question the provided question
	 */
	public void caseTCatch(TCatch node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TChar} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TChar} node
	 * @param question the provided question
	 */
	public void caseTChar(TChar node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TClass} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TClass} node
	 * @param question the provided question
	 */
	public void caseTClass(TClass node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TConst} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TConst} node
	 * @param question the provided question
	 */
	public void caseTConst(TConst node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TContinue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TContinue} node
	 * @param question the provided question
	 */
	public void caseTContinue(TContinue node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDefault} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDefault} node
	 * @param question the provided question
	 */
	public void caseTDefault(TDefault node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDo} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDo} node
	 * @param question the provided question
	 */
	public void caseTDo(TDo node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDouble} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDouble} node
	 * @param question the provided question
	 */
	public void caseTDouble(TDouble node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TElse} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TElse} node
	 * @param question the provided question
	 */
	public void caseTElse(TElse node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TExtends} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TExtends} node
	 * @param question the provided question
	 */
	public void caseTExtends(TExtends node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFinal} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFinal} node
	 * @param question the provided question
	 */
	public void caseTFinal(TFinal node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFinally} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFinally} node
	 * @param question the provided question
	 */
	public void caseTFinally(TFinally node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFloat} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFloat} node
	 * @param question the provided question
	 */
	public void caseTFloat(TFloat node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFor} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFor} node
	 * @param question the provided question
	 */
	public void caseTFor(TFor node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TGoto} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGoto} node
	 * @param question the provided question
	 */
	public void caseTGoto(TGoto node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TIf} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIf} node
	 * @param question the provided question
	 */
	public void caseTIf(TIf node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TImplements} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TImplements} node
	 * @param question the provided question
	 */
	public void caseTImplements(TImplements node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TImport} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TImport} node
	 * @param question the provided question
	 */
	public void caseTImport(TImport node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TInstanceof} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInstanceof} node
	 * @param question the provided question
	 */
	public void caseTInstanceof(TInstanceof node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TInt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInt} node
	 * @param question the provided question
	 */
	public void caseTInt(TInt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TInterface} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TInterface} node
	 * @param question the provided question
	 */
	public void caseTInterface(TInterface node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLong} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLong} node
	 * @param question the provided question
	 */
	public void caseTLong(TLong node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNative} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNative} node
	 * @param question the provided question
	 */
	public void caseTNative(TNative node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNew} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNew} node
	 * @param question the provided question
	 */
	public void caseTNew(TNew node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPackage} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPackage} node
	 * @param question the provided question
	 */
	public void caseTPackage(TPackage node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPrivate} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPrivate} node
	 * @param question the provided question
	 */
	public void caseTPrivate(TPrivate node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TProtected} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TProtected} node
	 * @param question the provided question
	 */
	public void caseTProtected(TProtected node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPublic} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPublic} node
	 * @param question the provided question
	 */
	public void caseTPublic(TPublic node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TReturn} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TReturn} node
	 * @param question the provided question
	 */
	public void caseTReturn(TReturn node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TShort} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TShort} node
	 * @param question the provided question
	 */
	public void caseTShort(TShort node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TStatic} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStatic} node
	 * @param question the provided question
	 */
	public void caseTStatic(TStatic node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TStrictfp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStrictfp} node
	 * @param question the provided question
	 */
	public void caseTStrictfp(TStrictfp node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TSuper} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSuper} node
	 * @param question the provided question
	 */
	public void caseTSuper(TSuper node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TSwitch} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSwitch} node
	 * @param question the provided question
	 */
	public void caseTSwitch(TSwitch node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TSynchronized} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSynchronized} node
	 * @param question the provided question
	 */
	public void caseTSynchronized(TSynchronized node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TThis} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThis} node
	 * @param question the provided question
	 */
	public void caseTThis(TThis node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TThrow} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThrow} node
	 * @param question the provided question
	 */
	public void caseTThrow(TThrow node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TThrows} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TThrows} node
	 * @param question the provided question
	 */
	public void caseTThrows(TThrows node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTransient} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTransient} node
	 * @param question the provided question
	 */
	public void caseTTransient(TTransient node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTry} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTry} node
	 * @param question the provided question
	 */
	public void caseTTry(TTry node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TVoid} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TVoid} node
	 * @param question the provided question
	 */
	public void caseTVoid(TVoid node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TVolatile} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TVolatile} node
	 * @param question the provided question
	 */
	public void caseTVolatile(TVolatile node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TWhile} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhile} node
	 * @param question the provided question
	 */
	public void caseTWhile(TWhile node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTrue} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTrue} node
	 * @param question the provided question
	 */
	public void caseTTrue(TTrue node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFalse} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFalse} node
	 * @param question the provided question
	 */
	public void caseTFalse(TFalse node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNull} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNull} node
	 * @param question the provided question
	 */
	public void caseTNull(TNull node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLParenthese} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLParenthese} node
	 * @param question the provided question
	 */
	public void caseTLParenthese(TLParenthese node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TRParenthese} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRParenthese} node
	 * @param question the provided question
	 */
	public void caseTRParenthese(TRParenthese node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLBrace} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLBrace} node
	 * @param question the provided question
	 */
	public void caseTLBrace(TLBrace node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TRBrace} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRBrace} node
	 * @param question the provided question
	 */
	public void caseTRBrace(TRBrace node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLBracket} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLBracket} node
	 * @param question the provided question
	 */
	public void caseTLBracket(TLBracket node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TRBracket} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRBracket} node
	 * @param question the provided question
	 */
	public void caseTRBracket(TRBracket node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TSemicolon} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TSemicolon} node
	 * @param question the provided question
	 */
	public void caseTSemicolon(TSemicolon node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TComma} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComma} node
	 * @param question the provided question
	 */
	public void caseTComma(TComma node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDot} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDot} node
	 * @param question the provided question
	 */
	public void caseTDot(TDot node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAssign} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAssign} node
	 * @param question the provided question
	 */
	public void caseTAssign(TAssign node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TComplement} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComplement} node
	 * @param question the provided question
	 */
	public void caseTComplement(TComplement node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAndAnd} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAndAnd} node
	 * @param question the provided question
	 */
	public void caseTAndAnd(TAndAnd node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOrOr} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOrOr} node
	 * @param question the provided question
	 */
	public void caseTOrOr(TOrOr node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLt} node
	 * @param question the provided question
	 */
	public void caseTLt(TLt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TGt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGt} node
	 * @param question the provided question
	 */
	public void caseTGt(TGt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TEq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEq} node
	 * @param question the provided question
	 */
	public void caseTEq(TEq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLteq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLteq} node
	 * @param question the provided question
	 */
	public void caseTLteq(TLteq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TGteq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGteq} node
	 * @param question the provided question
	 */
	public void caseTGteq(TGteq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNeq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNeq} node
	 * @param question the provided question
	 */
	public void caseTNeq(TNeq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPlus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPlus} node
	 * @param question the provided question
	 */
	public void caseTPlus(TPlus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TMinus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMinus} node
	 * @param question the provided question
	 */
	public void caseTMinus(TMinus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TStar} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStar} node
	 * @param question the provided question
	 */
	public void caseTStar(TStar node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDiv} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDiv} node
	 * @param question the provided question
	 */
	public void caseTDiv(TDiv node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAnd} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAnd} node
	 * @param question the provided question
	 */
	public void caseTAnd(TAnd node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOr} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOr} node
	 * @param question the provided question
	 */
	public void caseTOr(TOr node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TXor} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TXor} node
	 * @param question the provided question
	 */
	public void caseTXor(TXor node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TMod} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMod} node
	 * @param question the provided question
	 */
	public void caseTMod(TMod node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPlusPlus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPlusPlus} node
	 * @param question the provided question
	 */
	public void caseTPlusPlus(TPlusPlus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TMinusMinus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TMinusMinus} node
	 * @param question the provided question
	 */
	public void caseTMinusMinus(TMinusMinus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TIntegerLiteral} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @param question the provided question
	 */
	public void caseTIntegerLiteral(TIntegerLiteral node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TCharLiteral} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCharLiteral} node
	 * @param question the provided question
	 */
	public void caseTCharLiteral(TCharLiteral node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TStringLiteral} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TStringLiteral} node
	 * @param question the provided question
	 */
	public void caseTStringLiteral(TStringLiteral node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TIdentifier} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIdentifier} node
	 * @param question the provided question
	 */
	public void caseTIdentifier(TIdentifier node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PSourceFile} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PSourceFile} node
	 * @param question the provided question
	 */
	public void defaultPSourceFile(PSourceFile node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PProgram} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PProgram} node
	 * @param question the provided question
	 */
	public void defaultPProgram(PProgram node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PName} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PName} node
	 * @param question the provided question
	 */
	public void defaultPName(PName node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PAccess} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PAccess} node
	 * @param question the provided question
	 */
	public void defaultPAccess(PAccess node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PPackageDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPackageDecl} node
	 * @param question the provided question
	 */
	public void defaultPPackageDecl(PPackageDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PImportDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PImportDecl} node
	 * @param question the provided question
	 */
	public void defaultPImportDecl(PImportDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PTypeDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PTypeDecl} node
	 * @param question the provided question
	 */
	public void defaultPTypeDecl(PTypeDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PDecl} node
	 * @param question the provided question
	 */
	public void defaultPDecl(PDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PLocalDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PLocalDecl} node
	 * @param question the provided question
	 */
	public void defaultPLocalDecl(PLocalDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PType} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PType} node
	 * @param question the provided question
	 */
	public void defaultPType(PType node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBody} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBody} node
	 * @param question the provided question
	 */
	public void defaultPBody(PBody node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBlock} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBlock} node
	 * @param question the provided question
	 */
	public void defaultPBlock(PBlock node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PStm} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PStm} node
	 * @param question the provided question
	 */
	public void defaultPStm(PStm node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PExp} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PExp} node
	 * @param question the provided question
	 */
	public void defaultPExp(PExp node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PLvalue} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PLvalue} node
	 * @param question the provided question
	 */
	public void defaultPLvalue(PLvalue node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBool} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBool} node
	 * @param question the provided question
	 */
	public void defaultPBool(PBool node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBinop} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBinop} node
	 * @param question the provided question
	 */
	public void defaultPBinop(PBinop node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PUnop} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PUnop} node
	 * @param question the provided question
	 */
	public void defaultPUnop(PUnop node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PIncDecOp} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PIncDecOp} node
	 * @param question the provided question
	 */
	public void defaultPIncDecOp(PIncDecOp node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link EOF} node.
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link EOF} node
	 * @param question the provided question
	 */
    public void caseEOF(EOF node, Q question) {
		defaultToken(node, question);
	}

}
