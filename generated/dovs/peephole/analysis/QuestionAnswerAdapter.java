
package dovs.peephole.analysis;

import dovs.peephole.node.*;

/**
 * {@code QuestionAnswerAdapter} implements the default behaviour of the
 * {@link QuestionAnswer} interface. All calls return {@code null} by default.
 *
 * @param <Q> the type of the questions
 * @param <A> the type of the answers
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class QuestionAnswerAdapter<Q,A> implements QuestionAnswer<Q,A> {

	/**
	 * Returns the default answer for a {@link Node}.
	 * @param node the calling {@link Node}
	 * @param question the question
	 * @return the default answer for a {@link Node}
	 */
	public A defaultNode(Node node, Q question) {
		return null;
	}
	
	/**
	 * Returns the default answer for a {@link Token}. The call is deferred to
	 * {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param token the calling {@link Token}
	 * @param question the question
	 * @return the default answer for a {@link Token}
	 */
	public A defaultToken(Token token, Q question) {
		return defaultNode(token, question);
	}
	
	/**
	 * Returns the default answer for a {@link Start} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link Start} node
	 * @param question the question
	 * @return the default answer for a {@link Start} node
	 */
    public A caseStart(Start node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APatternCollection} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPPatternCollection(PPatternCollection,Object)}.
	 * @param node the calling {@link APatternCollection} node
	 * @param question the question
	 * @return the default answer for a {@link APatternCollection} node
	 */
	public A caseAPatternCollection(APatternCollection node, Q question) {
		return defaultPPatternCollection(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APatternDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPPatternDecl(PPatternDecl,Object)}.
	 * @param node the calling {@link APatternDecl} node
	 * @param question the question
	 * @return the default answer for a {@link APatternDecl} node
	 */
	public A caseAPatternDecl(APatternDecl node, Q question) {
		return defaultPPatternDecl(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AVar} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPVar(PVar,Object)}.
	 * @param node the calling {@link AVar} node
	 * @param question the question
	 * @return the default answer for a {@link AVar} node
	 */
	public A caseAVar(AVar node, Q question) {
		return defaultPVar(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AOpcode} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPOpcode(POpcode,Object)}.
	 * @param node the calling {@link AOpcode} node
	 * @param question the question
	 * @return the default answer for a {@link AOpcode} node
	 */
	public A caseAOpcode(AOpcode node, Q question) {
		return defaultPOpcode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AInst} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPInst(PInst,Object)}.
	 * @param node the calling {@link AInst} node
	 * @param question the question
	 * @return the default answer for a {@link AInst} node
	 */
	public A caseAInst(AInst node, Q question) {
		return defaultPInst(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AInstructionInstPat} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link AInstructionInstPat} node
	 * @param question the question
	 * @return the default answer for a {@link AInstructionInstPat} node
	 */
	public A caseAInstructionInstPat(AInstructionInstPat node, Q question) {
		return defaultPInstPat(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALabelBinderInstPat} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link ALabelBinderInstPat} node
	 * @param question the question
	 * @return the default answer for a {@link ALabelBinderInstPat} node
	 */
	public A caseALabelBinderInstPat(ALabelBinderInstPat node, Q question) {
		return defaultPInstPat(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AWildcardInstPat} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link AWildcardInstPat} node
	 * @param question the question
	 * @return the default answer for a {@link AWildcardInstPat} node
	 */
	public A caseAWildcardInstPat(AWildcardInstPat node, Q question) {
		return defaultPInstPat(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AVarExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AVarExp} node
	 * @param question the question
	 * @return the default answer for a {@link AVarExp} node
	 */
	public A caseAVarExp(AVarExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AMatchExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AMatchExp} node
	 * @param question the question
	 * @return the default answer for a {@link AMatchExp} node
	 */
	public A caseAMatchExp(AMatchExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABinopExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABinopExp} node
	 * @param question the question
	 * @return the default answer for a {@link ABinopExp} node
	 */
	public A caseABinopExp(ABinopExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AUnopExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AUnopExp} node
	 * @param question the question
	 * @return the default answer for a {@link AUnopExp} node
	 */
	public A caseAUnopExp(AUnopExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ABuiltinExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABuiltinExp} node
	 * @param question the question
	 * @return the default answer for a {@link ABuiltinExp} node
	 */
	public A caseABuiltinExp(ABuiltinExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AIntConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link AIntConstExp} node
	 */
	public A caseAIntConstExp(AIntConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACondConstExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACondConstExp} node
	 * @param question the question
	 * @return the default answer for a {@link ACondConstExp} node
	 */
	public A caseACondConstExp(ACondConstExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AConjunctionExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AConjunctionExp} node
	 * @param question the question
	 * @return the default answer for a {@link AConjunctionExp} node
	 */
	public A caseAConjunctionExp(AConjunctionExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ADisjunctionExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ADisjunctionExp} node
	 * @param question the question
	 * @return the default answer for a {@link ADisjunctionExp} node
	 */
	public A caseADisjunctionExp(ADisjunctionExp node, Q question) {
		return defaultPExp(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ACommuteBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ACommuteBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link ACommuteBuiltin} node
	 */
	public A caseACommuteBuiltin(ACommuteBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ADegreeBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ADegreeBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link ADegreeBuiltin} node
	 */
	public A caseADegreeBuiltin(ADegreeBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AFormalsBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link AFormalsBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link AFormalsBuiltin} node
	 */
	public A caseAFormalsBuiltin(AFormalsBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ANegateBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link ANegateBuiltin} node
	 */
	public A caseANegateBuiltin(ANegateBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AReturnsBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link AReturnsBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link AReturnsBuiltin} node
	 */
	public A caseAReturnsBuiltin(AReturnsBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ATargetBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ATargetBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link ATargetBuiltin} node
	 */
	public A caseATargetBuiltin(ATargetBuiltin node, Q question) {
		return defaultPBuiltin(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AEqCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AEqCond} node
	 * @param question the question
	 * @return the default answer for a {@link AEqCond} node
	 */
	public A caseAEqCond(AEqCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANeCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ANeCond} node
	 * @param question the question
	 * @return the default answer for a {@link ANeCond} node
	 */
	public A caseANeCond(ANeCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALtCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ALtCond} node
	 * @param question the question
	 * @return the default answer for a {@link ALtCond} node
	 */
	public A caseALtCond(ALtCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALeCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ALeCond} node
	 * @param question the question
	 * @return the default answer for a {@link ALeCond} node
	 */
	public A caseALeCond(ALeCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGtCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AGtCond} node
	 * @param question the question
	 * @return the default answer for a {@link AGtCond} node
	 */
	public A caseAGtCond(AGtCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGeCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AGeCond} node
	 * @param question the question
	 * @return the default answer for a {@link AGeCond} node
	 */
	public A caseAGeCond(AGeCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAeqCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AAeqCond} node
	 * @param question the question
	 * @return the default answer for a {@link AAeqCond} node
	 */
	public A caseAAeqCond(AAeqCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAneCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AAneCond} node
	 * @param question the question
	 * @return the default answer for a {@link AAneCond} node
	 */
	public A caseAAneCond(AAneCond node, Q question) {
		return defaultPCond(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link APlusBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link APlusBinop} node
	 * @param question the question
	 * @return the default answer for a {@link APlusBinop} node
	 */
	public A caseAPlusBinop(APlusBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AMinusBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AMinusBinop} node
	 */
	public A caseAMinusBinop(AMinusBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ATimesBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ATimesBinop} node
	 */
	public A caseATimesBinop(ATimesBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ADivideBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ADivideBinop} node
	 */
	public A caseADivideBinop(ADivideBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AModuloBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AModuloBinop} node
	 */
	public A caseAModuloBinop(AModuloBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AEqBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AEqBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AEqBinop} node
	 */
	public A caseAEqBinop(AEqBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ANeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ANeBinop} node
	 */
	public A caseANeBinop(ANeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALtBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALtBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALtBinop} node
	 */
	public A caseALtBinop(ALtBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ALeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link ALeBinop} node
	 */
	public A caseALeBinop(ALeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGtBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGtBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AGtBinop} node
	 */
	public A caseAGtBinop(AGtBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AGeBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGeBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AGeBinop} node
	 */
	public A caseAGeBinop(AGeBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AAndBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAndBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AAndBinop} node
	 */
	public A caseAAndBinop(AAndBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AOrBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AOrBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AOrBinop} node
	 */
	public A caseAOrBinop(AOrBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AXorBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AXorBinop} node
	 * @param question the question
	 * @return the default answer for a {@link AXorBinop} node
	 */
	public A caseAXorBinop(AXorBinop node, Q question) {
		return defaultPBinop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @param question the question
	 * @return the default answer for a {@link ANegateUnop} node
	 */
	public A caseANegateUnop(ANegateUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link AComplementUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @param question the question
	 * @return the default answer for a {@link AComplementUnop} node
	 */
	public A caseAComplementUnop(AComplementUnop node, Q question) {
		return defaultPUnop(node, question);
	}
	

	/**
	 * Returns the default answer for a {@link TWhiteSpace} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @param question the question
	 * @return the default answer for a {@link TWhiteSpace} node
	 */
	public A caseTWhiteSpace(TWhiteSpace node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTraditionalComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @param question the question
	 * @return the default answer for a {@link TTraditionalComment} node
	 */
	public A caseTTraditionalComment(TTraditionalComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDocumentationComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @param question the question
	 * @return the default answer for a {@link TDocumentationComment} node
	 */
	public A caseTDocumentationComment(TDocumentationComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TEndOfLineComment} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @param question the question
	 * @return the default answer for a {@link TEndOfLineComment} node
	 */
	public A caseTEndOfLineComment(TEndOfLineComment node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TCommute} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCommute} node
	 * @param question the question
	 * @return the default answer for a {@link TCommute} node
	 */
	public A caseTCommute(TCommute node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TDegree} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDegree} node
	 * @param question the question
	 * @return the default answer for a {@link TDegree} node
	 */
	public A caseTDegree(TDegree node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TFormals} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFormals} node
	 * @param question the question
	 * @return the default answer for a {@link TFormals} node
	 */
	public A caseTFormals(TFormals node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNegate} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNegate} node
	 * @param question the question
	 * @return the default answer for a {@link TNegate} node
	 */
	public A caseTNegate(TNegate node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TPattern} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPattern} node
	 * @param question the question
	 * @return the default answer for a {@link TPattern} node
	 */
	public A caseTPattern(TPattern node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TReturns} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TReturns} node
	 * @param question the question
	 * @return the default answer for a {@link TReturns} node
	 */
	public A caseTReturns(TReturns node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TTarget} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTarget} node
	 * @param question the question
	 * @return the default answer for a {@link TTarget} node
	 */
	public A caseTTarget(TTarget node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAeq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAeq} node
	 * @param question the question
	 * @return the default answer for a {@link TAeq} node
	 */
	public A caseTAeq(TAeq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TAne} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAne} node
	 * @param question the question
	 * @return the default answer for a {@link TAne} node
	 */
	public A caseTAne(TAne node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TEq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEq} node
	 * @param question the question
	 * @return the default answer for a {@link TEq} node
	 */
	public A caseTEq(TEq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TGe} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGe} node
	 * @param question the question
	 * @return the default answer for a {@link TGe} node
	 */
	public A caseTGe(TGe node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TGt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGt} node
	 * @param question the question
	 * @return the default answer for a {@link TGt} node
	 */
	public A caseTGt(TGt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLe} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLe} node
	 * @param question the question
	 * @return the default answer for a {@link TLe} node
	 */
	public A caseTLe(TLe node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLt} node
	 * @param question the question
	 * @return the default answer for a {@link TLt} node
	 */
	public A caseTLt(TLt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TNe} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNe} node
	 * @param question the question
	 * @return the default answer for a {@link TNe} node
	 */
	public A caseTNe(TNe node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TLParenthese} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLParenthese} node
	 * @param question the question
	 * @return the default answer for a {@link TLParenthese} node
	 */
	public A caseTLParenthese(TLParenthese node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TRParenthese} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRParenthese} node
	 * @param question the question
	 * @return the default answer for a {@link TRParenthese} node
	 */
	public A caseTRParenthese(TRParenthese node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TColon} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TColon} node
	 * @param question the question
	 * @return the default answer for a {@link TColon} node
	 */
	public A caseTColon(TColon node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TComma} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComma} node
	 * @param question the question
	 * @return the default answer for a {@link TComma} node
	 */
	public A caseTComma(TComma node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TArrow} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TArrow} node
	 * @param question the question
	 * @return the default answer for a {@link TArrow} node
	 */
	public A caseTArrow(TArrow node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpComplement} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpComplement} node
	 * @param question the question
	 * @return the default answer for a {@link TOpComplement} node
	 */
	public A caseTOpComplement(TOpComplement node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLogicalAnd} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLogicalAnd} node
	 * @param question the question
	 * @return the default answer for a {@link TOpLogicalAnd} node
	 */
	public A caseTOpLogicalAnd(TOpLogicalAnd node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLogicalOr} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLogicalOr} node
	 * @param question the question
	 * @return the default answer for a {@link TOpLogicalOr} node
	 */
	public A caseTOpLogicalOr(TOpLogicalOr node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLt} node
	 * @param question the question
	 * @return the default answer for a {@link TOpLt} node
	 */
	public A caseTOpLt(TOpLt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpGt} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpGt} node
	 * @param question the question
	 * @return the default answer for a {@link TOpGt} node
	 */
	public A caseTOpGt(TOpGt node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpEq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpEq} node
	 * @param question the question
	 * @return the default answer for a {@link TOpEq} node
	 */
	public A caseTOpEq(TOpEq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLteq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLteq} node
	 * @param question the question
	 * @return the default answer for a {@link TOpLteq} node
	 */
	public A caseTOpLteq(TOpLteq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpGteq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpGteq} node
	 * @param question the question
	 * @return the default answer for a {@link TOpGteq} node
	 */
	public A caseTOpGteq(TOpGteq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpNeq} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpNeq} node
	 * @param question the question
	 * @return the default answer for a {@link TOpNeq} node
	 */
	public A caseTOpNeq(TOpNeq node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMatch} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMatch} node
	 * @param question the question
	 * @return the default answer for a {@link TOpMatch} node
	 */
	public A caseTOpMatch(TOpMatch node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpPlus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpPlus} node
	 * @param question the question
	 * @return the default answer for a {@link TOpPlus} node
	 */
	public A caseTOpPlus(TOpPlus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMinus} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMinus} node
	 * @param question the question
	 * @return the default answer for a {@link TOpMinus} node
	 */
	public A caseTOpMinus(TOpMinus node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMul} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMul} node
	 * @param question the question
	 * @return the default answer for a {@link TOpMul} node
	 */
	public A caseTOpMul(TOpMul node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpDiv} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpDiv} node
	 * @param question the question
	 * @return the default answer for a {@link TOpDiv} node
	 */
	public A caseTOpDiv(TOpDiv node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpAnd} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpAnd} node
	 * @param question the question
	 * @return the default answer for a {@link TOpAnd} node
	 */
	public A caseTOpAnd(TOpAnd node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpOr} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpOr} node
	 * @param question the question
	 * @return the default answer for a {@link TOpOr} node
	 */
	public A caseTOpOr(TOpOr node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpXor} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpXor} node
	 * @param question the question
	 * @return the default answer for a {@link TOpXor} node
	 */
	public A caseTOpXor(TOpXor node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMod} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMod} node
	 * @param question the question
	 * @return the default answer for a {@link TOpMod} node
	 */
	public A caseTOpMod(TOpMod node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TIntegerLiteral} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @param question the question
	 * @return the default answer for a {@link TIntegerLiteral} node
	 */
	public A caseTIntegerLiteral(TIntegerLiteral node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link TIdentifier} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIdentifier} node
	 * @param question the question
	 * @return the default answer for a {@link TIdentifier} node
	 */
	public A caseTIdentifier(TIdentifier node, Q question) {
		return defaultToken(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PPatternCollection} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPatternCollection} node
	 * @param question the question
	 * @return the default answer for a {@link PPatternCollection} node
	 */
	public A defaultPPatternCollection(PPatternCollection node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PPatternDecl} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPatternDecl} node
	 * @param question the question
	 * @return the default answer for a {@link PPatternDecl} node
	 */
	public A defaultPPatternDecl(PPatternDecl node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PVar} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PVar} node
	 * @param question the question
	 * @return the default answer for a {@link PVar} node
	 */
	public A defaultPVar(PVar node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link POpcode} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link POpcode} node
	 * @param question the question
	 * @return the default answer for a {@link POpcode} node
	 */
	public A defaultPOpcode(POpcode node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PInst} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PInst} node
	 * @param question the question
	 * @return the default answer for a {@link PInst} node
	 */
	public A defaultPInst(PInst node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PInstPat} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PInstPat} node
	 * @param question the question
	 * @return the default answer for a {@link PInstPat} node
	 */
	public A defaultPInstPat(PInstPat node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PExp} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PExp} node
	 * @param question the question
	 * @return the default answer for a {@link PExp} node
	 */
	public A defaultPExp(PExp node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBuiltin} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBuiltin} node
	 * @param question the question
	 * @return the default answer for a {@link PBuiltin} node
	 */
	public A defaultPBuiltin(PBuiltin node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PCond} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PCond} node
	 * @param question the question
	 * @return the default answer for a {@link PCond} node
	 */
	public A defaultPCond(PCond node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PBinop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBinop} node
	 * @param question the question
	 * @return the default answer for a {@link PBinop} node
	 */
	public A defaultPBinop(PBinop node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link PUnop} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PUnop} node
	 * @param question the question
	 * @return the default answer for a {@link PUnop} node
	 */
	public A defaultPUnop(PUnop node, Q question) {
		return defaultNode(node, question);
	}
	
	/**
	 * Returns the default answer for a {@link EOF} node. The call is deferred
	 * to {@link QuestionAnswerAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link EOF} node
	 * @param question the question
	 * @return the default answer for a {@link EOF} node
	 */
    public A caseEOF(EOF node, Q question) {
		return defaultToken(node, question);
	}

}
