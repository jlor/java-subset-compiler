/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.peephole.analysis;

import dovs.peephole.node.*;

/**
 * {@code Question} defines an interface for the abstraction of questions
 * based on a {@link Node}. The interface is a visitor pattern.
 *
 * @param <Q> the type of the questions
 *
 * @author Johnni Winther, jw@brics.dk
 */
public interface Question<Q> {

	/**
	 * Called by {@link Start} from {@link Start#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
    public void caseStart(Start node, Q question);

	/**
	 * Called by {@link APatternCollection} from {@link APatternCollection#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAPatternCollection(APatternCollection node, Q question);
	
	/**
	 * Called by {@link APatternDecl} from {@link APatternDecl#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAPatternDecl(APatternDecl node, Q question);
	
	/**
	 * Called by {@link AVar} from {@link AVar#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAVar(AVar node, Q question);
	
	/**
	 * Called by {@link AOpcode} from {@link AOpcode#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAOpcode(AOpcode node, Q question);
	
	/**
	 * Called by {@link AInst} from {@link AInst#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAInst(AInst node, Q question);
	
	/**
	 * Called by {@link AInstructionInstPat} from {@link AInstructionInstPat#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAInstructionInstPat(AInstructionInstPat node, Q question);
	
	/**
	 * Called by {@link ALabelBinderInstPat} from {@link ALabelBinderInstPat#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseALabelBinderInstPat(ALabelBinderInstPat node, Q question);
	
	/**
	 * Called by {@link AWildcardInstPat} from {@link AWildcardInstPat#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAWildcardInstPat(AWildcardInstPat node, Q question);
	
	/**
	 * Called by {@link AVarExp} from {@link AVarExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAVarExp(AVarExp node, Q question);
	
	/**
	 * Called by {@link AMatchExp} from {@link AMatchExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAMatchExp(AMatchExp node, Q question);
	
	/**
	 * Called by {@link ABinopExp} from {@link ABinopExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseABinopExp(ABinopExp node, Q question);
	
	/**
	 * Called by {@link AUnopExp} from {@link AUnopExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAUnopExp(AUnopExp node, Q question);
	
	/**
	 * Called by {@link ABuiltinExp} from {@link ABuiltinExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseABuiltinExp(ABuiltinExp node, Q question);
	
	/**
	 * Called by {@link AIntConstExp} from {@link AIntConstExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAIntConstExp(AIntConstExp node, Q question);
	
	/**
	 * Called by {@link ACondConstExp} from {@link ACondConstExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseACondConstExp(ACondConstExp node, Q question);
	
	/**
	 * Called by {@link AConjunctionExp} from {@link AConjunctionExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAConjunctionExp(AConjunctionExp node, Q question);
	
	/**
	 * Called by {@link ADisjunctionExp} from {@link ADisjunctionExp#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseADisjunctionExp(ADisjunctionExp node, Q question);
	
	/**
	 * Called by {@link ACommuteBuiltin} from {@link ACommuteBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseACommuteBuiltin(ACommuteBuiltin node, Q question);
	
	/**
	 * Called by {@link ADegreeBuiltin} from {@link ADegreeBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseADegreeBuiltin(ADegreeBuiltin node, Q question);
	
	/**
	 * Called by {@link AFormalsBuiltin} from {@link AFormalsBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAFormalsBuiltin(AFormalsBuiltin node, Q question);
	
	/**
	 * Called by {@link ANegateBuiltin} from {@link ANegateBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseANegateBuiltin(ANegateBuiltin node, Q question);
	
	/**
	 * Called by {@link AReturnsBuiltin} from {@link AReturnsBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAReturnsBuiltin(AReturnsBuiltin node, Q question);
	
	/**
	 * Called by {@link ATargetBuiltin} from {@link ATargetBuiltin#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseATargetBuiltin(ATargetBuiltin node, Q question);
	
	/**
	 * Called by {@link AEqCond} from {@link AEqCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAEqCond(AEqCond node, Q question);
	
	/**
	 * Called by {@link ANeCond} from {@link ANeCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseANeCond(ANeCond node, Q question);
	
	/**
	 * Called by {@link ALtCond} from {@link ALtCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseALtCond(ALtCond node, Q question);
	
	/**
	 * Called by {@link ALeCond} from {@link ALeCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseALeCond(ALeCond node, Q question);
	
	/**
	 * Called by {@link AGtCond} from {@link AGtCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAGtCond(AGtCond node, Q question);
	
	/**
	 * Called by {@link AGeCond} from {@link AGeCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAGeCond(AGeCond node, Q question);
	
	/**
	 * Called by {@link AAeqCond} from {@link AAeqCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAAeqCond(AAeqCond node, Q question);
	
	/**
	 * Called by {@link AAneCond} from {@link AAneCond#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAAneCond(AAneCond node, Q question);
	
	/**
	 * Called by {@link APlusBinop} from {@link APlusBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAPlusBinop(APlusBinop node, Q question);
	
	/**
	 * Called by {@link AMinusBinop} from {@link AMinusBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAMinusBinop(AMinusBinop node, Q question);
	
	/**
	 * Called by {@link ATimesBinop} from {@link ATimesBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseATimesBinop(ATimesBinop node, Q question);
	
	/**
	 * Called by {@link ADivideBinop} from {@link ADivideBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseADivideBinop(ADivideBinop node, Q question);
	
	/**
	 * Called by {@link AModuloBinop} from {@link AModuloBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAModuloBinop(AModuloBinop node, Q question);
	
	/**
	 * Called by {@link AEqBinop} from {@link AEqBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAEqBinop(AEqBinop node, Q question);
	
	/**
	 * Called by {@link ANeBinop} from {@link ANeBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseANeBinop(ANeBinop node, Q question);
	
	/**
	 * Called by {@link ALtBinop} from {@link ALtBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseALtBinop(ALtBinop node, Q question);
	
	/**
	 * Called by {@link ALeBinop} from {@link ALeBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseALeBinop(ALeBinop node, Q question);
	
	/**
	 * Called by {@link AGtBinop} from {@link AGtBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAGtBinop(AGtBinop node, Q question);
	
	/**
	 * Called by {@link AGeBinop} from {@link AGeBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAGeBinop(AGeBinop node, Q question);
	
	/**
	 * Called by {@link AAndBinop} from {@link AAndBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAAndBinop(AAndBinop node, Q question);
	
	/**
	 * Called by {@link AOrBinop} from {@link AOrBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAOrBinop(AOrBinop node, Q question);
	
	/**
	 * Called by {@link AXorBinop} from {@link AXorBinop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAXorBinop(AXorBinop node, Q question);
	
	/**
	 * Called by {@link ANegateUnop} from {@link ANegateUnop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseANegateUnop(ANegateUnop node, Q question);
	
	/**
	 * Called by {@link AComplementUnop} from {@link AComplementUnop#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseAComplementUnop(AComplementUnop node, Q question);
	

	/**
	 * Called by {@link TWhiteSpace} from {@link TWhiteSpace#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTWhiteSpace(TWhiteSpace node, Q question);
	
	/**
	 * Called by {@link TTraditionalComment} from {@link TTraditionalComment#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTTraditionalComment(TTraditionalComment node, Q question);
	
	/**
	 * Called by {@link TDocumentationComment} from {@link TDocumentationComment#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTDocumentationComment(TDocumentationComment node, Q question);
	
	/**
	 * Called by {@link TEndOfLineComment} from {@link TEndOfLineComment#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTEndOfLineComment(TEndOfLineComment node, Q question);
	
	/**
	 * Called by {@link TCommute} from {@link TCommute#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTCommute(TCommute node, Q question);
	
	/**
	 * Called by {@link TDegree} from {@link TDegree#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTDegree(TDegree node, Q question);
	
	/**
	 * Called by {@link TFormals} from {@link TFormals#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTFormals(TFormals node, Q question);
	
	/**
	 * Called by {@link TNegate} from {@link TNegate#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTNegate(TNegate node, Q question);
	
	/**
	 * Called by {@link TPattern} from {@link TPattern#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTPattern(TPattern node, Q question);
	
	/**
	 * Called by {@link TReturns} from {@link TReturns#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTReturns(TReturns node, Q question);
	
	/**
	 * Called by {@link TTarget} from {@link TTarget#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTTarget(TTarget node, Q question);
	
	/**
	 * Called by {@link TAeq} from {@link TAeq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTAeq(TAeq node, Q question);
	
	/**
	 * Called by {@link TAne} from {@link TAne#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTAne(TAne node, Q question);
	
	/**
	 * Called by {@link TEq} from {@link TEq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTEq(TEq node, Q question);
	
	/**
	 * Called by {@link TGe} from {@link TGe#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTGe(TGe node, Q question);
	
	/**
	 * Called by {@link TGt} from {@link TGt#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTGt(TGt node, Q question);
	
	/**
	 * Called by {@link TLe} from {@link TLe#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTLe(TLe node, Q question);
	
	/**
	 * Called by {@link TLt} from {@link TLt#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTLt(TLt node, Q question);
	
	/**
	 * Called by {@link TNe} from {@link TNe#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTNe(TNe node, Q question);
	
	/**
	 * Called by {@link TLParenthese} from {@link TLParenthese#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTLParenthese(TLParenthese node, Q question);
	
	/**
	 * Called by {@link TRParenthese} from {@link TRParenthese#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTRParenthese(TRParenthese node, Q question);
	
	/**
	 * Called by {@link TColon} from {@link TColon#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTColon(TColon node, Q question);
	
	/**
	 * Called by {@link TComma} from {@link TComma#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTComma(TComma node, Q question);
	
	/**
	 * Called by {@link TArrow} from {@link TArrow#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTArrow(TArrow node, Q question);
	
	/**
	 * Called by {@link TOpComplement} from {@link TOpComplement#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpComplement(TOpComplement node, Q question);
	
	/**
	 * Called by {@link TOpLogicalAnd} from {@link TOpLogicalAnd#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpLogicalAnd(TOpLogicalAnd node, Q question);
	
	/**
	 * Called by {@link TOpLogicalOr} from {@link TOpLogicalOr#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpLogicalOr(TOpLogicalOr node, Q question);
	
	/**
	 * Called by {@link TOpLt} from {@link TOpLt#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpLt(TOpLt node, Q question);
	
	/**
	 * Called by {@link TOpGt} from {@link TOpGt#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpGt(TOpGt node, Q question);
	
	/**
	 * Called by {@link TOpEq} from {@link TOpEq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpEq(TOpEq node, Q question);
	
	/**
	 * Called by {@link TOpLteq} from {@link TOpLteq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpLteq(TOpLteq node, Q question);
	
	/**
	 * Called by {@link TOpGteq} from {@link TOpGteq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpGteq(TOpGteq node, Q question);
	
	/**
	 * Called by {@link TOpNeq} from {@link TOpNeq#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpNeq(TOpNeq node, Q question);
	
	/**
	 * Called by {@link TOpMatch} from {@link TOpMatch#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpMatch(TOpMatch node, Q question);
	
	/**
	 * Called by {@link TOpPlus} from {@link TOpPlus#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpPlus(TOpPlus node, Q question);
	
	/**
	 * Called by {@link TOpMinus} from {@link TOpMinus#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpMinus(TOpMinus node, Q question);
	
	/**
	 * Called by {@link TOpMul} from {@link TOpMul#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpMul(TOpMul node, Q question);
	
	/**
	 * Called by {@link TOpDiv} from {@link TOpDiv#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpDiv(TOpDiv node, Q question);
	
	/**
	 * Called by {@link TOpAnd} from {@link TOpAnd#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpAnd(TOpAnd node, Q question);
	
	/**
	 * Called by {@link TOpOr} from {@link TOpOr#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpOr(TOpOr node, Q question);
	
	/**
	 * Called by {@link TOpXor} from {@link TOpXor#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpXor(TOpXor node, Q question);
	
	/**
	 * Called by {@link TOpMod} from {@link TOpMod#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTOpMod(TOpMod node, Q question);
	
	/**
	 * Called by {@link TIntegerLiteral} from {@link TIntegerLiteral#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTIntegerLiteral(TIntegerLiteral node, Q question);
	
	/**
	 * Called by {@link TIdentifier} from {@link TIdentifier#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
	public void caseTIdentifier(TIdentifier node, Q question);
		/**
	 * Called by {@link EOF} from {@link EOF#apply(Question,Object)}.
	 * @param node the calling node
	 * @param question the provided question
	 */
    public void caseEOF(EOF node, Q question);

}
