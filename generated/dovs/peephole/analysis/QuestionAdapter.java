
package dovs.peephole.analysis;

import dovs.peephole.node.*;

/**
 * {@code QuestionAdapter} implements the default behaviour of the {@link Question}
 * interface.
 *
 * @param <Q> the type of the questions
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class QuestionAdapter<Q> implements Question<Q> {

	/**
	 * Default action for a question to a {@link Node}.
	 * @param node the calling {@link Node}
	 * @param question the provided question
	 */
	public void defaultNode(Node node, Q question) {
		// empty body
	}
	
	/**
	 * Default action for a question to a {@link Token}. 
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param token the calling {@link Token}
	 * @param question the provided question
	 */
	public void defaultToken(Token token, Q question) {
		defaultNode(token, question);
	}
	
	/**
	 * Action for applying a question to a {@link Start} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link Start} node
	 * @param question the provided question
	 */
    public void caseStart(Start node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APatternCollection} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPPatternCollection(PPatternCollection,Object)}.
	 * @param node the calling {@link APatternCollection} node
	 * @param question the provided question
	 */
	public void caseAPatternCollection(APatternCollection node, Q question) {
		defaultPPatternCollection(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APatternDecl} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPPatternDecl(PPatternDecl,Object)}.
	 * @param node the calling {@link APatternDecl} node
	 * @param question the provided question
	 */
	public void caseAPatternDecl(APatternDecl node, Q question) {
		defaultPPatternDecl(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AVar} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPVar(PVar,Object)}.
	 * @param node the calling {@link AVar} node
	 * @param question the provided question
	 */
	public void caseAVar(AVar node, Q question) {
		defaultPVar(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AOpcode} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPOpcode(POpcode,Object)}.
	 * @param node the calling {@link AOpcode} node
	 * @param question the provided question
	 */
	public void caseAOpcode(AOpcode node, Q question) {
		defaultPOpcode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AInst} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPInst(PInst,Object)}.
	 * @param node the calling {@link AInst} node
	 * @param question the provided question
	 */
	public void caseAInst(AInst node, Q question) {
		defaultPInst(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AInstructionInstPat} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link AInstructionInstPat} node
	 * @param question the provided question
	 */
	public void caseAInstructionInstPat(AInstructionInstPat node, Q question) {
		defaultPInstPat(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALabelBinderInstPat} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link ALabelBinderInstPat} node
	 * @param question the provided question
	 */
	public void caseALabelBinderInstPat(ALabelBinderInstPat node, Q question) {
		defaultPInstPat(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AWildcardInstPat} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPInstPat(PInstPat,Object)}.
	 * @param node the calling {@link AWildcardInstPat} node
	 * @param question the provided question
	 */
	public void caseAWildcardInstPat(AWildcardInstPat node, Q question) {
		defaultPInstPat(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AVarExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AVarExp} node
	 * @param question the provided question
	 */
	public void caseAVarExp(AVarExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AMatchExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AMatchExp} node
	 * @param question the provided question
	 */
	public void caseAMatchExp(AMatchExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABinopExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABinopExp} node
	 * @param question the provided question
	 */
	public void caseABinopExp(ABinopExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AUnopExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AUnopExp} node
	 * @param question the provided question
	 */
	public void caseAUnopExp(AUnopExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ABuiltinExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ABuiltinExp} node
	 * @param question the provided question
	 */
	public void caseABuiltinExp(ABuiltinExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AIntConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @param question the provided question
	 */
	public void caseAIntConstExp(AIntConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACondConstExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ACondConstExp} node
	 * @param question the provided question
	 */
	public void caseACondConstExp(ACondConstExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AConjunctionExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link AConjunctionExp} node
	 * @param question the provided question
	 */
	public void caseAConjunctionExp(AConjunctionExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ADisjunctionExp} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPExp(PExp,Object)}.
	 * @param node the calling {@link ADisjunctionExp} node
	 * @param question the provided question
	 */
	public void caseADisjunctionExp(ADisjunctionExp node, Q question) {
		defaultPExp(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ACommuteBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ACommuteBuiltin} node
	 * @param question the provided question
	 */
	public void caseACommuteBuiltin(ACommuteBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ADegreeBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ADegreeBuiltin} node
	 * @param question the provided question
	 */
	public void caseADegreeBuiltin(ADegreeBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AFormalsBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link AFormalsBuiltin} node
	 * @param question the provided question
	 */
	public void caseAFormalsBuiltin(AFormalsBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANegateBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ANegateBuiltin} node
	 * @param question the provided question
	 */
	public void caseANegateBuiltin(ANegateBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AReturnsBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link AReturnsBuiltin} node
	 * @param question the provided question
	 */
	public void caseAReturnsBuiltin(AReturnsBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ATargetBuiltin} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBuiltin(PBuiltin,Object)}.
	 * @param node the calling {@link ATargetBuiltin} node
	 * @param question the provided question
	 */
	public void caseATargetBuiltin(ATargetBuiltin node, Q question) {
		defaultPBuiltin(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AEqCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AEqCond} node
	 * @param question the provided question
	 */
	public void caseAEqCond(AEqCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANeCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ANeCond} node
	 * @param question the provided question
	 */
	public void caseANeCond(ANeCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALtCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ALtCond} node
	 * @param question the provided question
	 */
	public void caseALtCond(ALtCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALeCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link ALeCond} node
	 * @param question the provided question
	 */
	public void caseALeCond(ALeCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGtCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AGtCond} node
	 * @param question the provided question
	 */
	public void caseAGtCond(AGtCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGeCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AGeCond} node
	 * @param question the provided question
	 */
	public void caseAGeCond(AGeCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAeqCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AAeqCond} node
	 * @param question the provided question
	 */
	public void caseAAeqCond(AAeqCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAneCond} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPCond(PCond,Object)}.
	 * @param node the calling {@link AAneCond} node
	 * @param question the provided question
	 */
	public void caseAAneCond(AAneCond node, Q question) {
		defaultPCond(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link APlusBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link APlusBinop} node
	 * @param question the provided question
	 */
	public void caseAPlusBinop(APlusBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AMinusBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @param question the provided question
	 */
	public void caseAMinusBinop(AMinusBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ATimesBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @param question the provided question
	 */
	public void caseATimesBinop(ATimesBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ADivideBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @param question the provided question
	 */
	public void caseADivideBinop(ADivideBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AModuloBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @param question the provided question
	 */
	public void caseAModuloBinop(AModuloBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AEqBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AEqBinop} node
	 * @param question the provided question
	 */
	public void caseAEqBinop(AEqBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ANeBinop} node
	 * @param question the provided question
	 */
	public void caseANeBinop(ANeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALtBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALtBinop} node
	 * @param question the provided question
	 */
	public void caseALtBinop(ALtBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ALeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link ALeBinop} node
	 * @param question the provided question
	 */
	public void caseALeBinop(ALeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGtBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGtBinop} node
	 * @param question the provided question
	 */
	public void caseAGtBinop(AGtBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AGeBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AGeBinop} node
	 * @param question the provided question
	 */
	public void caseAGeBinop(AGeBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AAndBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AAndBinop} node
	 * @param question the provided question
	 */
	public void caseAAndBinop(AAndBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AOrBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AOrBinop} node
	 * @param question the provided question
	 */
	public void caseAOrBinop(AOrBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AXorBinop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPBinop(PBinop,Object)}.
	 * @param node the calling {@link AXorBinop} node
	 * @param question the provided question
	 */
	public void caseAXorBinop(AXorBinop node, Q question) {
		defaultPBinop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link ANegateUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @param question the provided question
	 */
	public void caseANegateUnop(ANegateUnop node, Q question) {
		defaultPUnop(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link AComplementUnop} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultPUnop(PUnop,Object)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @param question the provided question
	 */
	public void caseAComplementUnop(AComplementUnop node, Q question) {
		defaultPUnop(node, question);
	}
	

	/**
	 * Action for applying a question to a {@link TWhiteSpace} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @param question the provided question
	 */
	public void caseTWhiteSpace(TWhiteSpace node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTraditionalComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @param question the provided question
	 */
	public void caseTTraditionalComment(TTraditionalComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDocumentationComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @param question the provided question
	 */
	public void caseTDocumentationComment(TDocumentationComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TEndOfLineComment} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @param question the provided question
	 */
	public void caseTEndOfLineComment(TEndOfLineComment node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TCommute} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TCommute} node
	 * @param question the provided question
	 */
	public void caseTCommute(TCommute node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TDegree} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TDegree} node
	 * @param question the provided question
	 */
	public void caseTDegree(TDegree node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TFormals} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TFormals} node
	 * @param question the provided question
	 */
	public void caseTFormals(TFormals node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNegate} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNegate} node
	 * @param question the provided question
	 */
	public void caseTNegate(TNegate node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TPattern} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TPattern} node
	 * @param question the provided question
	 */
	public void caseTPattern(TPattern node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TReturns} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TReturns} node
	 * @param question the provided question
	 */
	public void caseTReturns(TReturns node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TTarget} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TTarget} node
	 * @param question the provided question
	 */
	public void caseTTarget(TTarget node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAeq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAeq} node
	 * @param question the provided question
	 */
	public void caseTAeq(TAeq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TAne} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TAne} node
	 * @param question the provided question
	 */
	public void caseTAne(TAne node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TEq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TEq} node
	 * @param question the provided question
	 */
	public void caseTEq(TEq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TGe} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGe} node
	 * @param question the provided question
	 */
	public void caseTGe(TGe node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TGt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TGt} node
	 * @param question the provided question
	 */
	public void caseTGt(TGt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLe} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLe} node
	 * @param question the provided question
	 */
	public void caseTLe(TLe node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLt} node
	 * @param question the provided question
	 */
	public void caseTLt(TLt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TNe} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TNe} node
	 * @param question the provided question
	 */
	public void caseTNe(TNe node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TLParenthese} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TLParenthese} node
	 * @param question the provided question
	 */
	public void caseTLParenthese(TLParenthese node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TRParenthese} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TRParenthese} node
	 * @param question the provided question
	 */
	public void caseTRParenthese(TRParenthese node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TColon} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TColon} node
	 * @param question the provided question
	 */
	public void caseTColon(TColon node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TComma} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TComma} node
	 * @param question the provided question
	 */
	public void caseTComma(TComma node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TArrow} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TArrow} node
	 * @param question the provided question
	 */
	public void caseTArrow(TArrow node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpComplement} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpComplement} node
	 * @param question the provided question
	 */
	public void caseTOpComplement(TOpComplement node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpLogicalAnd} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLogicalAnd} node
	 * @param question the provided question
	 */
	public void caseTOpLogicalAnd(TOpLogicalAnd node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpLogicalOr} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLogicalOr} node
	 * @param question the provided question
	 */
	public void caseTOpLogicalOr(TOpLogicalOr node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpLt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLt} node
	 * @param question the provided question
	 */
	public void caseTOpLt(TOpLt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpGt} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpGt} node
	 * @param question the provided question
	 */
	public void caseTOpGt(TOpGt node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpEq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpEq} node
	 * @param question the provided question
	 */
	public void caseTOpEq(TOpEq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpLteq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpLteq} node
	 * @param question the provided question
	 */
	public void caseTOpLteq(TOpLteq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpGteq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpGteq} node
	 * @param question the provided question
	 */
	public void caseTOpGteq(TOpGteq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpNeq} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpNeq} node
	 * @param question the provided question
	 */
	public void caseTOpNeq(TOpNeq node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpMatch} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMatch} node
	 * @param question the provided question
	 */
	public void caseTOpMatch(TOpMatch node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpPlus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpPlus} node
	 * @param question the provided question
	 */
	public void caseTOpPlus(TOpPlus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpMinus} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMinus} node
	 * @param question the provided question
	 */
	public void caseTOpMinus(TOpMinus node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpMul} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMul} node
	 * @param question the provided question
	 */
	public void caseTOpMul(TOpMul node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpDiv} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpDiv} node
	 * @param question the provided question
	 */
	public void caseTOpDiv(TOpDiv node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpAnd} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpAnd} node
	 * @param question the provided question
	 */
	public void caseTOpAnd(TOpAnd node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpOr} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpOr} node
	 * @param question the provided question
	 */
	public void caseTOpOr(TOpOr node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpXor} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpXor} node
	 * @param question the provided question
	 */
	public void caseTOpXor(TOpXor node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TOpMod} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TOpMod} node
	 * @param question the provided question
	 */
	public void caseTOpMod(TOpMod node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TIntegerLiteral} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @param question the provided question
	 */
	public void caseTIntegerLiteral(TIntegerLiteral node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link TIdentifier} node. 
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link TIdentifier} node
	 * @param question the provided question
	 */
	public void caseTIdentifier(TIdentifier node, Q question) {
		defaultToken(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PPatternCollection} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPatternCollection} node
	 * @param question the provided question
	 */
	public void defaultPPatternCollection(PPatternCollection node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PPatternDecl} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PPatternDecl} node
	 * @param question the provided question
	 */
	public void defaultPPatternDecl(PPatternDecl node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PVar} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PVar} node
	 * @param question the provided question
	 */
	public void defaultPVar(PVar node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link POpcode} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link POpcode} node
	 * @param question the provided question
	 */
	public void defaultPOpcode(POpcode node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PInst} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PInst} node
	 * @param question the provided question
	 */
	public void defaultPInst(PInst node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PInstPat} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PInstPat} node
	 * @param question the provided question
	 */
	public void defaultPInstPat(PInstPat node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PExp} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PExp} node
	 * @param question the provided question
	 */
	public void defaultPExp(PExp node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBuiltin} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBuiltin} node
	 * @param question the provided question
	 */
	public void defaultPBuiltin(PBuiltin node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PCond} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PCond} node
	 * @param question the provided question
	 */
	public void defaultPCond(PCond node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PBinop} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PBinop} node
	 * @param question the provided question
	 */
	public void defaultPBinop(PBinop node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link PUnop} node.
	 * The call is deferred to {@link QuestionAdapter#defaultNode(Node,Object)}.
	 * @param node the calling {@link PUnop} node
	 * @param question the provided question
	 */
	public void defaultPUnop(PUnop node, Q question) {
		defaultNode(node, question);
	}
	
	/**
	 * Action for applying a question to a {@link EOF} node.
	 * The call is deferred to {@link QuestionAdapter#defaultToken(Token,Object)}.
	 * @param node the calling {@link EOF} node
	 * @param question the provided question
	 */
    public void caseEOF(EOF node, Q question) {
		defaultToken(node, question);
	}

}
