
package dovs.peephole.analysis;

import dovs.peephole.node.*;

/**
 * {@code AnswerAdapter} implements the default behaviour of the {@link Answer}
 * interface. All calls return {@code null} by default.
 *
 * @param <A> the type of the answers
 *
 * @author Johnni Winther, jw@brics.dk
 */
public class AnswerAdapter<A> implements Answer<A> {

	/**
	 * Returns the default answer for a {@link Node}.
	 * @param node the calling {@link Node}
	 * @return the default answer for a {@link Node}
	 */
	public A defaultNode(Node node) {
		return null;
	}
	
	/**
	 * Returns the default answer for a {@link Token}. The call is deferred to
	 * {@link AnswerAdapter#defaultNode(Node)}.
	 * @param token the calling {@link Token}
	 * @return the default answer for a {@link Token}
	 */
	public A defaultToken(Token token) {
		return defaultNode(token);
	}
	
	/**
	 * Returns the default answer for a {@link Start} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link Start} node
	 * @return the default answer for a {@link Start} node
	 */
    public A caseStart(Start node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link APatternCollection} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPPatternCollection(PPatternCollection)}.
	 * @param node the calling {@link APatternCollection} node
	 * @return the default answer for a {@link APatternCollection} node
	 */
	public A caseAPatternCollection(APatternCollection node) {
		return defaultPPatternCollection(node);
	}
	
	/**
	 * Returns the default answer for a {@link APatternDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPPatternDecl(PPatternDecl)}.
	 * @param node the calling {@link APatternDecl} node
	 * @return the default answer for a {@link APatternDecl} node
	 */
	public A caseAPatternDecl(APatternDecl node) {
		return defaultPPatternDecl(node);
	}
	
	/**
	 * Returns the default answer for a {@link AVar} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPVar(PVar)}.
	 * @param node the calling {@link AVar} node
	 * @return the default answer for a {@link AVar} node
	 */
	public A caseAVar(AVar node) {
		return defaultPVar(node);
	}
	
	/**
	 * Returns the default answer for a {@link AOpcode} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPOpcode(POpcode)}.
	 * @param node the calling {@link AOpcode} node
	 * @return the default answer for a {@link AOpcode} node
	 */
	public A caseAOpcode(AOpcode node) {
		return defaultPOpcode(node);
	}
	
	/**
	 * Returns the default answer for a {@link AInst} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPInst(PInst)}.
	 * @param node the calling {@link AInst} node
	 * @return the default answer for a {@link AInst} node
	 */
	public A caseAInst(AInst node) {
		return defaultPInst(node);
	}
	
	/**
	 * Returns the default answer for a {@link AInstructionInstPat} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPInstPat(PInstPat)}.
	 * @param node the calling {@link AInstructionInstPat} node
	 * @return the default answer for a {@link AInstructionInstPat} node
	 */
	public A caseAInstructionInstPat(AInstructionInstPat node) {
		return defaultPInstPat(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALabelBinderInstPat} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPInstPat(PInstPat)}.
	 * @param node the calling {@link ALabelBinderInstPat} node
	 * @return the default answer for a {@link ALabelBinderInstPat} node
	 */
	public A caseALabelBinderInstPat(ALabelBinderInstPat node) {
		return defaultPInstPat(node);
	}
	
	/**
	 * Returns the default answer for a {@link AWildcardInstPat} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPInstPat(PInstPat)}.
	 * @param node the calling {@link AWildcardInstPat} node
	 * @return the default answer for a {@link AWildcardInstPat} node
	 */
	public A caseAWildcardInstPat(AWildcardInstPat node) {
		return defaultPInstPat(node);
	}
	
	/**
	 * Returns the default answer for a {@link AVarExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AVarExp} node
	 * @return the default answer for a {@link AVarExp} node
	 */
	public A caseAVarExp(AVarExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AMatchExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AMatchExp} node
	 * @return the default answer for a {@link AMatchExp} node
	 */
	public A caseAMatchExp(AMatchExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABinopExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ABinopExp} node
	 * @return the default answer for a {@link ABinopExp} node
	 */
	public A caseABinopExp(ABinopExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AUnopExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AUnopExp} node
	 * @return the default answer for a {@link AUnopExp} node
	 */
	public A caseAUnopExp(AUnopExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ABuiltinExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ABuiltinExp} node
	 * @return the default answer for a {@link ABuiltinExp} node
	 */
	public A caseABuiltinExp(ABuiltinExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AIntConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AIntConstExp} node
	 * @return the default answer for a {@link AIntConstExp} node
	 */
	public A caseAIntConstExp(AIntConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACondConstExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ACondConstExp} node
	 * @return the default answer for a {@link ACondConstExp} node
	 */
	public A caseACondConstExp(ACondConstExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link AConjunctionExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link AConjunctionExp} node
	 * @return the default answer for a {@link AConjunctionExp} node
	 */
	public A caseAConjunctionExp(AConjunctionExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ADisjunctionExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPExp(PExp)}.
	 * @param node the calling {@link ADisjunctionExp} node
	 * @return the default answer for a {@link ADisjunctionExp} node
	 */
	public A caseADisjunctionExp(ADisjunctionExp node) {
		return defaultPExp(node);
	}
	
	/**
	 * Returns the default answer for a {@link ACommuteBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link ACommuteBuiltin} node
	 * @return the default answer for a {@link ACommuteBuiltin} node
	 */
	public A caseACommuteBuiltin(ACommuteBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link ADegreeBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link ADegreeBuiltin} node
	 * @return the default answer for a {@link ADegreeBuiltin} node
	 */
	public A caseADegreeBuiltin(ADegreeBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link AFormalsBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link AFormalsBuiltin} node
	 * @return the default answer for a {@link AFormalsBuiltin} node
	 */
	public A caseAFormalsBuiltin(AFormalsBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link ANegateBuiltin} node
	 * @return the default answer for a {@link ANegateBuiltin} node
	 */
	public A caseANegateBuiltin(ANegateBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link AReturnsBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link AReturnsBuiltin} node
	 * @return the default answer for a {@link AReturnsBuiltin} node
	 */
	public A caseAReturnsBuiltin(AReturnsBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link ATargetBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBuiltin(PBuiltin)}.
	 * @param node the calling {@link ATargetBuiltin} node
	 * @return the default answer for a {@link ATargetBuiltin} node
	 */
	public A caseATargetBuiltin(ATargetBuiltin node) {
		return defaultPBuiltin(node);
	}
	
	/**
	 * Returns the default answer for a {@link AEqCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link AEqCond} node
	 * @return the default answer for a {@link AEqCond} node
	 */
	public A caseAEqCond(AEqCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANeCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link ANeCond} node
	 * @return the default answer for a {@link ANeCond} node
	 */
	public A caseANeCond(ANeCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALtCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link ALtCond} node
	 * @return the default answer for a {@link ALtCond} node
	 */
	public A caseALtCond(ALtCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALeCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link ALeCond} node
	 * @return the default answer for a {@link ALeCond} node
	 */
	public A caseALeCond(ALeCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGtCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link AGtCond} node
	 * @return the default answer for a {@link AGtCond} node
	 */
	public A caseAGtCond(AGtCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGeCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link AGeCond} node
	 * @return the default answer for a {@link AGeCond} node
	 */
	public A caseAGeCond(AGeCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAeqCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link AAeqCond} node
	 * @return the default answer for a {@link AAeqCond} node
	 */
	public A caseAAeqCond(AAeqCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAneCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPCond(PCond)}.
	 * @param node the calling {@link AAneCond} node
	 * @return the default answer for a {@link AAneCond} node
	 */
	public A caseAAneCond(AAneCond node) {
		return defaultPCond(node);
	}
	
	/**
	 * Returns the default answer for a {@link APlusBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link APlusBinop} node
	 * @return the default answer for a {@link APlusBinop} node
	 */
	public A caseAPlusBinop(APlusBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AMinusBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AMinusBinop} node
	 * @return the default answer for a {@link AMinusBinop} node
	 */
	public A caseAMinusBinop(AMinusBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ATimesBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ATimesBinop} node
	 * @return the default answer for a {@link ATimesBinop} node
	 */
	public A caseATimesBinop(ATimesBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ADivideBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ADivideBinop} node
	 * @return the default answer for a {@link ADivideBinop} node
	 */
	public A caseADivideBinop(ADivideBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AModuloBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AModuloBinop} node
	 * @return the default answer for a {@link AModuloBinop} node
	 */
	public A caseAModuloBinop(AModuloBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AEqBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AEqBinop} node
	 * @return the default answer for a {@link AEqBinop} node
	 */
	public A caseAEqBinop(AEqBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ANeBinop} node
	 * @return the default answer for a {@link ANeBinop} node
	 */
	public A caseANeBinop(ANeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALtBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALtBinop} node
	 * @return the default answer for a {@link ALtBinop} node
	 */
	public A caseALtBinop(ALtBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ALeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link ALeBinop} node
	 * @return the default answer for a {@link ALeBinop} node
	 */
	public A caseALeBinop(ALeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGtBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AGtBinop} node
	 * @return the default answer for a {@link AGtBinop} node
	 */
	public A caseAGtBinop(AGtBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AGeBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AGeBinop} node
	 * @return the default answer for a {@link AGeBinop} node
	 */
	public A caseAGeBinop(AGeBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AAndBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AAndBinop} node
	 * @return the default answer for a {@link AAndBinop} node
	 */
	public A caseAAndBinop(AAndBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AOrBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AOrBinop} node
	 * @return the default answer for a {@link AOrBinop} node
	 */
	public A caseAOrBinop(AOrBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AXorBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPBinop(PBinop)}.
	 * @param node the calling {@link AXorBinop} node
	 * @return the default answer for a {@link AXorBinop} node
	 */
	public A caseAXorBinop(AXorBinop node) {
		return defaultPBinop(node);
	}
	
	/**
	 * Returns the default answer for a {@link ANegateUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link ANegateUnop} node
	 * @return the default answer for a {@link ANegateUnop} node
	 */
	public A caseANegateUnop(ANegateUnop node) {
		return defaultPUnop(node);
	}
	
	/**
	 * Returns the default answer for a {@link AComplementUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultPUnop(PUnop)}.
	 * @param node the calling {@link AComplementUnop} node
	 * @return the default answer for a {@link AComplementUnop} node
	 */
	public A caseAComplementUnop(AComplementUnop node) {
		return defaultPUnop(node);
	}
	

	/**
	 * Returns the default answer for a {@link TWhiteSpace} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TWhiteSpace} node
	 * @return the default answer for a {@link TWhiteSpace} node
	 */
	public A caseTWhiteSpace(TWhiteSpace node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTraditionalComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTraditionalComment} node
	 * @return the default answer for a {@link TTraditionalComment} node
	 */
	public A caseTTraditionalComment(TTraditionalComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDocumentationComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDocumentationComment} node
	 * @return the default answer for a {@link TDocumentationComment} node
	 */
	public A caseTDocumentationComment(TDocumentationComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TEndOfLineComment} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TEndOfLineComment} node
	 * @return the default answer for a {@link TEndOfLineComment} node
	 */
	public A caseTEndOfLineComment(TEndOfLineComment node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TCommute} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TCommute} node
	 * @return the default answer for a {@link TCommute} node
	 */
	public A caseTCommute(TCommute node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TDegree} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TDegree} node
	 * @return the default answer for a {@link TDegree} node
	 */
	public A caseTDegree(TDegree node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TFormals} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TFormals} node
	 * @return the default answer for a {@link TFormals} node
	 */
	public A caseTFormals(TFormals node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNegate} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNegate} node
	 * @return the default answer for a {@link TNegate} node
	 */
	public A caseTNegate(TNegate node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TPattern} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TPattern} node
	 * @return the default answer for a {@link TPattern} node
	 */
	public A caseTPattern(TPattern node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TReturns} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TReturns} node
	 * @return the default answer for a {@link TReturns} node
	 */
	public A caseTReturns(TReturns node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TTarget} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TTarget} node
	 * @return the default answer for a {@link TTarget} node
	 */
	public A caseTTarget(TTarget node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAeq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAeq} node
	 * @return the default answer for a {@link TAeq} node
	 */
	public A caseTAeq(TAeq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TAne} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TAne} node
	 * @return the default answer for a {@link TAne} node
	 */
	public A caseTAne(TAne node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TEq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TEq} node
	 * @return the default answer for a {@link TEq} node
	 */
	public A caseTEq(TEq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TGe} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TGe} node
	 * @return the default answer for a {@link TGe} node
	 */
	public A caseTGe(TGe node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TGt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TGt} node
	 * @return the default answer for a {@link TGt} node
	 */
	public A caseTGt(TGt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLe} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLe} node
	 * @return the default answer for a {@link TLe} node
	 */
	public A caseTLe(TLe node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLt} node
	 * @return the default answer for a {@link TLt} node
	 */
	public A caseTLt(TLt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TNe} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TNe} node
	 * @return the default answer for a {@link TNe} node
	 */
	public A caseTNe(TNe node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TLParenthese} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TLParenthese} node
	 * @return the default answer for a {@link TLParenthese} node
	 */
	public A caseTLParenthese(TLParenthese node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TRParenthese} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TRParenthese} node
	 * @return the default answer for a {@link TRParenthese} node
	 */
	public A caseTRParenthese(TRParenthese node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TColon} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TColon} node
	 * @return the default answer for a {@link TColon} node
	 */
	public A caseTColon(TColon node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TComma} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TComma} node
	 * @return the default answer for a {@link TComma} node
	 */
	public A caseTComma(TComma node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TArrow} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TArrow} node
	 * @return the default answer for a {@link TArrow} node
	 */
	public A caseTArrow(TArrow node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpComplement} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpComplement} node
	 * @return the default answer for a {@link TOpComplement} node
	 */
	public A caseTOpComplement(TOpComplement node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLogicalAnd} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpLogicalAnd} node
	 * @return the default answer for a {@link TOpLogicalAnd} node
	 */
	public A caseTOpLogicalAnd(TOpLogicalAnd node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLogicalOr} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpLogicalOr} node
	 * @return the default answer for a {@link TOpLogicalOr} node
	 */
	public A caseTOpLogicalOr(TOpLogicalOr node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpLt} node
	 * @return the default answer for a {@link TOpLt} node
	 */
	public A caseTOpLt(TOpLt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpGt} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpGt} node
	 * @return the default answer for a {@link TOpGt} node
	 */
	public A caseTOpGt(TOpGt node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpEq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpEq} node
	 * @return the default answer for a {@link TOpEq} node
	 */
	public A caseTOpEq(TOpEq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpLteq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpLteq} node
	 * @return the default answer for a {@link TOpLteq} node
	 */
	public A caseTOpLteq(TOpLteq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpGteq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpGteq} node
	 * @return the default answer for a {@link TOpGteq} node
	 */
	public A caseTOpGteq(TOpGteq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpNeq} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpNeq} node
	 * @return the default answer for a {@link TOpNeq} node
	 */
	public A caseTOpNeq(TOpNeq node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMatch} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpMatch} node
	 * @return the default answer for a {@link TOpMatch} node
	 */
	public A caseTOpMatch(TOpMatch node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpPlus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpPlus} node
	 * @return the default answer for a {@link TOpPlus} node
	 */
	public A caseTOpPlus(TOpPlus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMinus} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpMinus} node
	 * @return the default answer for a {@link TOpMinus} node
	 */
	public A caseTOpMinus(TOpMinus node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMul} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpMul} node
	 * @return the default answer for a {@link TOpMul} node
	 */
	public A caseTOpMul(TOpMul node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpDiv} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpDiv} node
	 * @return the default answer for a {@link TOpDiv} node
	 */
	public A caseTOpDiv(TOpDiv node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpAnd} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpAnd} node
	 * @return the default answer for a {@link TOpAnd} node
	 */
	public A caseTOpAnd(TOpAnd node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpOr} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpOr} node
	 * @return the default answer for a {@link TOpOr} node
	 */
	public A caseTOpOr(TOpOr node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpXor} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpXor} node
	 * @return the default answer for a {@link TOpXor} node
	 */
	public A caseTOpXor(TOpXor node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TOpMod} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TOpMod} node
	 * @return the default answer for a {@link TOpMod} node
	 */
	public A caseTOpMod(TOpMod node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TIntegerLiteral} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TIntegerLiteral} node
	 * @return the default answer for a {@link TIntegerLiteral} node
	 */
	public A caseTIntegerLiteral(TIntegerLiteral node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link TIdentifier} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link TIdentifier} node
	 * @return the default answer for a {@link TIdentifier} node
	 */
	public A caseTIdentifier(TIdentifier node) {
		return defaultToken(node);
	}
	
	/**
	 * Returns the default answer for a {@link PPatternCollection} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PPatternCollection} node
	 * @return the default answer for a {@link PPatternCollection} node
	 */
	public A defaultPPatternCollection(PPatternCollection node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PPatternDecl} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PPatternDecl} node
	 * @return the default answer for a {@link PPatternDecl} node
	 */
	public A defaultPPatternDecl(PPatternDecl node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PVar} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PVar} node
	 * @return the default answer for a {@link PVar} node
	 */
	public A defaultPVar(PVar node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link POpcode} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link POpcode} node
	 * @return the default answer for a {@link POpcode} node
	 */
	public A defaultPOpcode(POpcode node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PInst} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PInst} node
	 * @return the default answer for a {@link PInst} node
	 */
	public A defaultPInst(PInst node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PInstPat} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PInstPat} node
	 * @return the default answer for a {@link PInstPat} node
	 */
	public A defaultPInstPat(PInstPat node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PExp} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PExp} node
	 * @return the default answer for a {@link PExp} node
	 */
	public A defaultPExp(PExp node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBuiltin} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBuiltin} node
	 * @return the default answer for a {@link PBuiltin} node
	 */
	public A defaultPBuiltin(PBuiltin node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PCond} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PCond} node
	 * @return the default answer for a {@link PCond} node
	 */
	public A defaultPCond(PCond node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PBinop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PBinop} node
	 * @return the default answer for a {@link PBinop} node
	 */
	public A defaultPBinop(PBinop node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link PUnop} node. The call is deferred
	 * to {@link AnswerAdapter#defaultNode(Node)}.
	 * @param node the calling {@link PUnop} node
	 * @return the default answer for a {@link PUnop} node
	 */
	public A defaultPUnop(PUnop node) {
		return defaultNode(node);
	}
	
	/**
	 * Returns the default answer for a {@link EOF} node. The call is deferred
	 * to {@link AnswerAdapter#defaultToken(Token)}.
	 * @param node the calling {@link EOF} node
	 * @return the default answer for a {@link EOF} node
	 */
    public A caseEOF(EOF node) {
		return defaultToken(node);
	}

}
