/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.peephole.node;

import java.util.*;
import dovs.peephole.analysis.*;

/**
 * {@code ACondConstExp} represents the {@code cond_const} alternative of the {@code exp} production in the AST.
 */
@SuppressWarnings("nls")
public final class ACondConstExp extends PExp {
	private PCond _cond_;

	/**
	 * Creates a new {@link ACondConstExp} node with no children.
	 */
	public ACondConstExp() {
	}

	/**
	 * Creates a new {@code ACondConstExp} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _cond_ the {@link PCond} node for the {@code Cond} child of this {@link ACondConstExp} node
	 */
	public ACondConstExp(@SuppressWarnings("hiding") PCond _cond_) {
		setCond(_cond_);
	}

	/**
	 * Returns a deep clone of this {@link ACondConstExp} node.
	 * @return a deep clone of this {@link ACondConstExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ACondConstExp clone() {
		return new ACondConstExp(
				cloneNode(_cond_)
			);
	}

	/**
	 * Creates a deep clone of this {@link ACondConstExp} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link ACondConstExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ACondConstExp clone(Map<Node,Node> oldToNewMap) {
		ACondConstExp node = new ACondConstExp(
				cloneNode(_cond_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link ACondConstExp} node.
	 * @return a textual representation of this {@link ACondConstExp} node
	 */
	public @Override String toString() {
		return "" + toString(this._cond_);
	}

	/**
	 * Returns the {@link EExp} corresponding to the
	 * type of this {@link PExp} node.
	 * @return the {@link EExp} for this node
	 */
	public EExp kindPExp() {
		return EExp.COND_CONST;
	}

	/**
	 * Returns the {@link PCond} node which is the {@code cond} child of this {@link ACondConstExp} node.
	 * @return the {@link PCond} node which is the {@code cond} child of this {@link ACondConstExp} node
	 */
	public PCond getCond() {
		return this._cond_;
	}

	/**
	 * Sets the {@code cond} child of this {@link ACondConstExp} node.
	 * @param value the new {@code cond} child of this {@link ACondConstExp} node
	 */
	public void setCond(PCond value) {
		if (this._cond_ != null) {
			this._cond_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._cond_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link ACondConstExp} node.
	 * @param child the child node to be removed from this {@link ACondConstExp} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link ACondConstExp} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._cond_ == child) {
			this._cond_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link ACondConstExp} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link ACondConstExp} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link ACondConstExp} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._cond_ == oldChild) {
			setCond((PCond)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link ACondConstExp} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._cond_ != null) {
			this._cond_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link ACondConstExp} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._cond_ != null) {
			if (filter.accept(this._cond_)) {
				collection.add((T)this._cond_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseACondConstExp(ACondConstExp)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link ACondConstExp} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseACondConstExp(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ACondConstExp}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseACondConstExp(this);
	}

	/**
	 * Applies this {@link ACondConstExp} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseACondConstExp(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ACondConstExp} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseACondConstExp(this, question);
	}

}
