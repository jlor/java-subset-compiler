/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.peephole.node;

import java.util.*;
import dovs.peephole.analysis.*;

/**
 * {@code TOpDiv} represents a <code>&apos;/&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TOpDiv extends Token {

	/**
	 * Creates a new {@link TOpDiv} token with no line and position information.
	 */
	public TOpDiv() {
		super.setText("/");
	}

	/**
	 * Creates a new {@link TOpDiv} token with the given line and position information.
	 * @param line the line number information for this {@link TOpDiv} token
	 * @param pos the line position information for this {@link TOpDiv} token
	 */
	public TOpDiv(int line, int pos) {
		super.setText("/");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TOpDiv} token.
	 * @return a clone of this {@link TOpDiv} token
	 */
	public @Override TOpDiv clone() {
		return new TOpDiv(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TOpDiv} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TOpDiv} token
	 */
	public @Override TOpDiv clone(Map<Node,Node> oldToNewMap) {
		TOpDiv token = new TOpDiv(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.OPDIV;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TOpDiv represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TOpDiv text.");
	}

	/**
	 * Calls the {@link Analysis#caseTOpDiv(TOpDiv)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TOpDiv} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTOpDiv(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TOpDiv}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTOpDiv(this);
	}

	/**
	 * Applies this {@link TOpDiv} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTOpDiv(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TOpDiv} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTOpDiv(this, question);
	}

}
