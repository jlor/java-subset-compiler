/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.peephole.node;

import java.util.*;
import dovs.peephole.analysis.*;

/**
 * {@code TTarget} represents a <code>&apos;target&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TTarget extends Token {

	/**
	 * Creates a new {@link TTarget} token with no line and position information.
	 */
	public TTarget() {
		super.setText("target");
	}

	/**
	 * Creates a new {@link TTarget} token with the given line and position information.
	 * @param line the line number information for this {@link TTarget} token
	 * @param pos the line position information for this {@link TTarget} token
	 */
	public TTarget(int line, int pos) {
		super.setText("target");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TTarget} token.
	 * @return a clone of this {@link TTarget} token
	 */
	public @Override TTarget clone() {
		return new TTarget(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TTarget} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TTarget} token
	 */
	public @Override TTarget clone(Map<Node,Node> oldToNewMap) {
		TTarget token = new TTarget(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.TARGET;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TTarget represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TTarget text.");
	}

	/**
	 * Calls the {@link Analysis#caseTTarget(TTarget)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TTarget} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTTarget(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TTarget}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTTarget(this);
	}

	/**
	 * Applies this {@link TTarget} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTTarget(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TTarget} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTTarget(this, question);
	}

}
