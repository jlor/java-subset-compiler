/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code TPlus} represents a <code>&apos;+&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TPlus extends Token {

	/**
	 * Creates a new {@link TPlus} token with no line and position information.
	 */
	public TPlus() {
		super.setText("+");
	}

	/**
	 * Creates a new {@link TPlus} token with the given line and position information.
	 * @param line the line number information for this {@link TPlus} token
	 * @param pos the line position information for this {@link TPlus} token
	 */
	public TPlus(int line, int pos) {
		super.setText("+");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TPlus} token.
	 * @return a clone of this {@link TPlus} token
	 */
	public @Override TPlus clone() {
		return new TPlus(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TPlus} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TPlus} token
	 */
	public @Override TPlus clone(Map<Node,Node> oldToNewMap) {
		TPlus token = new TPlus(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.PLUS;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TPlus represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TPlus text.");
	}

	/**
	 * Calls the {@link Analysis#caseTPlus(TPlus)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TPlus} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTPlus(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TPlus}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTPlus(this);
	}

	/**
	 * Applies this {@link TPlus} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTPlus(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TPlus} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTPlus(this, question);
	}

}
