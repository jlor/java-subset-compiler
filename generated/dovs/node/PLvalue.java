/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;

/**
 * {@code PLvalue} is the super class of all {@code lvalue} classes.
 */
@SuppressWarnings("nls")
public abstract class PLvalue extends Node {
	/**
	 * Returns the {@link ELvalue} corresponding to the
	 * type of this {@link PLvalue} node.
	 * @return the {@link ELvalue} for this node
	 */
	public abstract ELvalue kindPLvalue();

	/**
	 * Returns the {@link NodeEnum} corresponding to the
	 * type of this {@link Node} node.
	 * @return the {@link NodeEnum} for this node
	 */
	public NodeEnum kindNode() {
		return NodeEnum._LVALUE;
	}

}
