/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code AFieldDecl} represents the {@code field} alternative of the {@code decl} production in the AST.
 */
@SuppressWarnings("nls")
public final class AFieldDecl extends PDecl {
	private PAccess _access_;
	private TStatic _static_;
	private TFinal _final_;
	private PType _type_;
	private TIdentifier _name_;
	private PExp _init_;

	/**
	 * Creates a new {@link AFieldDecl} node with no children.
	 */
	public AFieldDecl() {
	}

	/**
	 * Creates a new {@code AFieldDecl} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _access_ the {@link PAccess} node for the {@code Access} child of this {@link AFieldDecl} node
	 * @param _static_ the {@link TStatic} node for the {@code Static} child of this {@link AFieldDecl} node
	 * @param _final_ the {@link TFinal} node for the {@code Final} child of this {@link AFieldDecl} node
	 * @param _type_ the {@link PType} node for the {@code Type} child of this {@link AFieldDecl} node
	 * @param _name_ the {@link TIdentifier} node for the {@code Name} child of this {@link AFieldDecl} node
	 * @param _init_ the {@link PExp} node for the {@code Init} child of this {@link AFieldDecl} node
	 */
	public AFieldDecl(@SuppressWarnings("hiding") PAccess _access_,
		@SuppressWarnings("hiding") TStatic _static_,
		@SuppressWarnings("hiding") TFinal _final_,
		@SuppressWarnings("hiding") PType _type_,
		@SuppressWarnings("hiding") TIdentifier _name_,
		@SuppressWarnings("hiding") PExp _init_)
	{
		setAccess(_access_);
		setStatic(_static_);
		setFinal(_final_);
		setType(_type_);
		setName(_name_);
		setInit(_init_);
	}

	/**
	 * Returns a deep clone of this {@link AFieldDecl} node.
	 * @return a deep clone of this {@link AFieldDecl} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AFieldDecl clone() {
		return new AFieldDecl(
				cloneNode(_access_),
				cloneNode(_static_),
				cloneNode(_final_),
				cloneNode(_type_),
				cloneNode(_name_),
				cloneNode(_init_)
			);
	}

	/**
	 * Creates a deep clone of this {@link AFieldDecl} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link AFieldDecl} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AFieldDecl clone(Map<Node,Node> oldToNewMap) {
		AFieldDecl node = new AFieldDecl(
				cloneNode(_access_, oldToNewMap),
				cloneNode(_static_, oldToNewMap),
				cloneNode(_final_, oldToNewMap),
				cloneNode(_type_, oldToNewMap),
				cloneNode(_name_, oldToNewMap),
				cloneNode(_init_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link AFieldDecl} node.
	 * @return a textual representation of this {@link AFieldDecl} node
	 */
	public @Override String toString() {
		return "" + toString(this._access_)
			 + toString(this._static_)
			 + toString(this._final_)
			 + toString(this._type_)
			 + toString(this._name_)
			 + toString(this._init_);
	}

	/**
	 * Returns the {@link EDecl} corresponding to the
	 * type of this {@link PDecl} node.
	 * @return the {@link EDecl} for this node
	 */
	public EDecl kindPDecl() {
		return EDecl.FIELD;
	}

	/**
	 * Returns the {@link PAccess} node which is the {@code access} child of this {@link AFieldDecl} node.
	 * @return the {@link PAccess} node which is the {@code access} child of this {@link AFieldDecl} node
	 */
	public PAccess getAccess() {
		return this._access_;
	}

	/**
	 * Sets the {@code access} child of this {@link AFieldDecl} node.
	 * @param value the new {@code access} child of this {@link AFieldDecl} node
	 */
	public void setAccess(PAccess value) {
		if (this._access_ != null) {
			this._access_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._access_ = value;
	}

	/**
	 * Returns the possibly {@code null} {@link TStatic} node which is the {@code static} child of this {@link AFieldDecl} node.
	 * @return the possibly {@code null} {@link TStatic} node which is the {@code static} child of this {@link AFieldDecl} node
	 */
	public TStatic getStatic() {
		return this._static_;
	}

	/**
	 * Sets the {@code static} child of this {@link AFieldDecl} node.
	 * @param value the new {@code static} child of this {@link AFieldDecl} node
	 */
	public void setStatic(TStatic value) {
		if (this._static_ != null) {
			this._static_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._static_ = value;
	}

	/**
	 * Returns the possibly {@code null} {@link TFinal} node which is the {@code final} child of this {@link AFieldDecl} node.
	 * @return the possibly {@code null} {@link TFinal} node which is the {@code final} child of this {@link AFieldDecl} node
	 */
	public TFinal getFinal() {
		return this._final_;
	}

	/**
	 * Sets the {@code final} child of this {@link AFieldDecl} node.
	 * @param value the new {@code final} child of this {@link AFieldDecl} node
	 */
	public void setFinal(TFinal value) {
		if (this._final_ != null) {
			this._final_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._final_ = value;
	}

	/**
	 * Returns the {@link PType} node which is the {@code type} child of this {@link AFieldDecl} node.
	 * @return the {@link PType} node which is the {@code type} child of this {@link AFieldDecl} node
	 */
	public PType getType() {
		return this._type_;
	}

	/**
	 * Sets the {@code type} child of this {@link AFieldDecl} node.
	 * @param value the new {@code type} child of this {@link AFieldDecl} node
	 */
	public void setType(PType value) {
		if (this._type_ != null) {
			this._type_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._type_ = value;
	}

	/**
	 * Returns the {@link TIdentifier} node which is the {@code name} child of this {@link AFieldDecl} node.
	 * @return the {@link TIdentifier} node which is the {@code name} child of this {@link AFieldDecl} node
	 */
	public TIdentifier getName() {
		return this._name_;
	}

	/**
	 * Sets the {@code name} child of this {@link AFieldDecl} node.
	 * @param value the new {@code name} child of this {@link AFieldDecl} node
	 */
	public void setName(TIdentifier value) {
		if (this._name_ != null) {
			this._name_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._name_ = value;
	}

	/**
	 * Returns the possibly {@code null} {@link PExp} node which is the {@code init} child of this {@link AFieldDecl} node.
	 * @return the possibly {@code null} {@link PExp} node which is the {@code init} child of this {@link AFieldDecl} node
	 */
	public PExp getInit() {
		return this._init_;
	}

	/**
	 * Sets the {@code init} child of this {@link AFieldDecl} node.
	 * @param value the new {@code init} child of this {@link AFieldDecl} node
	 */
	public void setInit(PExp value) {
		if (this._init_ != null) {
			this._init_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._init_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link AFieldDecl} node.
	 * @param child the child node to be removed from this {@link AFieldDecl} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link AFieldDecl} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._access_ == child) {
			this._access_ = null;
			return;
		}
		if (this._static_ == child) {
			this._static_ = null;
			return;
		}
		if (this._final_ == child) {
			this._final_ = null;
			return;
		}
		if (this._type_ == child) {
			this._type_ = null;
			return;
		}
		if (this._name_ == child) {
			this._name_ = null;
			return;
		}
		if (this._init_ == child) {
			this._init_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link AFieldDecl} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link AFieldDecl} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link AFieldDecl} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._access_ == oldChild) {
			setAccess((PAccess)newChild);
			return;
		}
		if (this._static_ == oldChild) {
			setStatic((TStatic)newChild);
			return;
		}
		if (this._final_ == oldChild) {
			setFinal((TFinal)newChild);
			return;
		}
		if (this._type_ == oldChild) {
			setType((PType)newChild);
			return;
		}
		if (this._name_ == oldChild) {
			setName((TIdentifier)newChild);
			return;
		}
		if (this._init_ == oldChild) {
			setInit((PExp)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link AFieldDecl} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._access_ != null) {
			this._access_.getDescendants(collection, filter);
		}
		if(this._static_ != null) {
			this._static_.getDescendants(collection, filter);
		}
		if(this._final_ != null) {
			this._final_.getDescendants(collection, filter);
		}
		if(this._type_ != null) {
			this._type_.getDescendants(collection, filter);
		}
		if(this._name_ != null) {
			this._name_.getDescendants(collection, filter);
		}
		if(this._init_ != null) {
			this._init_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link AFieldDecl} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._access_ != null) {
			if (filter.accept(this._access_)) {
				collection.add((T)this._access_);
			}
		}
		if(this._static_ != null) {
			if (filter.accept(this._static_)) {
				collection.add((T)this._static_);
			}
		}
		if(this._final_ != null) {
			if (filter.accept(this._final_)) {
				collection.add((T)this._final_);
			}
		}
		if(this._type_ != null) {
			if (filter.accept(this._type_)) {
				collection.add((T)this._type_);
			}
		}
		if(this._name_ != null) {
			if (filter.accept(this._name_)) {
				collection.add((T)this._name_);
			}
		}
		if(this._init_ != null) {
			if (filter.accept(this._init_)) {
				collection.add((T)this._init_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseAFieldDecl(AFieldDecl)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link AFieldDecl} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseAFieldDecl(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AFieldDecl}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseAFieldDecl(this);
	}

	/**
	 * Applies this {@link AFieldDecl} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseAFieldDecl(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AFieldDecl} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseAFieldDecl(this, question);
	}

}
