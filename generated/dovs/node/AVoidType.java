/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code AVoidType} represents the {@code void} alternative of the {@code type} production in the AST.
 */
@SuppressWarnings("nls")
public final class AVoidType extends PType {
	private TVoid _token_;

	/**
	 * Creates a new {@link AVoidType} node with no children.
	 */
	public AVoidType() {
	}

	/**
	 * Creates a new {@code AVoidType} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _token_ the {@link TVoid} node for the {@code Token} child of this {@link AVoidType} node
	 */
	public AVoidType(@SuppressWarnings("hiding") TVoid _token_) {
		setToken(_token_);
	}

	/**
	 * Returns a deep clone of this {@link AVoidType} node.
	 * @return a deep clone of this {@link AVoidType} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AVoidType clone() {
		return new AVoidType(
				cloneNode(_token_)
			);
	}

	/**
	 * Creates a deep clone of this {@link AVoidType} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link AVoidType} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AVoidType clone(Map<Node,Node> oldToNewMap) {
		AVoidType node = new AVoidType(
				cloneNode(_token_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link AVoidType} node.
	 * @return a textual representation of this {@link AVoidType} node
	 */
	public @Override String toString() {
		return "" + toString(this._token_);
	}

	/**
	 * Returns the {@link EType} corresponding to the
	 * type of this {@link PType} node.
	 * @return the {@link EType} for this node
	 */
	public EType kindPType() {
		return EType.VOID;
	}

	/**
	 * Returns the {@link TVoid} node which is the {@code token} child of this {@link AVoidType} node.
	 * @return the {@link TVoid} node which is the {@code token} child of this {@link AVoidType} node
	 */
	public TVoid getToken() {
		return this._token_;
	}

	/**
	 * Sets the {@code token} child of this {@link AVoidType} node.
	 * @param value the new {@code token} child of this {@link AVoidType} node
	 */
	public void setToken(TVoid value) {
		if (this._token_ != null) {
			this._token_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._token_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link AVoidType} node.
	 * @param child the child node to be removed from this {@link AVoidType} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link AVoidType} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._token_ == child) {
			this._token_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link AVoidType} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link AVoidType} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link AVoidType} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._token_ == oldChild) {
			setToken((TVoid)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link AVoidType} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._token_ != null) {
			this._token_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link AVoidType} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._token_ != null) {
			if (filter.accept(this._token_)) {
				collection.add((T)this._token_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseAVoidType(AVoidType)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link AVoidType} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseAVoidType(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AVoidType}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseAVoidType(this);
	}

	/**
	 * Applies this {@link AVoidType} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseAVoidType(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AVoidType} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseAVoidType(this, question);
	}

}
