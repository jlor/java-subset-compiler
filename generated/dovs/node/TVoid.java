/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code TVoid} represents a <code>&apos;void&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TVoid extends Token {

	/**
	 * Creates a new {@link TVoid} token with no line and position information.
	 */
	public TVoid() {
		super.setText("void");
	}

	/**
	 * Creates a new {@link TVoid} token with the given line and position information.
	 * @param line the line number information for this {@link TVoid} token
	 * @param pos the line position information for this {@link TVoid} token
	 */
	public TVoid(int line, int pos) {
		super.setText("void");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TVoid} token.
	 * @return a clone of this {@link TVoid} token
	 */
	public @Override TVoid clone() {
		return new TVoid(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TVoid} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TVoid} token
	 */
	public @Override TVoid clone(Map<Node,Node> oldToNewMap) {
		TVoid token = new TVoid(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.VOID;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TVoid represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TVoid text.");
	}

	/**
	 * Calls the {@link Analysis#caseTVoid(TVoid)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TVoid} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTVoid(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TVoid}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTVoid(this);
	}

	/**
	 * Applies this {@link TVoid} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTVoid(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TVoid} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTVoid(this, question);
	}

}
