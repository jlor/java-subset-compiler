/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code TImplements} represents a <code>&apos;implements&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TImplements extends Token {

	/**
	 * Creates a new {@link TImplements} token with no line and position information.
	 */
	public TImplements() {
		super.setText("implements");
	}

	/**
	 * Creates a new {@link TImplements} token with the given line and position information.
	 * @param line the line number information for this {@link TImplements} token
	 * @param pos the line position information for this {@link TImplements} token
	 */
	public TImplements(int line, int pos) {
		super.setText("implements");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TImplements} token.
	 * @return a clone of this {@link TImplements} token
	 */
	public @Override TImplements clone() {
		return new TImplements(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TImplements} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TImplements} token
	 */
	public @Override TImplements clone(Map<Node,Node> oldToNewMap) {
		TImplements token = new TImplements(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.IMPLEMENTS;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TImplements represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TImplements text.");
	}

	/**
	 * Calls the {@link Analysis#caseTImplements(TImplements)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TImplements} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTImplements(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TImplements}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTImplements(this);
	}

	/**
	 * Applies this {@link TImplements} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTImplements(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TImplements} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTImplements(this, question);
	}

}
