/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code TThis} represents a <code>&apos;this&apos;</code> token from the input file.
 */
@SuppressWarnings("nls")
public final class TThis extends Token {

	/**
	 * Creates a new {@link TThis} token with no line and position information.
	 */
	public TThis() {
		super.setText("this");
	}

	/**
	 * Creates a new {@link TThis} token with the given line and position information.
	 * @param line the line number information for this {@link TThis} token
	 * @param pos the line position information for this {@link TThis} token
	 */
	public TThis(int line, int pos) {
		super.setText("this");
		setLine(line);
		setPos(pos);
	}

	/**
	 * Creates a clone of this {@link TThis} token.
	 * @return a clone of this {@link TThis} token
	 */
	public @Override TThis clone() {
		return new TThis(getLine(), getPos());
	}

	/**
	 * Creates a deep clone of this {@link TThis} token while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a clone of this {@link TThis} token
	 */
	public @Override TThis clone(Map<Node,Node> oldToNewMap) {
		TThis token = new TThis(getLine(), getPos());
		oldToNewMap.put(this, token);
		return token;
	}

	/**
	 * Returns the {@link TokenEnum} corresponding to the
	 * type of this {@link Token} node.
	 * @return the {@link TokenEnum} for this node
	 */
	public TokenEnum kindToken() {
		return TokenEnum.THIS;
	}

	/**
	 * Implements the {@link Token#setText(String)} method. Since TThis represents
	 * fixed token, this method throws a {@link RuntimeException}.
	 * @param text the new text of this token
	 */
	public @Override void setText(@SuppressWarnings("unused") String text) {
		throw new RuntimeException("Cannot change TThis text.");
	}

	/**
	 * Calls the {@link Analysis#caseTThis(TThis)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link TThis} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseTThis(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TThis}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseTThis(this);
	}

	/**
	 * Applies this {@link TThis} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseTThis(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link TThis} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseTThis(this, question);
	}

}
