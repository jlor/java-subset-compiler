/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;

/**
 * {@code PExp} is the super class of all {@code exp} classes.
 */
@SuppressWarnings("nls")
public abstract class PExp extends Node {
	/**
	 * Returns the {@link EExp} corresponding to the
	 * type of this {@link PExp} node.
	 * @return the {@link EExp} for this node
	 */
	public abstract EExp kindPExp();

	/**
	 * Returns the {@link NodeEnum} corresponding to the
	 * type of this {@link Node} node.
	 * @return the {@link NodeEnum} for this node
	 */
	public NodeEnum kindNode() {
		return NodeEnum._EXP;
	}

}
