/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code ASourceFile} represents the {@code } alternative of the {@code source_file} production in the AST.
 */
@SuppressWarnings("nls")
public final class ASourceFile extends PSourceFile {
	private APackageDecl _package_;
	private NodeList<PImportDecl> _imports_ = new NodeList<PImportDecl>(this);
	private PTypeDecl _type_decl_;

	/**
	 * Creates a new {@link ASourceFile} node with no children.
	 */
	public ASourceFile() {
	}

	/**
	 * Creates a new {@code ASourceFile} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _package_ the {@link APackageDecl} node for the {@code Package} child of this {@link ASourceFile} node
	 * @param _imports_ the list of {@link PImportDecl} nodes for the {@code Imports} children of this {@link ASourceFile} node
	 * @param _type_decl_ the {@link PTypeDecl} node for the {@code TypeDecl} child of this {@link ASourceFile} node
	 */
	public ASourceFile(@SuppressWarnings("hiding") APackageDecl _package_,
		@SuppressWarnings("hiding") List<PImportDecl> _imports_,
		@SuppressWarnings("hiding") PTypeDecl _type_decl_)
	{
		setPackage(_package_);
		setImports(_imports_);
		setTypeDecl(_type_decl_);
	}

	/**
	 * Returns a deep clone of this {@link ASourceFile} node.
	 * @return a deep clone of this {@link ASourceFile} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ASourceFile clone() {
		return new ASourceFile(
				cloneNode(_package_),
				cloneList(_imports_),
				cloneNode(_type_decl_)
			);
	}

	/**
	 * Creates a deep clone of this {@link ASourceFile} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link ASourceFile} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ASourceFile clone(Map<Node,Node> oldToNewMap) {
		ASourceFile node = new ASourceFile(
				cloneNode(_package_, oldToNewMap),
				cloneList(_imports_, oldToNewMap),
				cloneNode(_type_decl_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link ASourceFile} node.
	 * @return a textual representation of this {@link ASourceFile} node
	 */
	public @Override String toString() {
		return "" + toString(this._package_)
			 + toString(this._imports_)
			 + toString(this._type_decl_);
	}

	/**
	 * Returns the possibly {@code null} {@link APackageDecl} node which is the {@code package} child of this {@link ASourceFile} node.
	 * @return the possibly {@code null} {@link APackageDecl} node which is the {@code package} child of this {@link ASourceFile} node
	 */
	public APackageDecl getPackage() {
		return this._package_;
	}

	/**
	 * Sets the {@code package} child of this {@link ASourceFile} node.
	 * @param value the new {@code package} child of this {@link ASourceFile} node
	 */
	public void setPackage(APackageDecl value) {
		if (this._package_ != null) {
			this._package_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._package_ = value;
	}

	/**
	 * Returns the possibly empty list of {@link PImportDecl} nodes that are the {@code imports} children of this {@link ASourceFile} node.
	 * @return the possibly empty list of {@link PImportDecl} nodes that are the {@code imports} children of this {@link ASourceFile} node
	 */
	public LinkedList<PImportDecl> getImports() {
		return this._imports_;
	}

	/**
	 * Sets the {@code imports} children of this {@link ASourceFile} node.
	 * @param value the new {@code imports} children of this {@link ASourceFile} node
	 */
	public void setImports(List<PImportDecl> value) {
		if (value == this._imports_) {
			return;
		}
		this._imports_.clear();
		this._imports_.addAll(value);
	}

	/**
	 * Returns the {@link PTypeDecl} node which is the {@code type_decl} child of this {@link ASourceFile} node.
	 * @return the {@link PTypeDecl} node which is the {@code type_decl} child of this {@link ASourceFile} node
	 */
	public PTypeDecl getTypeDecl() {
		return this._type_decl_;
	}

	/**
	 * Sets the {@code type_decl} child of this {@link ASourceFile} node.
	 * @param value the new {@code type_decl} child of this {@link ASourceFile} node
	 */
	public void setTypeDecl(PTypeDecl value) {
		if (this._type_decl_ != null) {
			this._type_decl_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._type_decl_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link ASourceFile} node.
	 * @param child the child node to be removed from this {@link ASourceFile} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link ASourceFile} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._package_ == child) {
			this._package_ = null;
			return;
		}
		if (this._imports_.remove(child)) {
			return;
		}
		if (this._type_decl_ == child) {
			this._type_decl_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link ASourceFile} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link ASourceFile} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link ASourceFile} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._package_ == oldChild) {
			setPackage((APackageDecl)newChild);
			return;
		}
		for (ListIterator<PImportDecl> i = this._imports_.listIterator() ; i.hasNext() ; ) {
			if (i.next() == oldChild) {
				if (newChild != null) {
					i.set((PImportDecl)newChild);
					return;
				}
				i.remove();
				return;
			}
		}
		if (this._type_decl_ == oldChild) {
			setTypeDecl((PTypeDecl)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link ASourceFile} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._package_ != null) {
			this._package_.getDescendants(collection, filter);
		}
		for (PImportDecl e : new ArrayList<PImportDecl>(this._imports_)) {
			e.getDescendants(collection, filter);
		}
		if(this._type_decl_ != null) {
			this._type_decl_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link ASourceFile} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._package_ != null) {
			if (filter.accept(this._package_)) {
				collection.add((T)this._package_);
			}
		}
		for (PImportDecl e : new ArrayList<PImportDecl>(this._imports_)) {
			if (filter.accept(e)) {
				collection.add((T)e);
			}
		}
		if(this._type_decl_ != null) {
			if (filter.accept(this._type_decl_)) {
				collection.add((T)this._type_decl_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseASourceFile(ASourceFile)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link ASourceFile} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseASourceFile(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ASourceFile}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseASourceFile(this);
	}

	/**
	 * Applies this {@link ASourceFile} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseASourceFile(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ASourceFile} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseASourceFile(this, question);
	}

}
