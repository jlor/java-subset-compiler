/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;

/**
 * {@code PLocalDecl} is the super class of all {@code local_decl} classes.
 */
@SuppressWarnings("nls")
public abstract class PLocalDecl extends Node {
	/**
	 * Returns the {@link TIdentifier} node which is the {@code name} child of this {@link PLocalDecl} node.
	 * @return the {@link TIdentifier} node which is the {@code name} child of this {@link PLocalDecl} node
	 */
	public abstract TIdentifier getName();

	/**
	 * Sets the {@code name} child of this {@link PLocalDecl} node.
	 * @param value the new {@code name} child of this {@link PLocalDecl} node
	 */
	public abstract void setName(TIdentifier value);

	/**
	 * Returns the possibly {@code null} {@link PExp} node which is the {@code init} child of this {@link PLocalDecl} node.
	 * @return the possibly {@code null} {@link PExp} node which is the {@code init} child of this {@link PLocalDecl} node
	 */
	public abstract PExp getInit();

	/**
	 * Sets the {@code init} child of this {@link PLocalDecl} node.
	 * @param value the new {@code init} child of this {@link PLocalDecl} node
	 */
	public abstract void setInit(PExp value);

	/**
	 * Returns the {@link PType} node which is the {@code type} child of this {@link PLocalDecl} node.
	 * @return the {@link PType} node which is the {@code type} child of this {@link PLocalDecl} node
	 */
	public abstract PType getType();

	/**
	 * Sets the {@code type} child of this {@link PLocalDecl} node.
	 * @param value the new {@code type} child of this {@link PLocalDecl} node
	 */
	public abstract void setType(PType value);

	/**
	 * Returns the {@link NodeEnum} corresponding to the
	 * type of this {@link Node} node.
	 * @return the {@link NodeEnum} for this node
	 */
	public NodeEnum kindNode() {
		return NodeEnum._LOCALDECL;
	}

}
