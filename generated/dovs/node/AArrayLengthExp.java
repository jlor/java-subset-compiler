/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code AArrayLengthExp} represents the {@code array_length} alternative of the {@code exp} production in the AST.
 */
@SuppressWarnings("nls")
public final class AArrayLengthExp extends PExp {
	private PExp _exp_;

	/**
	 * Creates a new {@link AArrayLengthExp} node with no children.
	 */
	public AArrayLengthExp() {
	}

	/**
	 * Creates a new {@code AArrayLengthExp} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _exp_ the {@link PExp} node for the {@code Exp} child of this {@link AArrayLengthExp} node
	 */
	public AArrayLengthExp(@SuppressWarnings("hiding") PExp _exp_) {
		setExp(_exp_);
	}

	/**
	 * Returns a deep clone of this {@link AArrayLengthExp} node.
	 * @return a deep clone of this {@link AArrayLengthExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AArrayLengthExp clone() {
		return new AArrayLengthExp(
				cloneNode(_exp_)
			);
	}

	/**
	 * Creates a deep clone of this {@link AArrayLengthExp} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link AArrayLengthExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AArrayLengthExp clone(Map<Node,Node> oldToNewMap) {
		AArrayLengthExp node = new AArrayLengthExp(
				cloneNode(_exp_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link AArrayLengthExp} node.
	 * @return a textual representation of this {@link AArrayLengthExp} node
	 */
	public @Override String toString() {
		return "" + toString(this._exp_);
	}

	/**
	 * Returns the {@link EExp} corresponding to the
	 * type of this {@link PExp} node.
	 * @return the {@link EExp} for this node
	 */
	public EExp kindPExp() {
		return EExp.ARRAY_LENGTH;
	}

	/**
	 * Returns the {@link PExp} node which is the {@code exp} child of this {@link AArrayLengthExp} node.
	 * @return the {@link PExp} node which is the {@code exp} child of this {@link AArrayLengthExp} node
	 */
	public PExp getExp() {
		return this._exp_;
	}

	/**
	 * Sets the {@code exp} child of this {@link AArrayLengthExp} node.
	 * @param value the new {@code exp} child of this {@link AArrayLengthExp} node
	 */
	public void setExp(PExp value) {
		if (this._exp_ != null) {
			this._exp_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._exp_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link AArrayLengthExp} node.
	 * @param child the child node to be removed from this {@link AArrayLengthExp} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link AArrayLengthExp} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._exp_ == child) {
			this._exp_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link AArrayLengthExp} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link AArrayLengthExp} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link AArrayLengthExp} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._exp_ == oldChild) {
			setExp((PExp)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link AArrayLengthExp} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._exp_ != null) {
			this._exp_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link AArrayLengthExp} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._exp_ != null) {
			if (filter.accept(this._exp_)) {
				collection.add((T)this._exp_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseAArrayLengthExp(AArrayLengthExp)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link AArrayLengthExp} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseAArrayLengthExp(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AArrayLengthExp}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseAArrayLengthExp(this);
	}

	/**
	 * Applies this {@link AArrayLengthExp} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseAArrayLengthExp(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AArrayLengthExp} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseAArrayLengthExp(this, question);
	}

}
