/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code AQualifiedName} represents the {@code qualified} alternative of the {@code name} production in the AST.
 */
@SuppressWarnings("nls")
public final class AQualifiedName extends PName {
	private NodeList<TIdentifier> _identifiers_ = new NodeList<TIdentifier>(this);

	/**
	 * Creates a new {@link AQualifiedName} node with no children.
	 */
	public AQualifiedName() {
	}

	/**
	 * Creates a new {@code AQualifiedName} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _identifiers_ the list of {@link TIdentifier} nodes for the {@code Identifiers} children of this {@link AQualifiedName} node
	 */
	public AQualifiedName(@SuppressWarnings("hiding") List<TIdentifier> _identifiers_) {
		setIdentifiers(_identifiers_);
	}

	/**
	 * Returns a deep clone of this {@link AQualifiedName} node.
	 * @return a deep clone of this {@link AQualifiedName} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AQualifiedName clone() {
		return new AQualifiedName(
				cloneList(_identifiers_)
			);
	}

	/**
	 * Creates a deep clone of this {@link AQualifiedName} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link AQualifiedName} node
	 */
	@Override @SuppressWarnings("unchecked")
	public AQualifiedName clone(Map<Node,Node> oldToNewMap) {
		AQualifiedName node = new AQualifiedName(
				cloneList(_identifiers_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link AQualifiedName} node.
	 * @return a textual representation of this {@link AQualifiedName} node
	 */
	public @Override String toString() {
		return "" + toString(this._identifiers_);
	}

	/**
	 * Returns the {@link EName} corresponding to the
	 * type of this {@link PName} node.
	 * @return the {@link EName} for this node
	 */
	public EName kindPName() {
		return EName.QUALIFIED;
	}

	/**
	 * Returns the possibly empty list of {@link TIdentifier} nodes that are the {@code identifiers} children of this {@link AQualifiedName} node.
	 * @return the possibly empty list of {@link TIdentifier} nodes that are the {@code identifiers} children of this {@link AQualifiedName} node
	 */
	public LinkedList<TIdentifier> getIdentifiers() {
		return this._identifiers_;
	}

	/**
	 * Sets the {@code identifiers} children of this {@link AQualifiedName} node.
	 * @param value the new {@code identifiers} children of this {@link AQualifiedName} node
	 */
	public void setIdentifiers(List<TIdentifier> value) {
		if (value == this._identifiers_) {
			return;
		}
		this._identifiers_.clear();
		this._identifiers_.addAll(value);
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link AQualifiedName} node.
	 * @param child the child node to be removed from this {@link AQualifiedName} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link AQualifiedName} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._identifiers_.remove(child)) {
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link AQualifiedName} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link AQualifiedName} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link AQualifiedName} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		for (ListIterator<TIdentifier> i = this._identifiers_.listIterator() ; i.hasNext() ; ) {
			if (i.next() == oldChild) {
				if (newChild != null) {
					i.set((TIdentifier)newChild);
					return;
				}
				i.remove();
				return;
			}
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link AQualifiedName} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		for (TIdentifier e : new ArrayList<TIdentifier>(this._identifiers_)) {
			e.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link AQualifiedName} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		for (TIdentifier e : new ArrayList<TIdentifier>(this._identifiers_)) {
			if (filter.accept(e)) {
				collection.add((T)e);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseAQualifiedName(AQualifiedName)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link AQualifiedName} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseAQualifiedName(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AQualifiedName}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseAQualifiedName(this);
	}

	/**
	 * Applies this {@link AQualifiedName} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseAQualifiedName(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link AQualifiedName} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseAQualifiedName(this, question);
	}

}
