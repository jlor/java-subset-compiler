/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code ACastExp} represents the {@code cast} alternative of the {@code exp} production in the AST.
 */
@SuppressWarnings("nls")
public final class ACastExp extends PExp {
	private TLParenthese _token_;
	private PType _type_;
	private PExp _exp_;

	/**
	 * Creates a new {@link ACastExp} node with no children.
	 */
	public ACastExp() {
	}

	/**
	 * Creates a new {@code ACastExp} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _token_ the {@link TLParenthese} node for the {@code Token} child of this {@link ACastExp} node
	 * @param _type_ the {@link PType} node for the {@code Type} child of this {@link ACastExp} node
	 * @param _exp_ the {@link PExp} node for the {@code Exp} child of this {@link ACastExp} node
	 */
	public ACastExp(@SuppressWarnings("hiding") TLParenthese _token_,
		@SuppressWarnings("hiding") PType _type_,
		@SuppressWarnings("hiding") PExp _exp_)
	{
		setToken(_token_);
		setType(_type_);
		setExp(_exp_);
	}

	/**
	 * Returns a deep clone of this {@link ACastExp} node.
	 * @return a deep clone of this {@link ACastExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ACastExp clone() {
		return new ACastExp(
				cloneNode(_token_),
				cloneNode(_type_),
				cloneNode(_exp_)
			);
	}

	/**
	 * Creates a deep clone of this {@link ACastExp} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link ACastExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ACastExp clone(Map<Node,Node> oldToNewMap) {
		ACastExp node = new ACastExp(
				cloneNode(_token_, oldToNewMap),
				cloneNode(_type_, oldToNewMap),
				cloneNode(_exp_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link ACastExp} node.
	 * @return a textual representation of this {@link ACastExp} node
	 */
	public @Override String toString() {
		return "" + toString(this._token_)
			 + toString(this._type_)
			 + toString(this._exp_);
	}

	/**
	 * Returns the {@link EExp} corresponding to the
	 * type of this {@link PExp} node.
	 * @return the {@link EExp} for this node
	 */
	public EExp kindPExp() {
		return EExp.CAST;
	}

	/**
	 * Returns the {@link TLParenthese} node which is the {@code token} child of this {@link ACastExp} node.
	 * @return the {@link TLParenthese} node which is the {@code token} child of this {@link ACastExp} node
	 */
	public TLParenthese getToken() {
		return this._token_;
	}

	/**
	 * Sets the {@code token} child of this {@link ACastExp} node.
	 * @param value the new {@code token} child of this {@link ACastExp} node
	 */
	public void setToken(TLParenthese value) {
		if (this._token_ != null) {
			this._token_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._token_ = value;
	}

	/**
	 * Returns the {@link PType} node which is the {@code type} child of this {@link ACastExp} node.
	 * @return the {@link PType} node which is the {@code type} child of this {@link ACastExp} node
	 */
	public PType getType() {
		return this._type_;
	}

	/**
	 * Sets the {@code type} child of this {@link ACastExp} node.
	 * @param value the new {@code type} child of this {@link ACastExp} node
	 */
	public void setType(PType value) {
		if (this._type_ != null) {
			this._type_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._type_ = value;
	}

	/**
	 * Returns the {@link PExp} node which is the {@code exp} child of this {@link ACastExp} node.
	 * @return the {@link PExp} node which is the {@code exp} child of this {@link ACastExp} node
	 */
	public PExp getExp() {
		return this._exp_;
	}

	/**
	 * Sets the {@code exp} child of this {@link ACastExp} node.
	 * @param value the new {@code exp} child of this {@link ACastExp} node
	 */
	public void setExp(PExp value) {
		if (this._exp_ != null) {
			this._exp_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._exp_ = value;
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link ACastExp} node.
	 * @param child the child node to be removed from this {@link ACastExp} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link ACastExp} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._token_ == child) {
			this._token_ = null;
			return;
		}
		if (this._type_ == child) {
			this._type_ = null;
			return;
		}
		if (this._exp_ == child) {
			this._exp_ = null;
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link ACastExp} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link ACastExp} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link ACastExp} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._token_ == oldChild) {
			setToken((TLParenthese)newChild);
			return;
		}
		if (this._type_ == oldChild) {
			setType((PType)newChild);
			return;
		}
		if (this._exp_ == oldChild) {
			setExp((PExp)newChild);
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link ACastExp} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._token_ != null) {
			this._token_.getDescendants(collection, filter);
		}
		if(this._type_ != null) {
			this._type_.getDescendants(collection, filter);
		}
		if(this._exp_ != null) {
			this._exp_.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link ACastExp} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._token_ != null) {
			if (filter.accept(this._token_)) {
				collection.add((T)this._token_);
			}
		}
		if(this._type_ != null) {
			if (filter.accept(this._type_)) {
				collection.add((T)this._type_);
			}
		}
		if(this._exp_ != null) {
			if (filter.accept(this._exp_)) {
				collection.add((T)this._exp_);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseACastExp(ACastExp)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link ACastExp} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseACastExp(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ACastExp}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseACastExp(this);
	}

	/**
	 * Applies this {@link ACastExp} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseACastExp(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ACastExp} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseACastExp(this, question);
	}

}
