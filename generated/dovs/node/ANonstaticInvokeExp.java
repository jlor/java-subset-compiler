/* This file was generated by SableCC (http://www.sablecc.org/). */

package dovs.node;

import java.util.*;
import dovs.analysis.*;

/**
 * {@code ANonstaticInvokeExp} represents the {@code nonstatic_invoke} alternative of the {@code exp} production in the AST.
 */
@SuppressWarnings("nls")
public final class ANonstaticInvokeExp extends PExp {
	private PExp _receiver_;
	private TIdentifier _name_;
	private NodeList<PExp> _args_ = new NodeList<PExp>(this);

	/**
	 * Creates a new {@link ANonstaticInvokeExp} node with no children.
	 */
	public ANonstaticInvokeExp() {
	}

	/**
	 * Creates a new {@code ANonstaticInvokeExp} node with the given nodes as children.
	 * The basic child nodes are removed from their previous parents.
	 * @param _receiver_ the {@link PExp} node for the {@code Receiver} child of this {@link ANonstaticInvokeExp} node
	 * @param _name_ the {@link TIdentifier} node for the {@code Name} child of this {@link ANonstaticInvokeExp} node
	 * @param _args_ the list of {@link PExp} nodes for the {@code Args} children of this {@link ANonstaticInvokeExp} node
	 */
	public ANonstaticInvokeExp(@SuppressWarnings("hiding") PExp _receiver_,
		@SuppressWarnings("hiding") TIdentifier _name_,
		@SuppressWarnings("hiding") List<PExp> _args_)
	{
		setReceiver(_receiver_);
		setName(_name_);
		setArgs(_args_);
	}

	/**
	 * Returns a deep clone of this {@link ANonstaticInvokeExp} node.
	 * @return a deep clone of this {@link ANonstaticInvokeExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ANonstaticInvokeExp clone() {
		return new ANonstaticInvokeExp(
				cloneNode(_receiver_),
				cloneNode(_name_),
				cloneList(_args_)
			);
	}

	/**
	 * Creates a deep clone of this {@link ANonstaticInvokeExp} node while putting all
	 * old node-new node relations in the map {@code oldToNewMap}.
	 * @param oldToNewMap the map filled with the old node-new node relation
	 * @return a deep clone of this {@link ANonstaticInvokeExp} node
	 */
	@Override @SuppressWarnings("unchecked")
	public ANonstaticInvokeExp clone(Map<Node,Node> oldToNewMap) {
		ANonstaticInvokeExp node = new ANonstaticInvokeExp(
				cloneNode(_receiver_, oldToNewMap),
				cloneNode(_name_, oldToNewMap),
				cloneList(_args_, oldToNewMap)
			);
		oldToNewMap.put(this, node);
		return node;
	}

	/**
	 * Returns a textual representation of this {@link ANonstaticInvokeExp} node.
	 * @return a textual representation of this {@link ANonstaticInvokeExp} node
	 */
	public @Override String toString() {
		return "" + toString(this._receiver_)
			 + toString(this._name_)
			 + toString(this._args_);
	}

	/**
	 * Returns the {@link EExp} corresponding to the
	 * type of this {@link PExp} node.
	 * @return the {@link EExp} for this node
	 */
	public EExp kindPExp() {
		return EExp.NONSTATIC_INVOKE;
	}

	/**
	 * Returns the {@link PExp} node which is the {@code receiver} child of this {@link ANonstaticInvokeExp} node.
	 * @return the {@link PExp} node which is the {@code receiver} child of this {@link ANonstaticInvokeExp} node
	 */
	public PExp getReceiver() {
		return this._receiver_;
	}

	/**
	 * Sets the {@code receiver} child of this {@link ANonstaticInvokeExp} node.
	 * @param value the new {@code receiver} child of this {@link ANonstaticInvokeExp} node
	 */
	public void setReceiver(PExp value) {
		if (this._receiver_ != null) {
			this._receiver_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._receiver_ = value;
	}

	/**
	 * Returns the {@link TIdentifier} node which is the {@code name} child of this {@link ANonstaticInvokeExp} node.
	 * @return the {@link TIdentifier} node which is the {@code name} child of this {@link ANonstaticInvokeExp} node
	 */
	public TIdentifier getName() {
		return this._name_;
	}

	/**
	 * Sets the {@code name} child of this {@link ANonstaticInvokeExp} node.
	 * @param value the new {@code name} child of this {@link ANonstaticInvokeExp} node
	 */
	public void setName(TIdentifier value) {
		if (this._name_ != null) {
			this._name_.parent(null);
		}
		if (value != null) {
			if (value.parent() != null) {
				value.parent().removeChild(value);
			}
			value.parent(this);
		}
		this._name_ = value;
	}

	/**
	 * Returns the possibly empty list of {@link PExp} nodes that are the {@code args} children of this {@link ANonstaticInvokeExp} node.
	 * @return the possibly empty list of {@link PExp} nodes that are the {@code args} children of this {@link ANonstaticInvokeExp} node
	 */
	public LinkedList<PExp> getArgs() {
		return this._args_;
	}

	/**
	 * Sets the {@code args} children of this {@link ANonstaticInvokeExp} node.
	 * @param value the new {@code args} children of this {@link ANonstaticInvokeExp} node
	 */
	public void setArgs(List<PExp> value) {
		if (value == this._args_) {
			return;
		}
		this._args_.clear();
		this._args_.addAll(value);
	}

	/**
	 * Removes the {@link Node} {@code child} as a child of this {@link ANonstaticInvokeExp} node.
	 * @param child the child node to be removed from this {@link ANonstaticInvokeExp} node
	 * @throws RuntimeException if {@code child} is not a child of this {@link ANonstaticInvokeExp} node
	 */
	@Override void removeChild(@SuppressWarnings("unused") Node child) {
		if (this._receiver_ == child) {
			this._receiver_ = null;
			return;
		}
		if (this._name_ == child) {
			this._name_ = null;
			return;
		}
		if (this._args_.remove(child)) {
			return;
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Replaces the {@link Node} {@code oldChild} child node of this {@link ANonstaticInvokeExp} node.
	 * with the {@link Node} {@code newChild}.
	 * @param oldChild the child node to be replaced
	 * @param newChild the new child node of this {@link ANonstaticInvokeExp} node
	 * @throws RuntimeException if {@code oldChild} is not a child of this {@link ANonstaticInvokeExp} node
	 */
	@Override void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild) {
		if (this._receiver_ == oldChild) {
			setReceiver((PExp)newChild);
			return;
		}
		if (this._name_ == oldChild) {
			setName((TIdentifier)newChild);
			return;
		}
		for (ListIterator<PExp> i = this._args_.listIterator() ; i.hasNext() ; ) {
			if (i.next() == oldChild) {
				if (newChild != null) {
					i.set((PExp)newChild);
					return;
				}
				i.remove();
				return;
			}
		}
		throw new RuntimeException("Not a child.");
	}

	/**
	 * Adds all descendants of this {@link ANonstaticInvokeExp} node (including the node itself) that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the descendants are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getDescendants(Collection<T> collection, NodeFilter<T> filter) {
		if (filter.accept(this)) {
			collection.add((T)this);
		}
		if(this._receiver_ != null) {
			this._receiver_.getDescendants(collection, filter);
		}
		if(this._name_ != null) {
			this._name_.getDescendants(collection, filter);
		}
		for (PExp e : new ArrayList<PExp>(this._args_)) {
			e.getDescendants(collection, filter);
		}
	}

	/**
	 * Adds all children of this {@link ANonstaticInvokeExp} node that are
	 * accepted by the {@link NodeFilter} {@code filter} to {@code collection}.
	 * @param collection the collection to which the children are added
	 * @param filter the {@link NodeFilter} used
	 */
	@SuppressWarnings("unchecked")
	public <T extends Node> void getChildren(Collection<T> collection, NodeFilter<T> filter) {
		if(this._receiver_ != null) {
			if (filter.accept(this._receiver_)) {
				collection.add((T)this._receiver_);
			}
		}
		if(this._name_ != null) {
			if (filter.accept(this._name_)) {
				collection.add((T)this._name_);
			}
		}
		for (PExp e : new ArrayList<PExp>(this._args_)) {
			if (filter.accept(e)) {
				collection.add((T)e);
			}
		}
	}

	/**
	 * Calls the {@link Analysis#caseANonstaticInvokeExp(ANonstaticInvokeExp)} of the {@link Analysis} {@code sw}.
	 * @param sw the {@link Analysis} to which this {@link ANonstaticInvokeExp} node is applied
	 */
	public void apply(Switch sw) {
		((Analysis)sw).caseANonstaticInvokeExp(this);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ANonstaticInvokeExp}
	 * node to the {@link Answer} visitor.
	 * @param caller the {@link Answer} to which this node is applied
	 * @return the answer as returned from {@code caller}
	 */
	public <A> A apply(Answer<A> caller) {
		return caller.caseANonstaticInvokeExp(this);
	}

	/**
	 * Applies this {@link ANonstaticInvokeExp} node to the {@link Question} visitor {@code caller}.
	 * @param caller the {@link Question} to which this node is applied
	 * @param question the question provided to {@code caller}
	 */
	public <Q> void apply(Question<Q> caller, Q question) {
		caller.caseANonstaticInvokeExp(this, question);
	}

	/**
	 * Returns the answer for {@code caller} by applying this {@link ANonstaticInvokeExp} node with the
	 * {@code question} to the {@link QuestionAnswer} visitor.
	 * @param caller the {@link QuestionAnswer} to which this node is applied
	 * @param question the question provided to {@code caller}
	 * @return the answer as returned from {@code caller}
	 */
	public <Q,A> A apply(QuestionAnswer<Q,A> caller, Q question) {
		return caller.caseANonstaticInvokeExp(this, question);
	}

}
